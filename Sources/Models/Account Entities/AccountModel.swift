//
//  AccountModel.swift
//  Din Aker Brygge
//
//  Created by Paresh on 24/05/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Foundation
import CoreData
import Gloss

class AccountModel:NSObject,Decodable {
    
    // Insert code here to add functionality to your managed object subclass
    
     var accountID: NSNumber?
     var openingHoursTitle: String?
     var address: String?
     var bookTable: String?
     var categoryGroupID: NSNumber?
     var categoryID: NSNumber?
     var city: String?
     var country: String?
     var descr: String?
     var email: String?
     var isFavourite: NSNumber?
     var latitude: NSNumber?
     var longitude: NSNumber?
     var name: String?
     var phone: String?
     var postalCode: String?
     var socialProfile: String?
     var status: String?
     var website: String?
     var images: Array<ImageList>?
     var merchantID: String?
     var isFastTrack: NSNumber?
     var isOpen: NSNumber?
     var startHour: NSNumber?
     var endHour: NSNumber?
     var offset: NSNumber?
    
    required init?(json: JSON) {
        self.accountID =   "AccountID" <~~ json
        self.name = "Name" <~~ json
        self.categoryID = "CategoryID" <~~ json
        self.categoryGroupID = "CategoryGroupID" <~~ json
        self.latitude = "Latitude" <~~ json
        self.longitude = "Longitude" <~~ json
        self.descr = "Description" <~~ json
        
        if (json["ImageList"] != nil) {
            
            self.images = [ImageList].from(jsonArray:("ImageList" <~~ json)!)
            
        }
        
        if self.images == nil {
            self.images = []
        }
        
        //Opening hours
        
        self.openingHoursTitle = "OpeningHoursTitle" <~~ json
        //Other field
        self.isFavourite = "IsFavorite" <~~ json
        self.descr = "Description" <~~ json
        self.email = "EmailID" <~~ json
        self.phone = "Phone" <~~ json
        self.socialProfile = "SocialProfile" <~~ json
        self.address = "Address" <~~ json
        self.city = "City" <~~ json
        self.country = "Country" <~~ json
        self.postalCode = "PostalCode" <~~ json
        self.status = "Status" <~~ json
        self.website = "WebSite" <~~ json
        self.bookTable = "BookTable" <~~ json
        
        self.merchantID = "MerchantID" <~~ json
        self.isFastTrack = "IsFastTrack" <~~ json
        self.isOpen = "IsOpen" <~~ json
        self.startHour = "StartHour" <~~ json
        self.endHour = "EndHour" <~~ json
        self.offset = "Offset" <~~ json
    }
    
    
   
    
    // MARK:- Get Image URL
    func getImageURLForType(_ imageType: Int) -> String? {
        
        switch imageType {
            
        case ACCOUNTBLACK_LOGO, ACCOUNTWHITE_LOGO:
            
            let logo = self.images!.filter { $0.imageType == imageType}
            
            if logo.count > 0 {
                
                let image = logo[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case ACCOUNTIMAGE_FULL:
            
            let full = self.images!.filter { $0.imageType == ACCOUNTIMAGE_FULL}
            let thumb = self.images!.filter { $0.imageType == ACCOUNTIMAGE_THUMB}
            
            if full.count > 0 {
                
                let image = full[0]
                return image.imageURL!
            }
            else if thumb.count > 0 {
                
                let image = thumb[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case ACCOUNTIMAGE_THUMB:
            
            let thumb = self.images!.filter { $0.imageType == ACCOUNTIMAGE_THUMB}
            let logo = self.images!.filter { $0.imageType == ACCOUNTWHITE_LOGO}
            
            if thumb.count > 0 {
                
                let image = thumb[0]
                return image.imageURL!
            }
            else if logo.count > 0 {
                
                let image = logo[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        default:
            return ""
        }
    }
}


