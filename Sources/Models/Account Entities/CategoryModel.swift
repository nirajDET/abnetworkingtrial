//
//  CategoryModel.swift
//  Din Aker Brygge
//
//  Created by Paresh on 24/05/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Foundation
import CoreData
import Gloss

@objc(CategoryModel)
class CategoryModel: NSObject,Glossy {

     var categoryID: NSNumber?
     var name: String?
     var subCategories: [SubCategoryModel]?

    override init() {
        super.init()
    }
    
    
    required init?(json: JSON) {
        self.categoryID =   "CategoryID" <~~ json
        self.name = "Name" <~~ json
        
        let arr:Array<JSON>? = "Categories" <~~ json
        if arr != nil {
            self.subCategories = [SubCategoryModel].from(jsonArray:arr!)
        }
        else {
            self.subCategories = []
        }
        
        
    }
    func toJSON() -> JSON? {
        return jsonify([
            
            "CategoryID" ~~> self.categoryID,
            "Name" ~~> self.name,
            "Categories" ~~> self.subCategories
            ])
    }

}
