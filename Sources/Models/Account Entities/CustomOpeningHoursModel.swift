//
//  CustomOpeningHoursModel.swift
//  Din Aker Brygge
//
//  Created by Paresh on 17/05/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Foundation
import CoreData

//@objc public class CustomOpeningHoursModel : NSObject{
//    
//    var Name : String?
//    var ValidFrom : String?
//    var ValidTo : String?
//    var Status : String?
//    var DayOfWeek : Dictionary<String,AnyObject>?
//    var OpeningType : NSString?
//}

@objc open class CustomOpeningHoursModel : NSManagedObject {
    
    open static let entityName = "CustomOpeningHoursModel"
    
    @NSManaged var Name : String?
    @NSManaged var ValidFrom : String?
    @NSManaged var ValidTo : String?
    @NSManaged var Status : String?
    @NSManaged var DayOfWeek : Dictionary<String,AnyObject>?
    @NSManaged var OpeningType : NSString?
}
