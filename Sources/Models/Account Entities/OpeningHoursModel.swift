//
//  OpeningHoursModel.swift
//  Din Aker Brygge
//
//  Created by Paresh on 17/05/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Foundation
import CoreData

//@objc public class OpeningHoursModel : NSObject{
//
//    var Exception : Array<CustomOpeningHoursModel>?
//    var Default : Array<CustomOpeningHoursModel>?
//}

@objc open class OpeningHoursModel : NSManagedObject {
    
    open static let entityName = "OpeningHoursModel"
    
    @NSManaged var Exception : Array<CustomOpeningHoursModel>?
    @NSManaged var Default : Array<CustomOpeningHoursModel>?
}
