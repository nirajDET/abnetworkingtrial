//
//  SubCategoryModel.swift
//  Din Aker Brygge
//
//  Created by Paresh on 24/05/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Foundation
import CoreData
import Gloss

@objc(SubCategoryModel)
class SubCategoryModel:NSObject, Glossy {

     var categoryID: NSNumber?
     var name: String?
    
    override init() {
        super.init()
    }
    
    required init?(json: JSON) {
        self.categoryID =   "CategoryID" <~~ json
        self.name = "Name" <~~ json

    }
    func toJSON() -> JSON? {
        return jsonify([
            
            "CategoryID" ~~> self.categoryID,
            "Name" ~~> self.name
            ])
    }
    

}
