//
//  Beacon.swift
//  BeaconManager
//
//  Created by Mihir Mehta on 23/11/15.
//  Copyright © 2015 Mihir Mehta. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation
import Gloss




public extension Sequence where Iterator.Element: Equatable {
    var uniqueElements: [Iterator.Element] {
        return self.reduce([]){
            uniqueElements, element in
            
            uniqueElements.contains(element)
                ? uniqueElements
                : uniqueElements + [element]
        }
    }
}

class Beacon:NSObject, Glossy {
    
    var beaconID: NSNumber?
    var beaconMac: String?
    var beaconType: NSNumber?
    var descr: String?
    //var isActive: NSNumber?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var name: String?
    var proximmity: NSNumber?
    var rssi: NSNumber?
    var status: String?
    var vendorSerial: String?
    var ibeacon: Ibeacon?
    
    
    required init?(json: JSON) {
        
        self.beaconID =   "BeaconID" <~~ json
        self.name = "Name" <~~ json
        self.descr = "Description" <~~ json
        self.vendorSerial = "VendorSerial" <~~ json
        self.beaconMac = "BeaconMac" <~~ json
        self.beaconType = "BeaconType" <~~ json
        self.ibeacon =    "ibeacon" <~~ json
        self.latitude =  "latitude" <~~ json
        self.longitude = "longitude" <~~ json
        
        
    }
    func toJSON() -> JSON? {
        return jsonify([
            "BeaconID" ~~> self.beaconID,
            "Name" ~~> self.name,
            "Description" ~~> self.descr,
            "VendorSerial" ~~> self.vendorSerial,
            "BeaconMac" ~~> self.beaconMac,
            "BeaconType" ~~> self.beaconType,
            "ibeacon" ~~> self.ibeacon,
            "latitude" ~~> self.latitude,
            "longitude" ~~> self.longitude,
            ])
    }
    
    override var description : String {
        return "BeaconID: \(beaconID) \(ibeacon!.description)"
        
    }
    override func isEqual(_ object: Any?) -> Bool {
        if let rhs = object as? Beacon {
            return ibeacon!.uuid == rhs.ibeacon!.uuid
        }
        return false
    }

}
