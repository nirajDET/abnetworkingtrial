//
//  BeaconRules.swift
//  Din Aker Brygge
//
//  Created by Paresh on 26/05/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Foundation
import CoreData
import Gloss

func ==(lhs:BeaconRules,rhs:BeaconRules) -> Bool {
    if lhs.beaconID == rhs.beaconID  && lhs.endtimeStamp == rhs.endtimeStamp && lhs.interval == rhs.interval && lhs.lastTimeTriggered == rhs.lastTimeTriggered  && lhs.proximity == rhs.proximity && lhs.startTimeStamp == rhs.startTimeStamp  {
        return true
    }
    return false
}


func !=(lhs:BeaconRules,rhs:BeaconRules) -> Bool {
    return !(lhs == rhs)
}

@objc(BeaconRules)
class BeaconRules: NSObject, Glossy, NSCopying {

     var beaconID: NSNumber?
     var endtimeStamp: NSNumber?
     var interval: NSNumber?
     var lastTimeTriggered: NSNumber?
     var proximity: NSNumber?
     var startTimeStamp: NSNumber?
    
    override init() {
        
        super.init()
    }
    
    required init?(json: JSON) {
        
        self.beaconID = "BeaconID" <~~ json
        self.proximity = "Proximity" <~~ json
        self.interval = "Interval" <~~ json
        self.startTimeStamp = "StartTimeStamp" <~~ json
        self.endtimeStamp = "EndtimeStamp" <~~ json
        self.lastTimeTriggered = "LastTimeTriggered"  <~~ json
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        return jsonify([
            
            "BeaconID" ~~> self.beaconID,
            "Proximity" ~~> self.proximity,
            "Interval" ~~> self.interval,
            "LastTimeTriggered" ~~> self.lastTimeTriggered,
            "StartTimeStamp" ~~> self.startTimeStamp,
            "EndtimeStamp" ~~> self.endtimeStamp
            ])
    }
    func copy(with zone: NSZone?) -> Any {
        let newValue = BeaconRules()
        newValue.beaconID = beaconID
        newValue.endtimeStamp = endtimeStamp
        newValue.interval = interval
        newValue.lastTimeTriggered = lastTimeTriggered
        newValue.proximity = proximity
        newValue.startTimeStamp = startTimeStamp
        return newValue
       
    }
    override var description : String {
        return "beaconID: \(beaconID) endtimeStamp: \(endtimeStamp) interval: \(interval) lastTimeTriggered: \(lastTimeTriggered) proximity: \(proximity) startTimeStamp: \(startTimeStamp)"
    }
    
}
