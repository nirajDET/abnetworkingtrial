//
//  BeaconScheduleModel.swift
//  Din Aker Brygge
//
//  Created by Rahul on 10/6/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import UIKit
import Gloss

class BeaconScheduleModel: NSObject, Glossy {

    var Title: String?
    var Day: String?
    var From: NSNumber?
    var To: NSNumber?
    
    required init(Title: String, Day: String, From: NSNumber, To: NSNumber) {
        
        super.init()
        self.Title = Title
        self.Day = Day
        self.From = From
        self.To = To
    }
    
    required init?(json: JSON) {
        
        self.Title = "Title" <~~ json
        self.Day = "Day" <~~ json
        self.From = "From" <~~ json
        self.To = "To" <~~ json
    }
    
    func toJSON() -> JSON? {
        
        return jsonify([
            "Title" ~~> self.Title,
            "Day" ~~> self.Day,
            "From" ~~> self.From,
            "To" ~~> self.To
            ])
    }
    
    func copyObject() -> BeaconScheduleModel {
        
        let copy = BeaconScheduleModel(Title: Title!, Day: Day!, From: From!, To: To!)
        return copy
    }
}
