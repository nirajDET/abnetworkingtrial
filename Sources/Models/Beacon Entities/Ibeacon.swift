//
//  Ibeacon.swift
//  BeaconManager
//
//  Created by Mihir Mehta on 28/04/16.
//  Copyright © 2016 Mihir Mehta. All rights reserved.
//

import Foundation
import CoreData
import Gloss

class Ibeacon:NSObject, Glossy {
    
    var lastProximity: NSNumber?
    var major: NSNumber?
    var minor: NSNumber?
    var uuid: String?
    
    
    required init?(json: JSON) {
        self.uuid = "UUID" <~~ json
        self.major = "Major" <~~ json
        self.minor = "Minor" <~~ json
    }
    func toJSON() -> JSON? {
        return jsonify([
            
            "UUID" ~~> self.uuid,
            "Major" ~~> self.major,
            "Minor" ~~> self.minor
            ])
        
    }
    
    override var description : String {
        return "UUID : \(self.uuid!) MAJOR : \(self.major!) MINOR : \(self.minor!) "
    }
    
    //    self.ibeacon?.uuid = "ibeacon.UUID" <~~ json
    //    self.ibeacon?.major = "ibeacon.Major" <~~ json
    //    self.ibeacon?.minor = "ibeacon.Minor" <~~ json
}
