//
//  self.swift
//  Din Aker Brygge
//
//  Created by Rahul on 8/11/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//



import Foundation
import CoreData
import Gloss
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


//func ==(lhs:CampaignActivity,rhs:CampaignActivity) -> Bool {
//    if lhs.title == rhs.title  && lhs.summary == rhs.summary && lhs.descr == rhs.descr && lhs.startDateTime == rhs.startDateTime && lhs.endDateTime == rhs.endDateTime && lhs.PinToTopStart == rhs.PinToTopStart  && lhs.PinToTopEnd == rhs.PinToTopEnd  && lhs.SendPushNotification == rhs.SendPushNotification && lhs.Status == rhs.Status && lhs.ShowAfterOnBoarding == rhs.ShowAfterOnBoarding  && lhs.ShowInFeed == rhs.ShowInFeed && lhs.IsForUnder20 == rhs.IsForUnder20 && lhs.IsForOver20 == rhs.IsForOver20 && lhs.IsTargetAudienceMale == rhs.IsTargetAudienceMale && lhs.IsTargetAudienceFemale == rhs.IsTargetAudienceFemale  && lhs.IsPinToTop == rhs.IsPinToTop  && lhs.Status == rhs.Status && lhs.offerKronerDiscount == rhs.offerKronerDiscount && lhs.offerKronerPrice == rhs.offerKronerPrice  && lhs.offerPercentageDiscount == rhs.offerPercentageDiscount && lhs.offerReceiveQuantity == rhs.offerReceiveQuantity && lhs.offerPayForQuantity == rhs.offerPayForQuantity && lhs.redemptionLimit == rhs.redemptionLimit && lhs.RedemptionLimitPerPerson == rhs.RedemptionLimitPerPerson  && lhs.url == rhs.url  && lhs.eventAddress == rhs.eventAddress && lhs.offerRatioWord == rhs.offerRatioWord && lhs.offerText == rhs.offerText  && lhs.offerTitle == rhs.offerTitle && lhs.accountAddress == rhs.accountAddress && lhs.plainTextArticleEvent == rhs.plainTextArticleEvent {
//
//        if lhs.imageList?.count != rhs.imageList?.count || lhs.beaconRulesList?.count != rhs.beaconRulesList?.count || lhs.Interests.count != rhs.Interests.count{
//            return false
//        }
//
//        if lhs.imageList != nil && rhs.imageList != nil && lhs.imageList?.count > 0 && rhs.imageList?.count > 0 {
//            for (index,element) in lhs.imageList!.enumerate() {
//                if element != rhs.imageList![index] {
//                    return false
//                }
//            }
//        }
//
//        if lhs.beaconRulesList != nil && rhs.beaconRulesList != nil && lhs.beaconRulesList?.count > 0 && rhs.beaconRulesList?.count > 0 {
//            for (index,element) in lhs.beaconRulesList!.enumerate() {
//                if element != rhs.beaconRulesList![index] {
//                    return false
//                }
//            }
//        }
//
//        if lhs.Interests.count > 0 && rhs.Interests.count > 0 {
//            for (index,element) in lhs.Interests.enumerate() {
//                if element != rhs.Interests[index] {
//                    return false
//                }
//            }
//        }
//        return true
//
//    }
//    return false
//}
//


@objc enum CampaignActivityType : Int {
    case weather = 0, percentageDiscountOffer = 1, kronerDiscountOffer = 2, kronerPriceOffer = 3,receiveQuantityOffer  = 4, textOffer = 5, longTermOffer = 6, event	= 7, article = 8, account	= 9, accountService = 10, cityExperience = 11, instagram = 12, facebook = 13, pinterest = 14, twitter = 15, blogPost = 16
}

@objc enum TileType : Int {
    case squareTile = 0, largeSquareTile = 1, rectangleTile = 2, dynamicTile = 3, largeRectangleTile = 4
}


@objc(CampaignActivity)
class CampaignActivity: NSObject,Decodable,NSCopying {
    
    
    var accountAddress: String?
    var accountID: NSNumber?
    var accountName: String?
    var campaignActivityID: NSNumber?
    var campaignActivityType: NSNumber?
    var categoryID: NSNumber?
    var descr: String?
    var endDateTime: NSNumber?
    var eventAddress: String?
    var facilitiesBoldTitle: String?
    var facilitiesRegularTitle: String?
    var facilitiesSymbol: String?
    var hTMLContent: String?
    var isFavorite: NSNumber?
    var lastDisplayedTimestamp: NSNumber?
    var latitude: NSNumber?
    var longitude: NSNumber?
    var notificationID: NSNumber?
    var offerKronerDiscount: NSNumber?
    var offerKronerPrice: NSNumber?
    var offerPayForQuantity: NSNumber?
    var offerPercentageDiscount: NSNumber?
    var offerRatioWord: String?
    var offerReceiveQuantity: NSNumber?
    var offerText: String?
    var offerType: NSNumber?
    var postDate: NSNumber?
    var startDateTime: NSNumber?
    var summary: String?
    var tileSize: NSNumber?
    var title: String?
    var url: String?
    var weatherName: String?
    var weatherTemperature: String?
    var weatherVar: String?
    var weatherWindDirection: String?
    var weatherWindSpeed: String?
    var offerTitle: String?
    var beaconRulesList: Array<BeaconRules>? = []
    var imageList: Array<ImageList>?
    var IsForUnder20: Bool? = false
    var IsForOver20: Bool? = false
    var IsTargetAudienceMale: Bool? = false
    var IsTargetAudienceFemale: Bool? = false
    var IsPinToTop: Bool? = false
    var ShowAfterOnBoarding: Bool? = false
    var ShowInFeed: Bool? = false
    var IsShowInAccountFeed: Bool? = false
    var SendPushNotification: Bool? = false
    var Status: Bool? = false
    var PinToTopStart: NSNumber?
    var PinToTopEnd: NSNumber?
    var Interests: [InterestEntity] = []
    var RedemptionLimit: NSNumber? = 0
    var RedemptionLimitPerPerson: NSNumber? = 0
    var RedemptionLimitTotal: NSNumber? = 0
    var plainTextArticleEvent:String?
    var merchantId:String?
    var OfferRedeemded: Array<OfferRedeemdedModel>? = []
    var BeaconSchedule: Array<BeaconScheduleModel>? = []
    var IsApproved: Bool? = true
    var isReviewed:Bool? = false
    var IsCampaignExpired: Bool? = false
    
    var SelectedRedeemQuantity = 1
    
    override init() {
        super.init()
    }
    
    
    func isEqual(_ lhs:CampaignActivity,rhs:CampaignActivity) -> Bool {
        if lhs.title == rhs.title  && lhs.summary == rhs.summary && lhs.descr == rhs.descr && lhs.startDateTime == rhs.startDateTime && lhs.endDateTime == rhs.endDateTime && lhs.PinToTopStart == rhs.PinToTopStart  && lhs.PinToTopEnd == rhs.PinToTopEnd  && lhs.SendPushNotification == rhs.SendPushNotification && lhs.Status == rhs.Status && lhs.IsApproved == rhs.IsApproved && lhs.ShowAfterOnBoarding == rhs.ShowAfterOnBoarding  && lhs.ShowInFeed == rhs.ShowInFeed && lhs.IsShowInAccountFeed == rhs.IsShowInAccountFeed && lhs.IsForUnder20 == rhs.IsForUnder20 && lhs.IsForOver20 == rhs.IsForOver20 && lhs.IsTargetAudienceMale == rhs.IsTargetAudienceMale && lhs.IsTargetAudienceFemale == rhs.IsTargetAudienceFemale  && lhs.IsPinToTop == rhs.IsPinToTop  && lhs.Status == rhs.Status && lhs.offerKronerDiscount == rhs.offerKronerDiscount && lhs.offerKronerPrice == rhs.offerKronerPrice  && lhs.offerPercentageDiscount == rhs.offerPercentageDiscount && lhs.offerReceiveQuantity == rhs.offerReceiveQuantity && lhs.offerPayForQuantity == rhs.offerPayForQuantity && lhs.RedemptionLimitTotal == rhs.RedemptionLimitTotal && lhs.RedemptionLimitPerPerson == rhs.RedemptionLimitPerPerson  && lhs.url == rhs.url  && lhs.eventAddress == rhs.eventAddress && lhs.offerRatioWord == rhs.offerRatioWord && lhs.offerText == rhs.offerText  && lhs.offerTitle == rhs.offerTitle && lhs.plainTextArticleEvent == rhs.plainTextArticleEvent && lhs.campaignActivityType ==  rhs.campaignActivityType {
            
            if lhs.imageList?.count != rhs.imageList?.count || lhs.beaconRulesList?.count != rhs.beaconRulesList?.count || lhs.Interests.count != rhs.Interests.count{
                return false
            }
            
            if lhs.imageList != nil && rhs.imageList != nil && lhs.imageList?.count > 0 && rhs.imageList?.count > 0 {
                for (index,element) in lhs.imageList!.enumerated() {
                    if element != rhs.imageList![index] {
                        return false
                    }
                }
            }
            
            if lhs.beaconRulesList != nil && rhs.beaconRulesList != nil && lhs.beaconRulesList?.count > 0 && rhs.beaconRulesList?.count > 0 {
                for (index,element) in lhs.beaconRulesList!.enumerated() {
                    if element != rhs.beaconRulesList![index] {
                        return false
                    }
                }
            }
            
            if lhs.Interests.count > 0 && rhs.Interests.count > 0 {
                for (index,element) in lhs.Interests.enumerated() {
                    if element != rhs.Interests[index] {
                        return false
                    }
                }
            }
            return true
            
        }
        return false
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        if let activity:CampaignActivity = object as? CampaignActivity {
            return activity.campaignActivityID == self.campaignActivityID
        }
        return false
    }
    
    
    required init(accountID: NSNumber, interests: [InterestEntity]) {
        
        super.init()
        self.campaignActivityID = -1 //In Admin module, this will help to identify that it's new campaign activity
        self.campaignActivityType = -1 //In Admin module, this will help to identify whether we should show preview or not
        self.accountID = accountID
        self.Interests = interests
        self.tileSize = 0
        self.isFavorite = 0
        self.offerPercentageDiscount = nil
        self.offerKronerDiscount = nil
        self.offerKronerPrice = nil
        self.offerReceiveQuantity = nil
        self.offerPayForQuantity = nil
        self.RedemptionLimitTotal = 10000
        self.RedemptionLimitPerPerson = -1
        let dateTimeDouble = Date().timeIntervalSince1970 * 1000
        self.startDateTime = dateTimeDouble as NSNumber? //NSDate().timeIntervalSince1970 * 1000
        self.endDateTime = dateTimeDouble as NSNumber?//NSDate().timeIntervalSince1970 * 1000
        self.PinToTopStart = dateTimeDouble as NSNumber?//NSDate().timeIntervalSince1970 * 1000
        self.PinToTopEnd = dateTimeDouble as NSNumber?//NSDate().timeIntervalSince1970 * 1000
        self.imageList = []
        self.Status = false
        
        //Delivery types
        self.ShowAfterOnBoarding = false
        self.ShowInFeed = false
        self.IsShowInAccountFeed = false
        self.SendPushNotification = false
        self.IsPinToTop = false
        
        //Targets
        self.IsForOver20 = true
        self.IsForUnder20 = true
        self.IsTargetAudienceMale = true
        self.IsTargetAudienceFemale = true
        self.plainTextArticleEvent = ""
        self.merchantId = ""
        
        self.IsApproved = true
        self.isReviewed = false
        
        
    }
    
    required init?(json: JSON) {
        
        super.init()
        self.campaignActivityID = "CampaignActivityID" <~~ json
        self.campaignActivityType = "CampaignActivityType" <~~ json
        self.tileSize = "TileSize" <~~ json
        self.title = "Title" <~~ json
        self.summary = "Summary" <~~ json
        self.descr = "Description" <~~ json
        self.hTMLContent = "HTMLContent" <~~ json
        self.url = "URL" <~~ json
        
        if (json["ImageList"] != nil) {
            self.imageList = [ImageList].from(jsonArray:("ImageList" <~~ json)!)
        }
        if self.imageList == nil {
            self.imageList = []
        }
        
        self.notificationID = "NotificationID" <~~ json
        self.isFavorite = "IsFavorite" <~~ json
        self.postDate = "PostDate" <~~ json
        self.latitude =  "Latitude" <~~ json
        self.longitude = "Longitude" <~~ json
        self.startDateTime = "StartDateTime" <~~ json
        self.endDateTime = "EndDateTime" <~~ json
        self.eventAddress = "EventAddress" <~~ json
        self.offerKronerDiscount = "OfferKronerDiscount" <~~ json
        self.offerReceiveQuantity = "OfferReceiveQuantity" <~~ json
        self.offerKronerPrice = "OfferKronerPrice" <~~ json
        self.offerRatioWord = "OfferRatioWord" <~~ json
        self.offerPercentageDiscount =  "OfferPercentageDiscount" <~~ json
        
        self.offerPayForQuantity = "OfferPayForQuantity" <~~ json
        self.offerTitle = "OfferTitle" <~~ json
        self.offerText = "OfferText" <~~ json
        self.offerType = "OfferType" <~~ json
        self.accountID = "AccountID" <~~ json
        self.accountAddress = "AccountAddress" <~~ json
        
        let tempCategoryID:Int? = "CategoryID" <~~ json
        if tempCategoryID != nil {
            self.categoryID = tempCategoryID! as NSNumber?
        }
        self.accountName =  "AccountName" <~~ json
        self.facilitiesSymbol =  "FacilitiesSymbol" <~~ json
        self.facilitiesBoldTitle = "FacilitiesBoldTitle" <~~ json
        self.facilitiesRegularTitle = "FacilitiesRegularTitle" <~~ json
        self.weatherName =  "WeatherName" <~~ json
        self.weatherWindDirection = "WeatherWindDirection" <~~ json
        self.weatherTemperature =  "WeatherTemperature" <~~ json
        self.weatherWindSpeed = "WeatherWindSpeed" <~~ json
        self.weatherVar = "WeatherVar" <~~ json
        
        self.IsForUnder20 = "IsForUnder20" <~~ json
        self.IsForOver20 = "IsForOver20" <~~ json
        self.IsTargetAudienceMale = "IsTargetAudienceMale" <~~ json
        self.IsTargetAudienceFemale = "IsTargetAudienceFemale" <~~ json
        self.IsPinToTop = "IsPinToTop" <~~ json
        self.ShowAfterOnBoarding = "ShowAfterOnBoarding" <~~ json
        self.ShowInFeed = "ShowInFeed" <~~ json
        self.IsShowInAccountFeed = "ShowInAccountFeed" <~~ json
        self.SendPushNotification = "SendPushNotification" <~~ json
        self.Status = "Status" <~~ json
        self.PinToTopStart = "PinToTopStart" <~~ json
        self.PinToTopEnd = "PinToTopEnd" <~~ json
        self.RedemptionLimit = "RedemptionLimit" <~~ json
        self.RedemptionLimitTotal = "RedemptionLimitTotal" <~~ json
        self.RedemptionLimitPerPerson = "RedemptionLimitPerPerson" <~~ json
        self.plainTextArticleEvent = "PlainTextArticleEvent" <~~ json
        self.merchantId = "MerchantId" <~~ json
        self.IsApproved = "IsApproved" <~~ json
        self.isReviewed = "IsReviewed" <~~ json
        self.IsCampaignExpired = "IsCampaignExpired" <~~ json
        
        //Beacon Rules List
        if (json["BeaconRulesList"] != nil) {
            
            self.beaconRulesList = [BeaconRules].from(jsonArray:("BeaconRulesList" <~~ json)!)
            
            
            
        }
        if self.beaconRulesList == nil {
            self.beaconRulesList = []
        }
        
        for rule in self.beaconRulesList! {
            print("\(rule.description)")
        }
        
        //Interest List
        self.Interests = []
        if (json["Interests"] != nil) {
            
            for dicts in json["Interests"] as! Array<Dictionary<String, AnyObject>> {
                
                let interest = InterestEntity(json: dicts)
                
                self.Interests.append(interest!)
            }
        }
        
        //Offer Redeem List
        if (json["OfferRedeemded"] != nil) {
            self.OfferRedeemded = [OfferRedeemdedModel].from(jsonArray:("OfferRedeemded" <~~ json)!)
        }
        if self.OfferRedeemded == nil {
            self.OfferRedeemded = []
        }
        
        //Beacon Schedule List
        if (json["BeaconSchedule"] != nil) {
            self.BeaconSchedule = [BeaconScheduleModel].from(jsonArray:("BeaconSchedule" <~~ json)!)
        }
        if self.BeaconSchedule == nil {
            self.BeaconSchedule = []
        }
        if self.ShowInFeed == false {
            self.IsPinToTop = false
        }
        
    }
    
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        //Convert InterestEntity array to JSon array
        let interestJSon: NSMutableArray = []
        for interest in self.Interests {
            
            let params = [
                "InterestID" : "\(interest.interestID!)",
                "Name"       : interest.name!
            ]
            interestJSon.add(params)
        }
        
        if AdminUserManager.sharedInstance.checkIfUserIsSuperAdmin() == false {
            self.isReviewed = false
        }
        
        if self.ShowInFeed == false {
            self.IsPinToTop = false
        }
        
        return jsonify([
            
            "CampaignActivityID" ~~> self.campaignActivityID,
            "CampaignActivityType" ~~> self.campaignActivityType,
            "AccountID" ~~> self.accountID,
            "TileSize" ~~> self.tileSize,
            "Title" ~~> self.title,
            "Summary" ~~> self.summary,
            "Description" ~~> self.descr,
            "StartDateTime" ~~> self.startDateTime,
            "EndDateTime" ~~> self.endDateTime,
            "EventAddress" ~~> self.eventAddress,
            "OfferKronerDiscount" ~~> self.offerKronerDiscount,
            "OfferReceiveQuantity" ~~> self.offerReceiveQuantity,
            "OfferKronerPrice" ~~> self.offerKronerPrice,
            "OfferRatioWord" ~~> self.offerRatioWord,
            "OfferPercentageDiscount" ~~> self.offerPercentageDiscount,
            "OfferPayForQuantity" ~~> self.offerPayForQuantity,
            "OfferText" ~~> self.offerText,
            "OfferTitle" ~~> self.offerTitle,
            "RedemptionLimit" ~~> self.RedemptionLimit,
            "RedemptionLimitTotal" ~~> self.RedemptionLimitTotal,
            "RedemptionLimitPerPerson" ~~> self.RedemptionLimitPerPerson,
            "IsForUnder20" ~~> self.IsForUnder20,
            "IsForOver20" ~~> self.IsForOver20,
            "IsTargetAudienceMale" ~~> self.IsTargetAudienceMale,
            "IsTargetAudienceFemale" ~~> self.IsTargetAudienceFemale,
            "IsPinToTop" ~~> self.IsPinToTop,
            "ShowInFeed" ~~> self.ShowInFeed,
            "ShowInAccountFeed" ~~> self.IsShowInAccountFeed,
            "SendPushNotification" ~~> self.SendPushNotification,
            "ShowAfterOnBoarding" ~~> self.ShowAfterOnBoarding,
            "PinToTopStart" ~~> self.PinToTopStart,
            "PinToTopEnd" ~~> self.PinToTopEnd,
            "ImageList" ~~> self.imageList?.toJSONArray(),
            "BeaconRulesList" ~~> self.beaconRulesList?.toJSONArray(),
            "BeaconSchedule" ~~> self.BeaconSchedule?.toJSONArray(),
            "Interests" ~~> interestJSon,
            "IsFavorite" ~~> self.isFavorite,
            "Status" ~~> self.Status,
            "PlainTextArticleEvent" ~~> self.plainTextArticleEvent,
            "MerchantId" ~~> self.merchantId,
            "IsApproved" ~~> self.IsApproved,
            "IsReviewed" ~~> self.isReviewed,
            "IsCampaignExpired" ~~> self.IsCampaignExpired
            ])
    }
    
    //Insert code here to add functionality to your managed object subclass
    static let entityName = "CampaignActivity"
    
    func getImageURLStringForActivity(_ isLarge:Bool) -> String? {
        
        var string:String?
        switch self.campaignActivityType!.intValue  {
        case CampaignActivityType.account.rawValue,CampaignActivityType.accountService.rawValue:
            
            for imageList:ImageList in self.imageList! {
                if isLarge == true && imageList.imageType == ACCOUNTIMAGE_FULL {
                    return imageList.imageURL
                    
                }
                else if isLarge == false && imageList.imageType == ACCOUNTIMAGE_THUMB{
                    return imageList.imageURL
                }
            }
        default:
            string =  nil
        }
        return string
    }
    
    
    // MARK:- Get Image URL
    func getImageURLForType(_ imageType: Int) -> String? {
        
        if self.imageList == nil {
            
            return ""
        }
        
        switch imageType {
            
        case ACCOUNTBLACK_LOGO, ACCOUNTWHITE_LOGO:
            
            let logo = self.imageList!.filter { $0.imageType == imageType}
            
            if logo.count > 0 {
                
                let image = logo[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case ACCOUNTIMAGE_FULL:
            
            let full = self.imageList!.filter { $0.imageType == ACCOUNTIMAGE_FULL}
            
            if full.count > 0 {
                
                let image = full[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case ACCOUNTIMAGE_THUMB:
            
            let thumb = self.imageList!.filter { $0.imageType == ACCOUNTIMAGE_THUMB}
            
            if thumb.count > 0 {
                
                let image = thumb[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case OFFERIMAGE_FULL:
            
            let full = self.imageList!.filter { $0.imageType == OFFERIMAGE_FULL}
            
            if full.count > 0 {
                
                let image = full[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case OFFERIMAGE_THUMB:
            
            let thumb = self.imageList!.filter { $0.imageType == OFFERIMAGE_THUMB}
            
            if thumb.count > 0 {
                
                let image = thumb[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case EVENTIMAGE_FULL:
            
            let full = self.imageList!.filter { $0.imageType == EVENTIMAGE_FULL}
            
            if full.count > 0 {
                
                let image = full[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case EVENTIMAGE_THUMB:
            
            let thumb = self.imageList!.filter { $0.imageType == EVENTIMAGE_THUMB}
            
            if thumb.count > 0 {
                
                let image = thumb[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case CITYEXPERIENCE_FULL:
            
            let full = self.imageList!.filter { $0.imageType == CITYEXPERIENCE_FULL}
            
            if full.count > 0 {
                
                let image = full[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case CITYEXPERIENCE_THUMB:
            
            let thumb = self.imageList!.filter { $0.imageType == CITYEXPERIENCE_THUMB}
            
            if thumb.count > 0 {
                
                let image = thumb[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case ARTICLE_FULL:
            
            let full = self.imageList!.filter { $0.imageType == ARTICLE_FULL}
            
            if full.count > 0 {
                
                let image = full[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case ARTICLE_THUMB:
            
            let thumb = self.imageList!.filter { $0.imageType == ARTICLE_THUMB}
            
            if thumb.count > 0 {
                
                let image = thumb[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case ACCOUNT_INSTA_LOGO:
            
            let thumb = self.imageList!.filter { $0.imageType == ACCOUNT_INSTA_LOGO}
            
            if thumb.count > 0 {
                
                let image = thumb[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        case INSTAGRAM_THUMB:
            
            let thumb = self.imageList!.filter { $0.imageType == INSTAGRAM_THUMB}
            
            if thumb.count > 0 {
                
                let image = thumb[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
            
        case INSTAGRAM_FULL:
            
            let thumb = self.imageList!.filter { $0.imageType == INSTAGRAM_FULL}
            
            if thumb.count > 0 {
                
                let image = thumb[0]
                return image.imageURL!
            }
            else {
                
                return ""
            }
            
        default:
            return ""
        }
    }
    
    func getImageURLForType(_ imageType: Int, fromArray: [ImageList]?) -> String? {
        
        if fromArray == nil {
            
            return ""
        }
        
        let logo = fromArray!.filter { $0.imageType == imageType}
        
        if logo.count > 0 {
            
            let image = logo[0]
            return image.imageURL!
        }
        else {
            
            return ""
        }
    }
    
    
    // MARK:- Get Instagram Images
    func getInstagramImagesArray(_ isThumb: Bool) -> [AnyObject]? {
        
        let full = self.imageList?.filter { $0.imageType == INSTAGRAM_FULL}
        let thumb = self.imageList?.filter { $0.imageType == INSTAGRAM_THUMB}
        
        if isThumb == false && full?.count > 0 {
            
            return full
        }
        else {
            
            return thumb
        }
    }
    
    
    // MARK:- Find Beacon Rules
    func findBeaconRuleFromBeacon(_ beacon:Beacon,proximity:NSNumber) -> BeaconRules? {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.writeToTextFile(with: "beacon Rulelist count ::: \(self.beaconRulesList?.count)", name: "Beacon_Trigeering")
        
        for beaconRule:BeaconRules in self.beaconRulesList! {
            
            if beaconRule.beaconID?.intValue == beacon.beaconID?.intValue && beaconRule.proximity!.intValue == beacon.ibeacon?.lastProximity?.intValue {
                
                return beaconRule
            }
            else {
                appDelegate.writeToTextFile(with: "\(self.title) ID: \(self.campaignActivityID!) does not met the condition for triggering ::: beaconRule.beaconID?.intValue = \(beaconRule.beaconID?.intValue)  ::: beacon.beaconID?.intValue = \(beacon.beaconID?.intValue)", name: "Beacon_Trigeering")
            }
        }
        
        
        
        return nil
    }
    
    func isCampaignActivityValid() -> ValidationType {
        
        //Article, BlogPost and Events
        if campaignActivityType?.intValue == CampaignActivityType.article.rawValue || campaignActivityType?.intValue == CampaignActivityType.blogPost.rawValue  {
            
            if self.title == nil || self.title!.isEmpty {
                return ValidationType(isValid:false, message:"Title cannot be blank")
            }
            else if self.endDateTime?.doubleValue < self.startDateTime?.doubleValue {
                return ValidationType(isValid:false, message:"End date should be greater than or equal to start date of article")
            }
            
            if self.IsPinToTop == true && AdminUserManager.sharedInstance.checkIfUserIsSuperAdmin() {
                if self.PinToTopEnd != nil && self.PinToTopEnd?.doubleValue < self.PinToTopStart?.doubleValue {
                    return ValidationType(isValid:false, message:"Pin end date should be greater than or equal to pin start date of article")
                }
                else if self.PinToTopStart != nil && self.PinToTopStart?.doubleValue < self.startDateTime?.doubleValue {
                    return ValidationType(isValid:false, message:"Pin start date should be greater than or equal to start date of article")
                }
                else if self.PinToTopEnd != nil && self.PinToTopEnd?.doubleValue > self.endDateTime?.doubleValue {
                    return ValidationType(isValid:false, message:"Pin end date should be lesser than or equal to end date of article")
                }
                else if self.PinToTopEnd != nil && self.PinToTopEnd?.doubleValue < self.startDateTime?.doubleValue {
                    return ValidationType(isValid:false, message:"Pin end date should be greater than or equal to start date of article")
                }
            }
            
        }
        else if campaignActivityType?.intValue == CampaignActivityType.event.rawValue {
            if self.title == nil || self.title!.isEmpty {
                return ValidationType(isValid:false, message:"Title cannot be blank")
            }
            else if self.endDateTime?.doubleValue < self.startDateTime?.doubleValue {
                return ValidationType(isValid:false, message:"End date should be greater than or equal to start date of event")
            }
            
            if self.IsPinToTop == true && AdminUserManager.sharedInstance.checkIfUserIsSuperAdmin() {
                if self.PinToTopEnd != nil && self.PinToTopEnd?.doubleValue < self.PinToTopStart?.doubleValue {
                    return ValidationType(isValid:false, message:"Pin end date should be greater than or equal to pin start date of event")
                }
                else if self.PinToTopStart != nil && self.PinToTopStart?.doubleValue < self.startDateTime?.doubleValue {
                    return ValidationType(isValid:false, message:"Pin start date should be greater than or equal to start date of event")
                }
                else if self.PinToTopEnd != nil && self.PinToTopEnd?.doubleValue > self.endDateTime?.doubleValue {
                    return ValidationType(isValid:false, message:"Pin end date should be lesser than or equal to end date of event")
                }
                else if self.PinToTopEnd != nil && self.PinToTopEnd?.doubleValue < self.startDateTime?.doubleValue {
                    return ValidationType(isValid:false, message:"Pin end date should be greater than or equal to start date of event")
                }
            }
        }
        else { //All Offer Type
            
            if campaignActivityType?.intValue != CampaignActivityType.longTermOffer.rawValue && (self.offerText == nil || self.offerText!.isEmpty) {
                return ValidationType(isValid:false, message:"Offer tile title text cannot be blank")
            }
            else if self.title == nil || self.title!.isEmpty {
                return ValidationType(isValid:false, message:"Title cannot be blank")
            }
            else if self.endDateTime?.doubleValue < self.startDateTime?.doubleValue {
                return ValidationType(isValid:false, message:"End date should be greater than or equal to start date")
            }
            
            if campaignActivityType?.intValue == CampaignActivityType.textOffer.rawValue {
                
                if self.offerTitle == nil || self.offerTitle!.isEmpty {
                    return ValidationType(isValid:false, message:"offer tile title text cannot be empty")
                }
            }else if campaignActivityType?.intValue == CampaignActivityType.longTermOffer.rawValue {
                //No field
                
            }else if campaignActivityType?.intValue == CampaignActivityType.kronerPriceOffer.rawValue {
                
                if self.offerKronerPrice != nil {
                    if self.offerKronerPrice?.floatValue > 99999.99 || self.offerKronerPrice?.floatValue < 0 {
                        return ValidationType(isValid:false, message:"Kroner price must be between 0 through 99 999.99 with 2 decimal points")
                    }
                }
                else {
                    return ValidationType(isValid:false, message:"Kroner price cannot be blank")
                }
            }else if campaignActivityType?.intValue == CampaignActivityType.receiveQuantityOffer.rawValue {
                
                if self.offerReceiveQuantity != nil {
                    if self.offerReceiveQuantity?.intValue > 99 || self.offerReceiveQuantity?.intValue < 0 {
                        return ValidationType(isValid:false, message:"Recieve quantity must be between 0 and 99")
                    }
                }
                else {
                    return ValidationType(isValid:false, message:"Bundle receive quantity cannot be blank")
                }
                
                if self.offerRatioWord != nil {
                    if self.offerRatioWord!.characters.count == 0 {
                        return ValidationType(isValid:false, message:"Bundle ratio Word cannot be blank")
                    }
                }
                else{
                    return ValidationType(isValid:false, message:"Bundle ratio word cannot be blank")
                }
                
                if self.offerPayForQuantity != nil {
                    if self.offerPayForQuantity?.intValue > 99 || self.offerPayForQuantity?.intValue < 0  {
                        return ValidationType(isValid:false, message:"Pay for quantity must be between 0 and 99")
                    }
                }
                else {
                    return ValidationType(isValid:false, message:"Bundle pay for quantity cannot be blank")
                }
                
            }else if campaignActivityType?.intValue == CampaignActivityType.percentageDiscountOffer.rawValue {
                
                if  self.offerPercentageDiscount != nil {
                    if self.offerPercentageDiscount?.intValue > 99 || self.offerPercentageDiscount?.intValue < -99 {
                        return ValidationType(isValid:false, message:"Percentage discount must be between -99 and 99")
                    }
                }
                else {
                    return ValidationType(isValid:false, message:"Percentage discount cannot be blank")
                }
                
            }else if campaignActivityType?.intValue == CampaignActivityType.kronerDiscountOffer.rawValue {
                
                if self.offerKronerDiscount != nil {
                    if self.offerKronerDiscount?.floatValue > 99999.99 || self.offerKronerDiscount?.floatValue < -99999.99 {
                        return ValidationType(isValid:false, message:"Kroner discount must be between -99 999.99 through 99 999.99 with 2 decimal points")
                    }
                }
                else {
                    return ValidationType(isValid:false, message:"Kroner discount cannot be blank")
                }
            }
            
            if self.IsPinToTop == true && AdminUserManager.sharedInstance.checkIfUserIsSuperAdmin(){
                if self.PinToTopEnd != nil && self.PinToTopEnd?.doubleValue < self.PinToTopStart?.doubleValue {
                    return ValidationType(isValid:false, message:"Pin end date should be greater than or equal to pin start date of offer")
                }
                else if self.PinToTopStart != nil && self.PinToTopStart?.doubleValue < self.startDateTime?.doubleValue {
                    return ValidationType(isValid:false, message:"Pin start date should be greater than or equal to start date of offer")
                }
                else if self.PinToTopEnd != nil && self.PinToTopEnd?.doubleValue > self.endDateTime?.doubleValue {
                    return ValidationType(isValid:false, message:"Pin end date should be lesser than or equal to end date of offer")
                }
                else if self.PinToTopEnd != nil && self.PinToTopEnd?.doubleValue < self.startDateTime?.doubleValue {
                    return ValidationType(isValid:false, message:"Pin end date should be greater than or equal to start date of offer")
                }
            }
            //            if self.RedemptionLimitPerPerson?.integerValue > 99999 ||  self.RedemptionLimitPerPerson?.integerValue < 0{
            //                return ValidationType(isValid:false, message:"User redemption limit must be between 0 and 99 999")
            //            }
            //            else if self.RedemptionLimitTotal?.integerValue > 99999 ||  self.RedemptionLimitTotal?.integerValue  < 0{
            //                return ValidationType(isValid:false, message:"Total redemption limit must be between 0 and 99 999")
            //            }
        }
        return ValidationType(isValid:true, message:"Is valid")
    }
    
    
    
    func copy(with zone: NSZone?) -> Any {
        
        let newValue = CampaignActivity()
        newValue.campaignActivityID = campaignActivityID
        newValue.campaignActivityType = campaignActivityType
        newValue.tileSize = tileSize
        newValue.title = title
        newValue.summary = summary
        newValue.descr = descr
        newValue.hTMLContent = hTMLContent
        newValue.url = url
        
        newValue.imageList = imageList
        
        newValue.notificationID = notificationID
        newValue.isFavorite = isFavorite
        newValue.postDate = postDate
        newValue.latitude =  latitude
        newValue.longitude = longitude
        newValue.startDateTime = startDateTime
        newValue.endDateTime = endDateTime
        newValue.eventAddress = eventAddress
        newValue.offerKronerDiscount = offerKronerDiscount
        newValue.offerReceiveQuantity = offerReceiveQuantity
        newValue.offerKronerPrice = offerKronerPrice
        newValue.offerRatioWord = offerRatioWord
        newValue.offerPercentageDiscount =  offerPercentageDiscount
        
        newValue.offerPayForQuantity = offerPayForQuantity
        newValue.offerTitle = offerTitle
        newValue.offerText = offerText
        newValue.offerType = offerType
        newValue.accountID = accountID
        newValue.accountAddress = accountAddress
        
        newValue.categoryID = categoryID
        
        newValue.accountName =  accountName
        newValue.facilitiesSymbol =  facilitiesSymbol
        newValue.facilitiesBoldTitle = facilitiesBoldTitle
        newValue.facilitiesRegularTitle = facilitiesRegularTitle
        newValue.weatherName =  weatherName
        newValue.weatherWindDirection = weatherWindDirection
        newValue.weatherTemperature =  weatherTemperature
        newValue.weatherWindSpeed = weatherWindSpeed
        newValue.weatherVar = weatherVar
        
        newValue.IsForUnder20 = IsForUnder20
        newValue.IsForOver20 = IsForOver20
        newValue.IsTargetAudienceMale = IsTargetAudienceMale
        newValue.IsTargetAudienceFemale = IsTargetAudienceFemale
        newValue.IsPinToTop = IsPinToTop
        newValue.ShowAfterOnBoarding = ShowAfterOnBoarding
        newValue.ShowInFeed = ShowInFeed
        newValue.IsShowInAccountFeed = IsShowInAccountFeed
        newValue.SendPushNotification = SendPushNotification
        newValue.Status = Status
        newValue.PinToTopStart = PinToTopStart
        newValue.PinToTopEnd = PinToTopEnd
        newValue.RedemptionLimit = RedemptionLimit
        newValue.RedemptionLimitTotal = RedemptionLimitTotal
        newValue.RedemptionLimitPerPerson = RedemptionLimitPerPerson
        newValue.plainTextArticleEvent = plainTextArticleEvent
        newValue.merchantId = merchantId
        newValue.IsApproved = IsApproved
        newValue.isReviewed = isReviewed
        newValue.IsCampaignExpired = IsCampaignExpired
        if beaconRulesList != nil {
            newValue.beaconRulesList = [BeaconRules].from(jsonArray:beaconRulesList!.toJSONArray()!)
        }
        newValue.Interests = Interests
        newValue.OfferRedeemded = OfferRedeemded
        
        return newValue
    }
    
    
}
