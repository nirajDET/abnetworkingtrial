//
//  ConciergeConversationModel.swift
//  Din Aker Brygge
//
//  Created by Rahul on 7/15/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import UIKit
import Gloss

enum MessageSenderType : Int {
    case user
    case admin
}

struct ConciergeConversationModel: Glossy {

    var MessageID: NSNumber?
    var MessageTime: NSNumber?
    var Text: String?
    var Image: String?
    var SenderType: MessageSenderType?
    
    init(SenderType:MessageSenderType, Text: String?, Image:String?, Date:NSNumber) {
        
        self.SenderType = SenderType
        self.Text = Text
        self.Image = Image
        self.MessageTime = Date
    }
    
    // MARK:- Deserialization
    init?(json: JSON) {
        
        self.MessageID = "MessageID" <~~ json
        self.MessageTime = "MessageTime" <~~ json
        self.Text = "Text" <~~ json
        self.Image = "Image" <~~ json
        self.SenderType = "SenderType" <~~ json
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        return jsonify([
            "MessageID" ~~> self.MessageID,
            "MessageTime" ~~> self.MessageTime,
            "Text" ~~> self.Text,
            "Image" ~~> self.Image,
            "SenderType" ~~> self.SenderType
            ])
    }
}
