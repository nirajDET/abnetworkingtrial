//
//  ConciergeRequestModel.swift
//  Din Aker Brygge
//
//  Created by Rahul on 7/14/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import UIKit
import Gloss

@objc class ConciergeRequestModel:NSObject, Glossy {

    var RequestID: NSNumber!
    var CategoryID: NSNumber?
    var CategoryName: String?
    var SubCategoryID: NSNumber?
    var SubCategoryName: String?
    var AccountID: NSNumber!
    var AccountName: String?
    var FirstName: String?
    var LastName: String?
    var MobilePhone: String?
    var EmailID: String?
    var TotalUnreadConversation: Int!
    var TotalResponse: Int?
    var ReadStatus: Bool?
    var Date:Double!
    var Text: String?
    var Image: String?
    var Requests:  Array<RequestContentModel>?
    var Responses:  Array<ConciergeResponseModel>?
    var Conversations:  Array<ConciergeConversationModel>?
    var imageList: Array<ImageList>?
    var SenderType: MessageSenderType?
    var IsLastPage: Bool!
    var unReadCount: NSNumber?
    override init() {
    
        super.init()
        
    }
    
    // MARK:- Deserialization
    required init?(json: JSON) {
        
        self.RequestID = "RequestID" <~~ json
        self.CategoryID = "CategoryID" <~~ json
        self.CategoryName = "CategoryName" <~~ json
        self.SubCategoryID = "SubCategoryID" <~~ json
        self.SubCategoryName = "SubCategoryName" <~~ json
        self.AccountID = "AccountID" <~~ json
        self.AccountName = "AccountName" <~~ json
        self.FirstName = "FirstName" <~~ json
        self.LastName = "LastName" <~~ json
        self.MobilePhone = "MobilePhone" <~~ json
        self.EmailID = "EmailID" <~~ json
        self.TotalUnreadConversation = "TotalUnreadConversation" <~~ json
        self.TotalResponse = "TotalResponse" <~~ json
        self.ReadStatus = "ReadStatus" <~~ json
        self.Date = "Date" <~~ json
        self.Text = "Text" <~~ json
        self.Image = "Image" <~~ json
        self.SenderType = "SenderType" <~~ json
        self.IsLastPage = "IsLastPage" <~~ json
        
        self.unReadCount = "UnReadCount" <~~ json
        
        if self.unReadCount == nil {
            self.unReadCount = NSNumber(value: 0)
        }
        
        
        if (json["Requests"] != nil) {
            
            self.Requests = [RequestContentModel].from(jsonArray:("Requests" <~~ json)!)
        }
        if (json["Responses"] != nil) {
            
            self.Responses = [ConciergeResponseModel].from(jsonArray:("Responses" <~~ json)!)
        }
        if (json["Conversations"] != nil) {
        
            self.Conversations = [ConciergeConversationModel].from(jsonArray:("Conversations" <~~ json)!)
        }
        if (json["ImageList"] != nil) {
            
            self.imageList = [ImageList].from(jsonArray:("ImageList" <~~ json)!)
        }
        
        if self.imageList == nil {
            self.imageList = []
        }
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        return jsonify([
            "RequestID" ~~> self.RequestID,
            "CategoryID" ~~> self.CategoryID,
            "CategoryName" ~~> self.CategoryName,
            "SubCategoryID" ~~> self.SubCategoryID,
            "SubCategoryName" ~~> self.SubCategoryName,
            "AccountID" ~~> self.AccountID,
            "AccountName" ~~> self.AccountName,
            "FirstName" ~~> self.FirstName,
            "LastName" ~~> self.LastName,
            "MobilePhone" ~~> self.MobilePhone,
            "EmailID" ~~> self.EmailID,
            "TotalUnreadConversation" ~~> self.TotalUnreadConversation,
            "TotalResponse" ~~> self.TotalResponse,
            "ReadStatus" ~~> self.ReadStatus,
            "Date" ~~> self.Date,
            "Text" ~~> self.Text,
            "Image" ~~> self.Image,
            "SenderType" ~~> self.SenderType,
            "Requests" ~~> self.Requests,
            "Responses" ~~> self.Responses,
            "Conversations" ~~> self.Conversations,
            "ImageList" ~~> self.imageList,
            "IsLastPage" ~~> self.IsLastPage
            ])
    }
    
    func getAccountImageFor(_ type: Int) -> String? {
        
        if self.imageList != nil {
            
            for imageList:ImageList in self.imageList! {
                
                if imageList.imageType == type {
                    
                    return imageList.imageURL
                }
            }
        }
        
        return nil
    }
}
