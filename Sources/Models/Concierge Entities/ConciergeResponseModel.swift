//
//  ConciergeResponseModel.swift
//  Din Aker Brygge
//
//  Created by Rahul on 7/19/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import UIKit
import Gloss

struct ConciergeResponseModel: Glossy {

    var ResponseID: NSNumber?
    var AccountID: NSNumber!
    var AccountName: String?
    var ReadStatus: Bool?
    var Description: String?
    
    
    // MARK:- Deserialization
    init?(json: JSON) {
        
        self.ResponseID = "ResponseID" <~~ json
        self.AccountID = "AccountID" <~~ json
        self.AccountName = "AccountName" <~~ json
        self.ReadStatus = "ReadStatus" <~~ json
        self.Description = "Description" <~~ json
    }
    
    // MARK: ObjectToJSON
    func toJSON() -> JSON? {
        
        return jsonify([
            
            "ResponseID" ~~> self.ResponseID,
            "AccountID" ~~> self.AccountID,
            "AccountName" ~~> self.AccountName,
            "ReadStatus" ~~> self.ReadStatus,
            "Description" ~~> self.Description
        ])
    }
}
