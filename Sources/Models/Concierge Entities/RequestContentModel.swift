//
//  RequestContentModel.swift
//  Din Aker Brygge
//
//  Created by Rahul on 7/15/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import UIKit
import Gloss

struct RequestContentModel: Glossy {

    var Text: String?
    var Image: String?
    
    // MARK:- Deserialization
    init?(json: JSON) {

        self.Text = "Text" <~~ json
        self.Image = "Image" <~~ json
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        return jsonify([
            "Text" ~~> self.Text,
            "Image" ~~> self.Image,
            ])
    }
}
