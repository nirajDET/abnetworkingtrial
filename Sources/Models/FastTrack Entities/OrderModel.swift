//
//  OrderModel.swift
//  Din Aker Brygge
//
//  Created by Rahul Chandera on 22/06/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Gloss

class OrderModel:NSObject, Glossy {

    var OrderID: NSNumber!
    var AccountID: NSNumber!
    var AccountName: String?
    var AccountLogo: String?
    var OrderDate: NSNumber!
    var PickUpImage: String?
    var AccountEmail: String?
    var TotalPrice: NSNumber!
    var Products: Array<OrderProductModel>?
    var ETA:NSNumber!
    var FirstName: String?
    var LastName: String?
    var MobilePhone: String?
    var EmailID: String?
    var note: String?
    var orderStatus:NSNumber?
    var isOpen:NSNumber?
    var openingHoursTitle:String?
    var phone:String?
    var webSite:String?
    var bookTable:String?
    
    override init() {
        
        super.init()
        
    }
    
    // MARK:- Deserialization
    required init?(json: JSON) {
        
        self.OrderID = "OrderID" <~~ json
        self.AccountID = "AccountID" <~~ json
        self.AccountName = "AccountName" <~~ json
        self.OrderDate = "OrderDate" <~~ json
        self.PickUpImage = "PickupImage" <~~ json
        self.AccountEmail = "AccountEmail" <~~ json
        self.Products = [OrderProductModel].from(jsonArray:("Products" <~~ json)!)
        self.AccountLogo = "AccountLogo" <~~ json
        self.TotalPrice = "TotalPrice" <~~ json
        self.FirstName = "FirstName" <~~ json
        self.LastName = "LastName" <~~ json
        self.MobilePhone = "MobilePhone" <~~ json
        self.EmailID = "EmailID" <~~ json
        self.ETA = "ETA" <~~ json
        self.note = "Note" <~~ json
        self.orderStatus = "OrderStatus" <~~ json
        self.isOpen = "IsOpen" <~~ json
        self.openingHoursTitle = "OpeningHoursTitle" <~~ json
        self.phone = "Phone" <~~ json
        self.webSite = "WebSite" <~~ json
        self.bookTable = "BookTable" <~~ json
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        return jsonify([
            "OrderID" ~~> self.OrderID,
            "AccountID" ~~> self.AccountID,
            "AccountName" ~~> self.AccountName,
            "OrderDate" ~~> self.OrderDate,
            "PickupImage" ~~> self.PickUpImage,
            "AccountEmail" ~~> self.AccountEmail,
            "Products" ~~> self.Products,
            "AccountLogo" ~~> self.AccountLogo,
            "TotalPrice" ~~> self.TotalPrice,
            "OrderStatus" ~~> self.orderStatus,
            "Note" ~~> self.note,
            "FirstName" ~~> self.FirstName,
            "LastName" ~~> self.LastName,
            "MobilePhone" ~~> self.MobilePhone,
            "EmailID" ~~> self.EmailID,
            "ETA" ~~> self.ETA
            ])
    }
    
    func getProductsInfo() -> String {
    
        var products:String = ""
        for item in self.Products! {
            
            if products.characters.count == 0 {
                
                products = (item.ProductName)!
            }
            else {
            
                products = products + ", " + (item.ProductName)!
            }
        }

        return products
    }
}
