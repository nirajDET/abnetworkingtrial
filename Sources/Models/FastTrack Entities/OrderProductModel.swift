//
//  OrderProductModel.swift
//  Din Aker Brygge
//
//  Created by Paresh Navadiya on 29/06/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Gloss

struct OrderProductModel: Glossy {

    var ProductID: NSNumber?
    var ProductName: String?
    var VariantID:NSNumber?
    var VariantName: String?
    var Price: NSNumber!
    var Quantity: NSNumber?
    var PickUpImage: String?
    var Note: String?
    var ModifierGroups: Array<ProductModifierGroupModel>?
    
    // MARK:- Deserialization
    init?(json: JSON) {
        
        self.ProductID = "ProductID" <~~ json
        self.ProductName = "ProductName" <~~ json
        
//        self.ImageURL = "ImageURL" <~~ json
//        self.Description = "Description" <~~ json
//        self.Allergens = "Allergens" <~~ json
        
        self.VariantID = "VariantID" <~~ json
        self.VariantName = "VariantName" <~~ json
        self.Price = "Price" <~~ json
        self.Quantity = "Quantity" <~~ json
        self.PickUpImage = "PickUpImage" <~~ json
        self.Note = "Note" <~~ json
        self.ModifierGroups = [ProductModifierGroupModel].from(jsonArray:("ModifierGroups" <~~ json)!)
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        return jsonify([
            "ProductID" ~~> self.ProductID,
            "ProductName" ~~> self.ProductName,
            "VariantID" ~~> self.VariantID,
            "VariantName" ~~> self.VariantName,
            "Price" ~~> self.Price,
            "Quantity" ~~> self.Quantity,
            "PickUpImage" ~~> self.PickUpImage,
            "Note" ~~> self.Note,
            "ModifierGroups" ~~> self.ModifierGroups
        ])
    }
    
    func getProductsInfo() -> String {
        
        var products:String = self.VariantName!
        for group in self.ModifierGroups! {
            
            for modifier in group.Modifiers! {
                
                products = products + ", " + modifier.Name!
            }
        }
        return products
    }
}
