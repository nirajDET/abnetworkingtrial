//
//  ProductCategoryModel.swift
//  Din Aker Brygge
//
//  Created by Rahul Chandera on 22/06/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Gloss

struct ProductCategoryModel: Glossy {

    var CategoryID: NSNumber?
    var Name: String?
    var ImageURL: String?
    var Products: Array<ProductModel>?
    
    // MARK:- Deserialization
    init?(json: JSON) {
        
        self.CategoryID = "CategoryID" <~~ json
        self.Name = "Name" <~~ json
        self.ImageURL = "ImageURL" <~~ json
        self.Products = [ProductModel].from(jsonArray:("Products" <~~ json)!)
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        return jsonify([
            "CategoryID" ~~> self.CategoryID,
            "Name" ~~> self.Name,
            "ImageURL" ~~> self.ImageURL,
            "Products" ~~> self.Products
            ])
    }
}
