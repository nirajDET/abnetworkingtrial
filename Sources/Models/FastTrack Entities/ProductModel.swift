//
//  ProductModel.swift
//  Din Aker Brygge
//
//  Created by Rahul Chandera on 22/06/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Gloss
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


func ==(lhs:ProductModel,rhs:ProductModel) -> Bool {
    
    
    
    if lhs.ProductID == rhs.ProductID  && lhs.Name == rhs.Name && lhs.ImageURL == rhs.ImageURL && lhs.Description == rhs.Description && lhs.Allergens == rhs.Allergens && lhs.VariantLabel == rhs.VariantLabel  && lhs.Variants! == rhs.Variants!  && lhs.CategoryID == rhs.CategoryID && lhs.Status == rhs.Status  {
        if lhs.ModifierGroups!.count != rhs.ModifierGroups!.count {
            return false
        }
        for (index,element) in lhs.ModifierGroups!.enumerated() {
            if element != rhs.ModifierGroups![index] {
                return false
            }
        }
        return true
        
    }
    return false
}

class ProductModel: Glossy,Equatable {

    var ProductID: NSNumber?
    var Name: String?
    var ImageURL: String?
    var Description:String?
    var Allergens:String?
    var VariantLabel:String?
    var Variants: Array<ProductVariantModel>?
    var ModifierGroups: Array<ProductModifierGroupModel>?
    var CategoryID:NSNumber?
    var Status:NSNumber?
    
    
    @objc func copyWithZone(_ zone: NSZone?) -> AnyObject {
        let copy = ProductModel(pID: ProductID, name: Name, imageURL: ImageURL, Descr: Description, aller: Allergens, varLabel: VariantLabel, variants: Variants, modifierGroups: ModifierGroups!, cateId: CategoryID, status: Status)
        return copy
    }
    
    
    init(pID:NSNumber?,name:String?,imageURL:String?,Descr:String?,aller:String?,varLabel:String?,variants:[ProductVariantModel]?,modifierGroups:[ProductModifierGroupModel],cateId:NSNumber?,status:NSNumber?){
        self.ProductID = pID
        self.Name = name
        self.ImageURL = imageURL
        self.Description = Descr
        self.Allergens = aller
        self.VariantLabel = varLabel
        self.Variants = variants
        self.ModifierGroups = modifierGroups
        self.CategoryID = cateId
        self.Status = status
        
        
    }
    
    init() {
        self.Variants = []
        self.ModifierGroups = []
        self.Status = NSNumber(value: false)
        
    }
    
    func isProductDataValid() -> ValidationType {
        if self.Name == nil {
            return ValidationType(isValid:false, message:"Product's Name cannot be empty")
        }//self.categoryPickerView.selectRow(1, inComponent: 0, animated: false)
        if self.Name!.isEmpty {
            return ValidationType(isValid:false, message:"Product's Name cannot be empty")
        }
        if self.Variants == nil || self.Variants?.count == 0 {
            return ValidationType(isValid:false, message:"Please select a default variant.")
        }
        if self.CategoryID == nil || self.CategoryID?.intValue == 0 {
            return ValidationType(isValid:false, message:"Please select a category.")

        }
        
        
        return ValidationType(isValid:true, message:"Product is valid.")
    }
    
    // MARK:- Deserialization
    required init?(json: JSON) {
        
        self.ProductID = "ProductID" <~~ json
        self.Name = "Name" <~~ json
        self.ImageURL = "ImageURL" <~~ json
        self.Description = "Description" <~~ json
        self.Allergens = "Allergens" <~~ json
        self.VariantLabel = "VariantLabel" <~~ json
        self.Variants = [ProductVariantModel].from(jsonArray:("Variants" <~~ json)!)
        self.ModifierGroups = [ProductModifierGroupModel].from(jsonArray:("ModifierGroups" <~~ json)!)
        self.CategoryID = "CategoryID" <~~ json
        self.Status = "Status" <~~ json
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        return jsonify([
            "ProductID" ~~> self.ProductID,
            "Name" ~~> self.Name,
            "ImageURL" ~~> self.ImageURL,
            "Description" ~~> self.Description,
            "Allergens" ~~> self.Allergens,
            "VariantLabel" ~~> self.VariantLabel,
            "Variants" ~~> self.Variants,
            "ModifierGroups" ~~> self.ModifierGroups,
            "CategoryID" ~~> self.CategoryID,
            "Status" ~~> self.Status
        ])
    }
    
    
    func getProductPrice() -> NSNumber {
        var productPrice:Float = 0
        
      
        
        if Variants != nil {
            let filteredArrProductVariants:Array<ProductVariantModel> = Variants!.filter({ $0.IsDefault == true })
            
            if filteredArrProductVariants.count > 0 {
                
                let productVariant:ProductVariantModel  = filteredArrProductVariants[0]
                let tempProductPrice:NSNumber? = productVariant.Price
                
                if tempProductPrice != nil {
                    productPrice = productPrice +  Float(tempProductPrice!)
                }
            }
        }
        
        if self.ModifierGroups != nil {
            for productModifierGroupModel in self.ModifierGroups!{
                
                if  productModifierGroupModel.Modifiers != nil {
                    let filteredArrProductgroupModifier:Array<ProductModifierModel> = productModifierGroupModel.Modifiers!.filter({
                        Bool($0.IsDefault! as NSNumber) == true
                    })
                    
                    if filteredArrProductgroupModifier.count > 0 {
                        
                        for productModifierModel in filteredArrProductgroupModifier{
                            let tempProductPrice:NSNumber? = productModifierModel.Price
                            if tempProductPrice != nil {
                                productPrice = productPrice + Float(tempProductPrice!)
                            }
                        }
                    }
                }
                
            }
        }
        return NSNumber(value: productPrice)
    }
    
    
    func getProductVariantModifierDescr(_ doNotConciderSingleVariant: Bool = false) -> String {
        
        var productItems:String = ""
        
        if self.Variants != nil {
            
            if doNotConciderSingleVariant == false {
                
                productItems = getVariantsString()
            }
            else if self.Variants?.count > 1 {
            
                productItems = getVariantsString()
            }
        }

        if self.ModifierGroups != nil {
            
            for productModifierGroupModel in self.ModifierGroups!{
                
                if  productModifierGroupModel.Modifiers != nil {
                    let filteredArrProductgroupModifier:Array<ProductModifierModel> = productModifierGroupModel.Modifiers!.filter({
                        Bool($0.IsDefault! as NSNumber) == true
                    })
                    
                    if filteredArrProductgroupModifier.count > 0 {
                        
                        for productModifierModel in filteredArrProductgroupModifier {
                            
                            var tempProductName:String? = productModifierModel.Name
                            
                            if tempProductName != nil
                            {
                                tempProductName = tempProductName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                                if productItems.characters.count == 0 {
                                    productItems = tempProductName!
                                }
                                else {
                                
                                    productItems = productItems + ", \(tempProductName!)"
                                }
                            }
                        }
                    }
                }
            }
        }
        return productItems
    }
    
    func getVariantsString() -> String {
        
        var productItems:String = ""
        
        let filteredArrProductVariants:Array<ProductVariantModel> = self.Variants!.filter({ $0.IsDefault == true })
        
        if filteredArrProductVariants.count > 0 {
            
            let productVariant:ProductVariantModel  = filteredArrProductVariants[0]
            var tempProductName:String? = productVariant.Name
            if tempProductName != nil {
                tempProductName = tempProductName?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                productItems = productItems + tempProductName!
            }
        }
        
        return productItems
    }
   
    
}
