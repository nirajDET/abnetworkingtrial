//
//  ProductModifierGroupModel.swift
//  Din Aker Brygge
//
//  Created by Rahul Chandera on 22/06/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Gloss

func ==(lhs:ProductModifierGroupModel,rhs:ProductModifierGroupModel) -> Bool {
    if lhs.ModifierGroupID == rhs.ModifierGroupID  && lhs.Title == rhs.Title && lhs.MinSelection == rhs.MinSelection && lhs.MaxSelection == rhs.MaxSelection && lhs.Modifiers! == rhs.Modifiers! {
        if lhs.Modifiers!.count != rhs.Modifiers!.count {
            return false
        }
        for (index,element) in lhs.Modifiers!.enumerated() {
            if element != rhs.Modifiers![index] {
                return false
            }
        }

    }
    return false
}

func !=(lhs: ProductVariantModel, rhs: ProductVariantModel) -> Bool {
    return !(lhs == rhs)
}

class ProductModifierGroupModel: Glossy,Equatable {

    var ModifierGroupID: NSNumber?
    var Title: String?
    var MinSelection: NSNumber?
    var MaxSelection: NSNumber?
    var Modifiers: Array<ProductModifierModel>?
    
    // MARK:- Deserialization
    required init?(json: JSON) {
        
        self.ModifierGroupID = "ModifierGroupID" <~~ json
        self.Title = "Title" <~~ json
        self.MinSelection = "MinSelection" <~~ json
        self.MaxSelection = "MaxSelection" <~~ json
        self.Modifiers = [ProductModifierModel].from(jsonArray:("Modifiers" <~~ json)!)
    }
    
    init() {
        self.ModifierGroupID = NSNumber(value: 0)
        self.Title = ""
        self.MinSelection = NSNumber(value: 0)
        self.MaxSelection = NSNumber(value: 1)
        self.Modifiers = []
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        return jsonify([
            "ModifierGroupID" ~~> self.ModifierGroupID,
            "Title" ~~> self.Title,
            "MinSelection" ~~> self.MinSelection,
            "MaxSelection" ~~> self.MaxSelection,
            "Modifiers" ~~> self.Modifiers
            ])
    }
    
    func isProductModifierGroupModelValid() -> ValidationType {
        if self.Title!.isEmpty {
            return (isValid:false,message:"Group Name, Min or Max cannot be blank.")
        }
        
        var selectedModifiersCount:Int = 0
        
                 
        
        for modifierModel:ProductModifierModel in self.Modifiers! {
            
            let validation:ValidationType = modifierModel.isProductModifierModelValid()
            if !validation.isValid {
                return validation
            }
            
            if modifierModel.IsDefault!.boolValue {
                selectedModifiersCount += 1
            }
        }
        
        if self.Modifiers!.count == 0 {
             return (isValid:false,message:"You must enter at least 1 option.")
        }
        
        if self.Modifiers!.count < (self.MinSelection?.intValue)! {
            return (isValid:false,message:"You must enter at least \(self.MinSelection?.intValue) options.")
        }
        
        if (self.MinSelection?.intValue)! > (self.MaxSelection?.intValue)! {
            return (isValid:false,message:"The Min value cannot be larger than the Max value.")
        }
        if selectedModifiersCount < (self.MinSelection?.intValue)! {
            return (isValid:false,message:"The number of Default options should be at least equal to Min number of options.")
        }
        if selectedModifiersCount > (self.MaxSelection?.intValue)! {
            //Ask Amy
            return (isValid:false,message:"The number of Default options should be less or equal to Max value.")
        }
        
        
         return (isValid:true,message:"Modifier group is valid.")
    }
    func isProductModifierGroupModelBlank() -> Bool {
        if !self.Title!.isEmpty {
            return false
        }
        for modifierModel:ProductModifierModel in self.Modifiers! {
            
            if !modifierModel.isProductModifierModelBlank() {
                return false
            }
        }
        
        return true
    }

    
}
