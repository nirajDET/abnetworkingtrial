//
//  ProductModifierModel.swift
//  Din Aker Brygge
//
//  Created by Rahul Chandera on 22/06/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Gloss

func ==(lhs:ProductModifierModel,rhs:ProductModifierModel) -> Bool {
    if lhs.ModifierGroupID == rhs.ModifierGroupID  && lhs.ModifierID == rhs.ModifierID && lhs.Name == rhs.Name && lhs.Price == rhs.Price && lhs.IsDefault == rhs.IsDefault {
        return true
    }
    return false
}



class ProductModifierModel: Glossy,Equatable {

    var ModifierGroupID: NSNumber?
    var ModifierID: NSNumber?
    var Name: String?
    var Price: NSNumber?
    var IsDefault: NSNumber?
    
    init() {
        self.ModifierGroupID = NSNumber(value: 0)
        self.ModifierID = NSNumber(value: 0)
        self.Name = ""
        self.Price = NSNumber(value: -1)
        self.IsDefault = NSNumber(value: false)
    }
    
    // MARK:- Deserialization
    required init?(json: JSON) {
        
        self.ModifierGroupID = "ModifierGroupID" <~~ json
        self.ModifierID = "ModifierID" <~~ json
        self.Name = "Name" <~~ json
        self.Price = "Price" <~~ json
        self.IsDefault = "IsDefault" <~~ json
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        return jsonify([
            "ModifierGroupID" ~~> self.ModifierGroupID,
            "ModifierID" ~~> self.ModifierID,
            "Name" ~~> self.Name,
            "Price" ~~> self.Price,
            "IsDefault" ~~> self.IsDefault
            ])
    }
    func isProductModifierModelValid() -> ValidationType {
        if self.Name!.isEmpty {
            return (isValid:false,message:"You must enter both an Option Name and a price.")
        }
        if (self.Price?.intValue)! < 0 {
            return (isValid:false,message:"You must enter both an Option Name and a price.")
        }
        
        return (isValid:true,message:"Modifier is valid")
    }
    func isProductModifierModelBlank() -> Bool {
        if !self.Name!.isEmpty {
            return false
        }
        if self.Price?.intValue != -1 {
            return false
        }
        
        return true
    }
}
