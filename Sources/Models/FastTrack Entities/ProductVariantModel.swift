//
//  ProductVariantModel.swift
//  Din Aker Brygge
//
//  Created by Rahul Chandera on 22/06/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Gloss

func ==(lhs:ProductVariantModel,rhs:ProductVariantModel) -> Bool {
    if lhs.VariantID == rhs.VariantID && lhs.Name == rhs.Name && lhs.Price == rhs.Price && lhs.IsDefault == rhs.IsDefault {
        return true
    }
    return false
}



struct ProductVariantModel: Glossy,Equatable {

    var VariantID: NSNumber?
    var Name: String?
    var Price: NSNumber?
    var IsDefault: NSNumber?
    
    
    // MARK:- Deserialization
    
    init() {
        self.VariantID = NSNumber(value: 0)
        self.Name = ""
        self.Price =  NSNumber(value: 0)
        self.IsDefault = NSNumber(value: false)
    }
    
    init?(json: JSON) {
        
        self.VariantID = "VariantID" <~~ json
        self.Name = "Name" <~~ json
        self.Price = "Price" <~~ json
        self.IsDefault = "IsDefault" <~~ json
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        return jsonify([
            "VariantID" ~~> self.VariantID,
            "Name" ~~> self.Name,
            "Price" ~~> self.Price,
            "IsDefault" ~~> self.IsDefault
            ])
    }
    func isProductVariantModelValid() -> ValidationType {
        if self.Name!.isEmpty {
            return (isValid:false,message:"You must enter both a Variant Name and a Price.")
        }
        if (self.Price?.intValue)! <= 0 {
            return (isValid:false,message:"You must enter both a Variant Name and a Price.")
        }
        return (isValid:true,message:"Variant is valid.")
    }
    func isProductVariantModelBlank() -> Bool {
        if !self.Name!.isEmpty {
            return false
        }
        if self.Price?.intValue != 0 {
            return false
        }
        
        return true
    }

}
