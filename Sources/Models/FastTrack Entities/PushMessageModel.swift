//
//  PushMessageModel.swift
//  Din Aker Brygge
//
//  Created by Rahul on 7/7/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import UIKit
import Gloss

struct PushMessageModel: Glossy {

    var PushType: NSNumber?
    var Id: NSNumber?
    var `Type`: NSNumber?
    var SubType: NSNumber?
    var Title: String?
    var OrderId: String?
    var Status: NSNumber?
    var AccountName: String?
    var AccountId: NSNumber?
    
    // MARK:- Deserialization
    init?(json: JSON) {
        
        self.PushType = "PushType" <~~ json
        self.Id = "Id" <~~ json
        self.Type = "Type" <~~ json
        self.SubType = "SubType" <~~ json
        self.Title = "Title" <~~ json
        self.OrderId = "OrderId" <~~ json
        self.Status = "Status" <~~ json
        self.AccountName = "AccountName" <~~ json
        self.AccountId = "AccountId" <~~ json
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        return jsonify([
            "PushType" ~~> self.PushType,
            "Id" ~~> self.Id,
            "Type" ~~> self.Type,
            "SubType" ~~> self.SubType,
            "Title" ~~> self.Title,
            "OrderId" ~~> self.OrderId,
            "Status" ~~> self.Status,
            "AccountName" ~~> self.AccountName,
            "AccountId" ~~> self.AccountId
            ])
    }
}
