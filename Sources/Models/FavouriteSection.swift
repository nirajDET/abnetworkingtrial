//
//  FavouriteSection.swift
//  Din Aker Brygge
//
//  Created by Mihir Mehta on 23/05/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Foundation

@objc class FavouriteSection: NSObject {
    var sectionName:String?
    var activities:Array<CampaignActivity>?
    
}