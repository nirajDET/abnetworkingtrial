//
//  ImageList.swift
//  Din Aker Brygge
//
//  Created by Paresh on 24/05/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Foundation
import CoreData
import Gloss

let ACCOUNTBLACK_LOGO = 0
let ACCOUNTWHITE_LOGO = 1
let ACCOUNTIMAGE_THUMB = 2
let ACCOUNTIMAGE_FULL = 3
let OFFERIMAGE_THUMB = 4
let OFFERIMAGE_FULL = 5
let INSTAGRAM_THUMB = 6
let INSTAGRAM_FULL = 7
let EVENTIMAGE_THUMB = 8
let EVENTIMAGE_FULL = 9
let CITYEXPERIENCE_THUMB = 10
let CITYEXPERIENCE_FULL = 11
let ARTICLE_THUMB = 12
let ARTICLE_FULL = 13
let ACCOUNT_INSTA_LOGO = 14
let ACCOUNT_PICKUP_IMAGE = 15

@objc enum ImageType : Int {
    case accountblack_LOGO = 0, accountwhite_LOGO = 1, accountimage_THUMB = 2, accountimage_FULL = 3, offerimage_THUMB = 4, offerimage_FULL = 5, instagram_THUMB = 6, instagram_FULL = 7, eventimage_THUMB = 8, eventimage_FULL = 9, cityexperience_THUMB = 10, cityexperience_FULL = 11, article_THUMB = 12, article_FULL = 13, account_INSTA_LOGO = 14
}


func ==(lhs:ImageList,rhs:ImageList) -> Bool {
    if lhs.title == rhs.title  && lhs.imageType == rhs.imageType && lhs.imageURL == rhs.imageURL {
        return true
    }
    return false
}

func !=(lhs:ImageList,rhs:ImageList) -> Bool {
    
    return !(lhs == rhs)
}




@objc(ImageList)
class ImageList:NSObject,Glossy {

// Insert code here to add functionality to your managed object subclass
    
    
     var imageType: Int?
     var imageURL: String?
     var title: String?
     var accountModel: AccountModel?
     var parentCampaignActivity: CampaignActivity?
    
    override init() {
        super.init()
    }
    
     required init?(json: JSON) {
        
        self.imageType = "ImageType" <~~ json
        self.imageURL = "ImageURL" <~~ json
        self.title = "Title" <~~ json
    }
    
    
    func toJSON() -> JSON? {
        return jsonify([
            "ImageType" ~~> self.imageType,
            "ImageURL" ~~> self.imageURL,
            "Title" ~~> self.title
            ])
    }


}
