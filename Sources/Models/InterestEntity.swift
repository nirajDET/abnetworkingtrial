//
//  InterestEntity.swift
//  Din Aker Brygge
//
//  Created by Mihir Mehta on 03/01/17.
//  Copyright © 2017 Dynamic Elements. All rights reserved.
//

import Foundation
import Gloss
public func ==(lhs: InterestEntity, rhs: InterestEntity) -> Bool {
    if lhs.interestID != rhs.interestID {
        return false
    }
    if lhs.name != rhs.name {
        return false
    }
    return true
}


public class InterestEntity: NSObject, NSCoding, Glossy {
    /// Returns a Boolean value indicating whether two values are equal.
    ///
    /// Equality is the inverse of inequality. For any values `a` and `b`,
    /// `a == b` implies that `a != b` is `false`.
    ///
    /// - Parameters:
    ///   - lhs: A value to compare.
    ///   - rhs: Another value to compare.
    

    var interestID:NSNumber?
    var name:String?
    
    required public init?(json: JSON) {
        self.interestID = "InterestID" <~~ json
        self.name = "Name" <~~ json
    }
    
    public func toJSON() -> JSON? {
        return jsonify([
            "InterestID" ~~> self.interestID,
            "Name" ~~> self.name
            ])
    }
    
    override public func isEqual(_ object: Any?) -> Bool {
        guard let user = object as? InterestEntity else { return false }
        return self == user
    }

    //  NJ

    public func encode(with encoder: NSCoder) {
        // Methods
        encoder.encode(interestID, forKey: "interestID")
        encoder.encode(name, forKey: "name")

    }
    
    required public init?(coder decoder: NSCoder) {
        // Methods
        interestID = decoder.decodeObject(forKey: "interestID") as? NSNumber
        name = decoder.decodeObject(forKey: "name") as? String

    }

    override init() {
        super.init()
    }
}
