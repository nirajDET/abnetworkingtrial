//
//  ParsingManager.swift
//  BeaconManager
//
//  Created by Mihir Mehta on 26/04/16.
//  Copyright © 2016 Mihir Mehta. All rights reserved.
//

import Foundation
import Gloss
import CoreData
import CoreLocation

func DLog(message: String, function: String = #function) {
    #if DEBUG
        print("\(message)")
    #endif
}

enum ParsingResult {
    case Success
    case Error(error:NSError)
}


func ==(lhs: ParsingResult, rhs: ParsingResult) -> Bool {
    
    switch(lhs,rhs) {
        
    case (.Success, .Success): return true
    case let (.Error(la), .Error(ra)): return la == ra
    default: return false
    }
    
}



@objc public class ParsingManager : NSObject{
    
    static let sharedInstance = ParsingManager()
    
    
    
    //MARK:- User
    func parseUserObject(dictionary:Dictionary<String,AnyObject>) -> UserEntity {
        
        let userModel = UserEntity()
        
        userModel.UserId =   ("UserID" <~~ dictionary)!
        userModel.DeviceID = ("DeviceID" <~~ dictionary)!
        userModel.ApiKey = "ApiKey" <~~ dictionary
        //userModel.Roles = [AdminRoleModel].fromJSONArray(("Roles" <~~ dictionary)!)
        userModel.Address = "Address" <~~ dictionary
        userModel.City = "City" <~~ dictionary
        userModel.Country = "Country" <~~ dictionary
        //userModel.AvatarUrl = "AvatarUrl" <~~ dictionary
        userModel.DOB = "Dob" <~~ dictionary
        userModel.Email = "EmailID" <~~ dictionary
        userModel.FirstName = "FirstName" <~~ dictionary
        userModel.LastName = "LastName" <~~ dictionary
        userModel.Gender = ("Gender" <~~ dictionary)!
        userModel.DoSubscribeNewsletter = ("IsNewsletterSubscribed" <~~ dictionary)!
        userModel.ContactNumber = "MobilePhone" <~~ dictionary
        //userModel.Password = "Password" <~~ dictionary
        userModel.PostalCode = "PostalCode" <~~ dictionary
        userModel.AccountID = "AccountID" <~~ dictionary
        
        if  dictionary["RelationWithAB"] != nil {
               userModel.relationWithAkerBrygge = ("RelationWithAB" <~~ dictionary)!
        }
        
     
        //userModel.Status = "Status" <~~ dictionary
        
        userModel.Interests = []
        var arrInterests:Array<InterestEntity> = [];
        let arrDataInterests:Array<Dictionary<String,AnyObject>>? =  "InterestList" <~~ dictionary
        if arrDataInterests != nil {
            for dict:Dictionary<String,AnyObject> in arrDataInterests! {
                
                arrInterests.append(self.parseInterestDictionary(dict))
            }
        }
        userModel.Interests = arrInterests
        
        return userModel
    }
    
    func parseInterestDictionary(dictionary:Dictionary<String,AnyObject>) -> InterestEntity {
        
        let interestModel = InterestEntity()
        
        interestModel.InterestID = ("InterestID" <~~ dictionary)!
        interestModel.Name = "Name" <~~ dictionary
        
        return interestModel
    }
    
    
    
    //MARK: Categories
    func parseCategories(categoryArray:Array<Dictionary<String,AnyObject>>,completionHandler:(arrCategories:Array<CategoryModel>) -> Void)
    {
        //remove all objects
        let backgroundContext:NSManagedObjectContext = CoreDataHelper.sharedInstance.getBackgroundManageObjectContext()
        do {
            try CategoryModel.removeAllInContext(CoreDataHelper.sharedInstance.getMainManageObjectContext())
            try SubCategoryModel.removeAllInContext(CoreDataHelper.sharedInstance.getMainManageObjectContext())
            
            try backgroundContext.saveContextAndWait()
        }
        catch let error as NSError  {
            DLog("Error while removing CategoryModel and SubCategoryModel : \(error)");
        }

        //now continue
        let appManager:ApplicationManager = ApplicationManager.sharedInstance();
        appManager.storeCategoriesToLocal(categoryArray)
        appManager.arCategory = []
        
        let userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        self.parseCategoriesFromArray(categoryArray) { (result, categories) in
            
            if result == .Success {
                
                let arrSortedCategories : Array<CategoryModel> = categories.sort({$0.categoryID!.intValue < $1.categoryID!.intValue})
                print("arrSortedCategories\(arrSortedCategories)");
                appManager.arCategory = NSMutableArray(array:arrSortedCategories)
                
                completionHandler(arrCategories:arrSortedCategories)
                
                userDefaults.setObject(NSDate(), forKey: USER_DEFAULT_BUS_OWNER_FETCH_DATE)
                DLog("parseAllCategories count \(appManager.arCategory.count)")
            }
            else {
                
                completionHandler(arrCategories:[])
                //if result == .Error {
                DLog("Error while parsing categories")
                //}
            }
        }
    }
    
    func parseCategoriesFromArray(data:Array<Dictionary<String,AnyObject>>,completionQueue:dispatch_queue_t = dispatch_get_main_queue(),completion:(result:ParsingResult,arrcategories:Array<CategoryModel>)->Void  ) {
        
        let backgroundContext:NSManagedObjectContext = CoreDataHelper.sharedInstance.getBackgroundManageObjectContext()
        var categories:Array<CategoryModel> = []
        
        for dict:Dictionary<String,AnyObject> in data {
            dispatch_sync(dispatch_queue_create("no.dynamicelemnts.parsing", DISPATCH_QUEUE_SERIAL), { () -> Void in
                self.parseCategoryFromDictionary(dict, categories: &categories, managedContext:backgroundContext)
            })
        }

        do {
            
            try backgroundContext.saveContextAndWait()
        }
        catch let error as NSError  {
            completion(result:.Error(error: error),arrcategories:[])
        }
        dispatch_async(completionQueue) {
            
            do {
                let insertedCategoryList:Array<CategoryModel> = try CategoryModel.allInContext(CoreDataHelper.sharedInstance.getMainManageObjectContext())
                completion(result:.Success,arrcategories: insertedCategoryList)
            }
            catch let error as NSError  {
                completion(result:.Error(error: error),arrcategories: [])
            }
            
        }
    }
    
    func parseCategoryFromDictionary(dictionary:Dictionary<String,AnyObject>,inout categories:Array<CategoryModel>, managedContext:NSManagedObjectContext) {
        
        let categoryModel:CategoryModel = CoreDataHelper.sharedInstance.insertIntoCoreData("CategoryModel",managedContext: managedContext) as! CategoryModel
        
        categoryModel.categoryID =   "CategoryID" <~~ dictionary
        categoryModel.name = "Name" <~~ dictionary
        
        var arrSubCategories:Array<SubCategoryModel> = []
        
        if categoryModel.name == CATEGORY_BYDELSTRIPS {
            
            //let nullManagedObjectContext:NSManagedObjectContext
            let subCategoryModel:SubCategoryModel = CoreDataHelper.sharedInstance.insertIntoCoreData("SubCategoryModel",managedContext: managedContext) as! SubCategoryModel
            subCategoryModel.categoryID = 0
            subCategoryModel.name = CATEGORY_OPPLEVELSER
            arrSubCategories.append(subCategoryModel)
            
        }
        
        let dataSubCategories:Array<Dictionary<String,AnyObject>> = ("Categories" <~~ dictionary)!
        
        for subdictionary:Dictionary<String,AnyObject> in dataSubCategories {
            
            let subCategoryModel:SubCategoryModel = CoreDataHelper.sharedInstance.insertIntoCoreData("SubCategoryModel",managedContext: managedContext) as! SubCategoryModel
            
            subCategoryModel.categoryID =   "CategoryID" <~~ subdictionary
            subCategoryModel.name = "Name" <~~ subdictionary
            
            arrSubCategories.append(subCategoryModel)
        }
        
        categoryModel.subCategories = Set(arrSubCategories)
        
        categories.append(categoryModel)
    }
    
    //MARK: Accounts
    
    func parseAccounts(storingArray:Array<Dictionary<String,AnyObject>>, isForCoreData:Bool, completionHandler:(arrAcounts:Array<AccountModel>) -> Void)
    {
        let mainContext:NSManagedObjectContext = CoreDataHelper.sharedInstance.getMainManageObjectContext()
        
        do {
            try AccountModel.removeAllInContext(mainContext)
            //try OpeningHoursModel.removeAllInContext(backgroundContext)
            //try CustomOpeningHoursModel.removeAllInContext(backgroundContext)
            
            try mainContext.saveContextAndWait()
        }
        catch let error as NSError  {
            DLog("Error while removing AccountModel,OpeningHoursModel and CustomOpeningHoursModel : \(error)");
        }
        
         //now continue
        //let err:NSError?
        let appManager:ApplicationManager = ApplicationManager.sharedInstance();
        appManager.storeAccountsToLocal(storingArray)
        appManager.arrAcounts = []
        
        let userDefaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        
        self.parseAccountsFromArray(storingArray, isForCoreData:isForCoreData ) { (result, arrItems) in
            
            if result == .Success {
                
                let arrSortedAccounts : Array<AccountModel> = arrItems.sort({$0.accountID!.intValue < $1.accountID!.intValue})
                appManager.arrAcounts = NSMutableArray(array:arrSortedAccounts)
                
                completionHandler(arrAcounts:arrItems)
                
                userDefaults.setObject(NSDate(), forKey: USER_DEFAULT_BUS_OWNER_FETCH_DATE)
                DLog("parseAllAccounts count \(appManager.arrAcounts.count)")
            }
            else {
                
                completionHandler(arrAcounts:[])
                //if result == .Error {
                DLog("Error while parsing categories")
                //}
            }
        }
    }
    
    func parseAccountsFromArray(data:Array<Dictionary<String,AnyObject>>, isForCoreData:Bool,completionQueue:dispatch_queue_t = dispatch_get_main_queue(),completion:(result:ParsingResult,arrItems:Array<AccountModel>)->Void  ) {
        
        let backgroundContext:NSManagedObjectContext = CoreDataHelper.sharedInstance.getBackgroundManageObjectContext()
        var allItems:Array<AccountModel> = []
        
        for dict:Dictionary<String,AnyObject> in data {
            dispatch_sync(dispatch_queue_create("no.dynamicelemnts.parsing", DISPATCH_QUEUE_SERIAL), { () -> Void in
                
                if isForCoreData == true{
                    self.parseAccountFromDictionary(dict, isForCoreData:isForCoreData, storingArray: &allItems, managedContext:backgroundContext )
                }
                else{
                    self.parseAccountWithAllDetailsFromDictionary(dict, isForCoreData:isForCoreData, storingArray: &allItems, managedContext:backgroundContext )
                }
            })
        }
        
        do {
            
            try backgroundContext.saveContextAndWait()
        }
        catch let error as NSError  {
            completion(result:.Error(error: error),arrItems:[])
        }
        dispatch_async(completionQueue) {
            
            do {
                
                if isForCoreData == true{
                    let insertedAccountList:Array<AccountModel> = try AccountModel.allInContext(CoreDataHelper.sharedInstance.getMainManageObjectContext())
                    completion(result:.Success,arrItems: insertedAccountList)
                }
                else{
                    //let arrSortedAccounts : Array<AccountModel> = allItems.sort({$0.accountID!.intValue < $1.accountID!.intValue})
                    
                    completion(result:.Success,arrItems:allItems)
                }
            }
            catch let error as NSError  {
                completion(result:.Error(error: error),arrItems: [])
            }
            
        }
    }
    
    func parseAccountFromDictionary(dictionary:Dictionary<String,AnyObject>, isForCoreData:Bool, inout storingArray:Array<AccountModel>,managedContext:NSManagedObjectContext) {
        
        let itemModel:AccountModel!
        
        if isForCoreData == true{
          itemModel  = CoreDataHelper.sharedInstance.insertIntoCoreData("AccountModel",managedContext: managedContext) as! AccountModel
        }
        else{
            //let nullManagedObjectContext:NSManagedObjectContext
            let entity:NSEntityDescription = NSEntityDescription.entityForName("AccountModel", inManagedObjectContext:managedContext)!
            itemModel = NSManagedObject(entity: entity, insertIntoManagedObjectContext: nil) as! AccountModel
        }
        
        itemModel.accountID =   "AccountID" <~~ dictionary
        itemModel.name = "Name" <~~ dictionary
        itemModel.categoryID = "CategoryID" <~~ dictionary
        itemModel.categoryGroupID = "CategoryGroupID" <~~ dictionary
        itemModel.latitude = "Latitude" <~~ dictionary
        itemModel.longitude = "Longitude" <~~ dictionary
        itemModel.descr = "Description" <~~ dictionary
        
        itemModel.images = []
        var arrImageList:Array<ImageList> = [];
        
        if dictionary["ImageList"] != nil {
            for dict:Dictionary<String,AnyObject> in (dictionary["ImageList"]! as? Array<Dictionary<String,AnyObject>>)! {
                
                arrImageList.append(self.parseImageListFromDictionary(dict,isForCoreData:isForCoreData,managedObject:managedContext))
            }
            itemModel.images = Set(arrImageList)
        }
        
        storingArray.append(itemModel)
    }
    
    func parseAccountWithAllDetailsFromDictionary(dictionary:Dictionary<String,AnyObject>, isForCoreData:Bool, inout storingArray:Array<AccountModel>,managedContext:NSManagedObjectContext) {
        
        let itemModel:AccountModel!
        
        if isForCoreData == true{
            itemModel  = CoreDataHelper.sharedInstance.insertIntoCoreData("AccountModel",managedContext: managedContext) as! AccountModel
        }
        else{
            //let nullManagedObjectContext:NSManagedObjectContext
            let entity:NSEntityDescription = NSEntityDescription.entityForName("AccountModel", inManagedObjectContext:managedContext)!
            itemModel = NSManagedObject(entity: entity, insertIntoManagedObjectContext: nil) as! AccountModel
        }
        
        itemModel.accountID =   "AccountID" <~~ dictionary
        itemModel.name = "Name" <~~ dictionary
        itemModel.categoryID = "CategoryID" <~~ dictionary
        itemModel.categoryGroupID = "CategoryGroupID" <~~ dictionary
        itemModel.latitude = "Latitude" <~~ dictionary
        itemModel.longitude = "Longitude" <~~ dictionary
        itemModel.descr = "Description" <~~ dictionary
        
        itemModel.images = []
        var arrImageList:Array<ImageList> = [];
        
        if dictionary["ImageList"] != nil {
            for dict:Dictionary<String,AnyObject> in (dictionary["ImageList"]! as? Array<Dictionary<String,AnyObject>>)! {
                
                arrImageList.append(self.parseImageListFromDictionary(dict,isForCoreData:isForCoreData,managedObject:managedContext))
            }
            itemModel.images = Set(arrImageList)
        }
        
        //Opening hours
        
        itemModel.openingHoursTitle = "OpeningHoursTitle" <~~ dictionary
        //Other field
        itemModel.isFavourite = "IsFavorite" <~~ dictionary
        itemModel.descr = "Description" <~~ dictionary
        itemModel.email = "EmailID" <~~ dictionary
        itemModel.phone = "Phone" <~~ dictionary
        itemModel.socialProfile = "SocialProfile" <~~ dictionary
        itemModel.address = "Address" <~~ dictionary
        itemModel.city = "City" <~~ dictionary
        itemModel.country = "Country" <~~ dictionary
        itemModel.postalCode = "PostalCode" <~~ dictionary
        itemModel.status = "Status" <~~ dictionary
        itemModel.website = "WebSite" <~~ dictionary
        itemModel.bookTable = "BookTable" <~~ dictionary
        
        itemModel.merchantID = "MerchantID" <~~ dictionary
        itemModel.isFastTrack = "IsFastTrack" <~~ dictionary
        itemModel.isOpen = "IsOpen" <~~ dictionary
        itemModel.startHour = "StartHour" <~~ dictionary
        itemModel.endHour = "EndHour" <~~ dictionary
        itemModel.offset = "Offset" <~~ dictionary
        storingArray.append(itemModel)
    }
    
    //As account detail has so much parameters so this function has been added
    func updateAndParseAccountAllDetailsFromDictionary(dictionary:Dictionary<String,AnyObject>,isForCoreData:Bool,strAccountID:NSNumber) -> AccountModel?
    {
        let backgroundContext:NSManagedObjectContext = CoreDataHelper.sharedInstance.getMainManageObjectContext()

        if isForCoreData == true {
            
            let fetchRequest = NSFetchRequest(entityName: "AccountModel")
            let accountID = strAccountID
            let pred = NSPredicate(format:"accountID == %@",accountID)
            fetchRequest.predicate = pred
            
            //3
            do {
                let results = try backgroundContext.executeFetchRequest(fetchRequest)
                let arrFetchRecord:Array<AccountModel>? = results as? [AccountModel]
                
                if arrFetchRecord != nil {
                    
                    if arrFetchRecord!.count>0 {
                        let accountModel:AccountModel = arrFetchRecord![0]
                        accountModel.accountID =   "AccountID" <~~ dictionary
                        accountModel.name = "Name" <~~ dictionary
                        accountModel.categoryID = "CategoryID" <~~ dictionary
                        accountModel.categoryGroupID = "CategoryGroupID" <~~ dictionary
                        accountModel.latitude = "Latitude" <~~ dictionary
                        accountModel.longitude = "Longitude" <~~ dictionary
                        
                        
                        accountModel.images = []
                        var arrImageList:Array<ImageList> = [];
                        
                        if dictionary["ImageList"] != nil {
                            for dict:Dictionary<String,AnyObject> in (dictionary["ImageList"]! as? Array<Dictionary<String,AnyObject>>)! {
                                
                                arrImageList.append(self.parseImageListFromDictionary(dict,isForCoreData:true,managedObject:backgroundContext))
                            }
                            accountModel.images = Set(arrImageList)
                        }
                        
                        //Opening hours
                        accountModel.openingHoursTitle = "OpeningHoursTitle" <~~ dictionary
                    
                        //Other field
                        accountModel.isFavourite = "IsFavorite" <~~ dictionary
                        accountModel.descr = "Description" <~~ dictionary
                        accountModel.email = "EmailID" <~~ dictionary
                        accountModel.phone = "Phone" <~~ dictionary
                        accountModel.socialProfile = "SocialProfile" <~~ dictionary
                        accountModel.address = "Address" <~~ dictionary
                        accountModel.city = "City" <~~ dictionary
                        accountModel.country = "Country" <~~ dictionary
                        accountModel.postalCode = "PostalCode" <~~ dictionary
                        accountModel.status = "Status" <~~ dictionary
                        accountModel.website = "WebSite" <~~ dictionary
                        accountModel.bookTable = "BookTable" <~~ dictionary
                        
                        accountModel.merchantID = "MerchantID" <~~ dictionary
                        accountModel.isFastTrack = "IsFastTrack" <~~ dictionary
                        accountModel.isOpen = "IsOpen" <~~ dictionary
                        
                        
                        do {
                            
                            try backgroundContext.saveContextAndWait()
                            return accountModel
                            
                        }
                        catch let error as NSError  {
                            print("Could not save accountmodel \(error), \(error.userInfo)")
                            return nil
                        }
                    }
                    else //No record found
                    {
                        let entity:NSEntityDescription = NSEntityDescription.entityForName("AccountModel", inManagedObjectContext:backgroundContext)!
                        let accountModel:AccountModel = NSManagedObject(entity: entity, insertIntoManagedObjectContext: nil) as! AccountModel
                        
                        accountModel.accountID =   "AccountID" <~~ dictionary
                        accountModel.name = "Name" <~~ dictionary
                        accountModel.categoryID = "CategoryID" <~~ dictionary
                        accountModel.categoryGroupID = "CategoryGroupID" <~~ dictionary
                        accountModel.latitude = "Latitude" <~~ dictionary
                        accountModel.longitude = "Longitude" <~~ dictionary
                        
                        
                        accountModel.images = []
                        var arrImageList:Array<ImageList> = [];
                        
                        if dictionary["ImageList"] != nil {
                            for dict:Dictionary<String,AnyObject> in (dictionary["ImageList"]! as? Array<Dictionary<String,AnyObject>>)! {
                                
                                arrImageList.append(self.parseImageListFromDictionary(dict,isForCoreData:false,managedObject:backgroundContext))
                            }
                            accountModel.images = Set(arrImageList)
                        }
                        
                        
                        //Opening hours
                        accountModel.openingHoursTitle = "OpeningHoursTitle" <~~ dictionary
              
                        //Other field
                        accountModel.isFavourite = "IsFavorite" <~~ dictionary
                        accountModel.descr = "Description" <~~ dictionary
                        accountModel.email = "EmailID" <~~ dictionary
                        accountModel.phone = "Phone" <~~ dictionary
                        accountModel.socialProfile = "SocialProfile" <~~ dictionary
                        accountModel.address = "Address" <~~ dictionary
                        accountModel.city = "City" <~~ dictionary
                        accountModel.country = "Country" <~~ dictionary
                        accountModel.postalCode = "PostalCode" <~~ dictionary
                        accountModel.status = "Status" <~~ dictionary
                        accountModel.website = "WebSite" <~~ dictionary
                        accountModel.bookTable = "BookTable" <~~ dictionary
                        
                        accountModel.merchantID = "MerchantID" <~~ dictionary
                        accountModel.isFastTrack = "IsFastTrack" <~~ dictionary
                        accountModel.isOpen = "IsOpen" <~~ dictionary
                        
                        return accountModel;
                    }
                    
                    
                }
                else{
                    return nil
                }
                
                
            } catch let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
                return nil
            }

        }
        else {
            
            let entity:NSEntityDescription = NSEntityDescription.entityForName("AccountModel", inManagedObjectContext:backgroundContext)!
            let accountModel:AccountModel = NSManagedObject(entity: entity, insertIntoManagedObjectContext: nil) as! AccountModel
            
            accountModel.accountID =   "AccountID" <~~ dictionary
            accountModel.name = "Name" <~~ dictionary
            //accountModel.categoryID = "CategoryID" <~~ dictionary
            //accountModel.categoryGroupID = "CategoryGroupID" <~~ dictionary
            accountModel.latitude = "Latitude" <~~ dictionary
            accountModel.longitude = "Longitude" <~~ dictionary
            
            
            accountModel.images = []
            var arrImageList:Array<ImageList> = [];
            
            if dictionary["ImageList"] != nil {
                for dict:Dictionary<String,AnyObject> in (dictionary["ImageList"]! as? Array<Dictionary<String,AnyObject>>)! {
                    
                    arrImageList.append(self.parseImageListFromDictionary(dict,isForCoreData:false,managedObject:backgroundContext))
                }
                accountModel.images = Set(arrImageList)
            }
            
            
            //Opening hours
            
            accountModel.openingHoursTitle = "OpeningHoursTitle" <~~ dictionary
            
//            let openingHoursModelEntity:NSEntityDescription = NSEntityDescription.entityForName("OpeningHoursModel", inManagedObjectContext:backgroundContext)!
//            let openingHoursModel:OpeningHoursModel = NSManagedObject(entity: openingHoursModelEntity, insertIntoManagedObjectContext: nil) as! OpeningHoursModel
//            
//            let dataOpeningHours:Dictionary<String,AnyObject>? = "OpeningHours" <~~ dictionary
//            
//            if dataOpeningHours != nil {
//                
//                let expectionOpeningHours:Array<Dictionary<String,AnyObject>> = ("Exception" <~~ dataOpeningHours!)!
//                let defaultOpeningHours:Array<Dictionary<String,AnyObject>> = ("Default" <~~ dataOpeningHours!)!
//                
//                var arrExpectionOpeningHours:Array<CustomOpeningHoursModel> = []
//                for subdictionary:Dictionary<String,AnyObject> in expectionOpeningHours {
//                    
//                    let entity:NSEntityDescription = NSEntityDescription.entityForName("CustomOpeningHoursModel", inManagedObjectContext:backgroundContext)!
//                    let subItemModel:CustomOpeningHoursModel = NSManagedObject(entity: entity, insertIntoManagedObjectContext: nil) as! CustomOpeningHoursModel
//
//                    
//                    subItemModel.name =   "Name" <~~ subdictionary
//                    subItemModel.validFrom = "ValidFrom" <~~ subdictionary
//                    subItemModel.validTo = "ValidTo" <~~ subdictionary
//                    subItemModel.status = "Status" <~~ subdictionary
//                    subItemModel.dayOfWeek = "DayOfWeek" <~~ subdictionary
//                    subItemModel.openingType = "OpeningType" <~~ subdictionary
//                    
//                    arrExpectionOpeningHours.append(subItemModel)
//                }
//                
//                var arrDefaultOpeningHours:Array<CustomOpeningHoursModel> = []
//                for subdictionary:Dictionary<String,AnyObject> in defaultOpeningHours {
//                    
//                    let entity:NSEntityDescription = NSEntityDescription.entityForName("CustomOpeningHoursModel", inManagedObjectContext:backgroundContext)!
//                    let subItemModel:CustomOpeningHoursModel = NSManagedObject(entity: entity, insertIntoManagedObjectContext: nil) as! CustomOpeningHoursModel
//                    
//                    subItemModel.name =   "Name" <~~ subdictionary
//                    subItemModel.validFrom = "ValidFrom" <~~ subdictionary
//                    subItemModel.validTo = "ValidTo" <~~ subdictionary
//                    subItemModel.status = "Status" <~~ subdictionary
//                    subItemModel.dayOfWeek = "DayOfWeek" <~~ subdictionary
//                    subItemModel.openingType = "OpeningType" <~~ subdictionary
//                    
//                    
//                    arrDefaultOpeningHours.append(subItemModel)
//                }
//                
//                openingHoursModel.exception = Set(arrExpectionOpeningHours)
//                openingHoursModel.defaultHours = Set(arrExpectionOpeningHours)
//                
//                accountModel.openingHours =  openingHoursModel;
//            }
            
            
            //Other field
            accountModel.isFavourite = "IsFavorite" <~~ dictionary
            accountModel.descr = "Description" <~~ dictionary
            accountModel.email = "EmailID" <~~ dictionary
            accountModel.phone = "Phone" <~~ dictionary
            accountModel.socialProfile = "SocialProfile" <~~ dictionary
            accountModel.address = "Address" <~~ dictionary
            accountModel.city = "City" <~~ dictionary
            accountModel.country = "Country" <~~ dictionary
            accountModel.postalCode = "PostalCode" <~~ dictionary
            accountModel.status = "Status" <~~ dictionary
            accountModel.website = "WebSite" <~~ dictionary
            accountModel.bookTable = "BookTable" <~~ dictionary
            
            accountModel.merchantID = "MerchantID" <~~ dictionary
            accountModel.isFastTrack = "IsFastTrack" <~~ dictionary
            accountModel.isOpen = "IsOpen" <~~ dictionary
            
            return accountModel;
            
        }
    }
    
    
    //MARK:- CampignActivity
    
    func parseCampaignActivities(storingArray:Array<Dictionary<String,AnyObject>>,isForCoreData:Bool, completionHandler:(arrCampaignActivites:Array<CampaignActivity>) -> Void) {
        
        if isForCoreData == true {
            let backgroundContext:NSManagedObjectContext = CoreDataHelper.sharedInstance.getBackgroundManageObjectContext()
            do {
                try CampaignActivity.removeAllInContext(backgroundContext)
                try backgroundContext.saveContextAndWait()
            }
            catch let error as NSError  {
                DLog("Error while removing AccountModel,OpeningHoursModel and CustomOpeningHoursModel : \(error)");
            }
        }
        
        self.parseCampaignActivityFromArray(storingArray,isForCoreData:isForCoreData) { (result, campaignActivites) in
            
            if result == .Success {
                
                completionHandler(arrCampaignActivites:campaignActivites)
            }
            else {
                
                completionHandler(arrCampaignActivites:[])
                //if result == .Error {
                DLog("Error while parsing CampaignActivites")
                //}
            }
        }
    }
    
    
    func parseCampaignActivityFromArray(campaignActs:Array<Dictionary<String,AnyObject>>,isForCoreData:Bool, completionQueue:dispatch_queue_t = dispatch_get_main_queue(),completion:(result:ParsingResult,campaignActivites:Array<CampaignActivity>) -> Void  ) {
        
        let backgroundContext:NSManagedObjectContext = CoreDataHelper.sharedInstance.getBackgroundManageObjectContext()
        
        var campaignActivities:Array<CampaignActivity> = []
        
        for dict:Dictionary<String,AnyObject> in campaignActs {
            dispatch_sync(dispatch_queue_create("no.dynamicelemnts.parsing", DISPATCH_QUEUE_SERIAL), { () -> Void in
                self.parseCampaignActivityFromDictionary(dict, isForCoreData:isForCoreData, campaignActivities: &campaignActivities, managedContext:backgroundContext)
                
            })
        }
        
        if isForCoreData.boolValue == true {
            
            do {
                
                try backgroundContext.saveContextAndWait()
            }
            catch let error as NSError  {
                completion(result:.Error(error: error),campaignActivites:[])
            }
            dispatch_async(completionQueue) {
                
                do {
                    let insertedAccountList:Array<CampaignActivity> = try CampaignActivity.allInContext(CoreDataHelper.sharedInstance.getMainManageObjectContext())
                    completion(result:.Success, campaignActivites:insertedAccountList)
                }
                catch let error as NSError  {
                    completion(result:.Error(error: error),campaignActivites:[])
                }
                
            }
        }
        else{
            dispatch_async(completionQueue) {
                
                completion(result:.Success ,campaignActivites: campaignActivities)
                
            }
        }
        
        
    }
    
    func parseCampaignActivityFromDictionary(dictionary:Dictionary<String,AnyObject>, isForCoreData:Bool, inout campaignActivities:Array<CampaignActivity>, managedContext:NSManagedObjectContext) {
        
        let campaignActivity:CampaignActivity!
        
        if isForCoreData == true {
            campaignActivity = CoreDataHelper.sharedInstance.insertIntoCoreData("CampaignActivity",managedContext: managedContext) as! CampaignActivity
        }
        else{
            
            //let nullManagedObjectContext:NSManagedObjectContext
            let entity:NSEntityDescription = NSEntityDescription.entityForName("CampaignActivity", inManagedObjectContext:managedContext)!
            campaignActivity = NSManagedObject(entity: entity, insertIntoManagedObjectContext: nil) as! CampaignActivity
        }

        campaignActivity.campaignActivityID = "CampaignActivityID" <~~ dictionary
        campaignActivity.campaignActivityType = "CampaignActivityType" <~~ dictionary
        campaignActivity.tileSize = "TileSize" <~~ dictionary
        campaignActivity.title = "Title" <~~ dictionary
        campaignActivity.summary = "Summary" <~~ dictionary
        campaignActivity.descr = "Description" <~~ dictionary
        campaignActivity.hTMLContent = "HTMLContent" <~~ dictionary
        campaignActivity.url = "URL" <~~ dictionary
        
        campaignActivity.imageList = []
        var arrImageList:Array<ImageList> = [];
        let arrDataImageList:Array<Dictionary<String,AnyObject>>? =  "ImageList" <~~ dictionary
        if arrDataImageList != nil {
            for dict:Dictionary<String,AnyObject> in arrDataImageList! {
                arrImageList.append(self.parseImageListFromDictionary(dict,isForCoreData:isForCoreData,managedObject:managedContext))
            }
        }
        
        campaignActivity.imageList = Set(arrImageList)
        
        campaignActivity.notificationID = "NotificationID" <~~ dictionary
        campaignActivity.isFavorite = "IsFavorite" <~~ dictionary
        campaignActivity.postDate = "PostDate" <~~ dictionary
        campaignActivity.latitude =  "Latitude" <~~ dictionary
        campaignActivity.longitude = "Longitude" <~~ dictionary
        campaignActivity.startDateTime = "StartDateTime" <~~ dictionary
        campaignActivity.endDateTime = "EndDateTime" <~~ dictionary
        campaignActivity.eventAddress = "EventAddress" <~~ dictionary
        campaignActivity.offerKronerDiscount = "OfferKronerDiscount" <~~ dictionary
        campaignActivity.offerReceiveQuantity = "OfferReceiveQuantity" <~~ dictionary
        campaignActivity.offerKronerPrice = "OfferKronerPrice" <~~ dictionary
        campaignActivity.offerRatioWord = "OfferRatioWord" <~~ dictionary
        campaignActivity.offerPercentageDiscount =  "OfferPercentageDiscount" <~~ dictionary
//        if dictionary["OfferPercentageDiscount"] != nil {
//            
//            campaignActivity.offerPercentageDiscount =  "\(dictionary["OfferPercentageDiscount"]!)"
//        }

        campaignActivity.offerPayForQuantity = "OfferPayForQuantity" <~~ dictionary
        campaignActivity.offerTitle = "OfferTitle" <~~ dictionary
        campaignActivity.offerText = "OfferText" <~~ dictionary
        campaignActivity.offerType = "OfferType" <~~ dictionary
        campaignActivity.redemptionLimit = "RedemptionLimit" <~~ dictionary
        campaignActivity.accountID = "AccountID" <~~ dictionary
        campaignActivity.accountAddress = "AccountAddress" <~~ dictionary
        
        let tempCategoryID:Int? = "CategoryID" <~~ dictionary
        if tempCategoryID != nil {
            campaignActivity.categoryID = tempCategoryID!
        }
        campaignActivity.accountName =  "AccountName" <~~ dictionary
        campaignActivity.facilitiesSymbol =  "FacilitiesSymbol" <~~ dictionary
        campaignActivity.facilitiesBoldTitle = "FacilitiesBoldTitle" <~~ dictionary
        campaignActivity.facilitiesRegularTitle = "FacilitiesRegularTitle" <~~ dictionary
        campaignActivity.weatherName =  "WeatherName" <~~ dictionary
        campaignActivity.weatherWindDirection = "WeatherWindDirection" <~~ dictionary
        campaignActivity.weatherTemperature =  "WeatherTemperature" <~~ dictionary
        campaignActivity.weatherWindSpeed = "WeatherWindSpeed" <~~ dictionary
        campaignActivity.weatherVar = "WeatherVar" <~~ dictionary
        campaignActivity.beaconRulesList = []
        
        var arrBeaconRulesList:Array<BeaconRules> = [];
        let arrDataBeaconRulesList:Array<Dictionary<String,AnyObject>>? =  "BeaconRulesList" <~~ dictionary
        if arrDataBeaconRulesList != nil {
            for dict:Dictionary<String,AnyObject> in arrDataBeaconRulesList! {
                
                arrBeaconRulesList.append(self.parseBeaconRuleFromDictionary(dict,isForCoreData:isForCoreData,managedObject:managedContext))
            }
        }
       
        campaignActivity.beaconRulesList = Set(arrBeaconRulesList)
        
        campaignActivities.append(campaignActivity)
        

    }
    func parseImageListFromDictionary(dictionary:Dictionary<String,AnyObject>,isForCoreData:Bool, managedObject:NSManagedObjectContext) -> ImageList {
        
        let imageList:ImageList!
        
        if isForCoreData == true {
            imageList = CoreDataHelper.sharedInstance.insertIntoCoreData("ImageList",managedContext: managedObject) as! ImageList
        }
        else{
            
            //let nullManagedObjectContext:NSManagedObjectContext
            let entity:NSEntityDescription = NSEntityDescription.entityForName("ImageList", inManagedObjectContext:managedObject)!
            imageList = NSManagedObject(entity: entity, insertIntoManagedObjectContext: nil) as! ImageList
        }
        
        imageList.imageType = "ImageType" <~~ dictionary
        imageList.imageURL = "ImageURL" <~~ dictionary
        imageList.title = "Title" <~~ dictionary
        return imageList
    }

    func parseBeaconRuleFromDictionary(dictionary:Dictionary<String,AnyObject>,isForCoreData:Bool,managedObject:NSManagedObjectContext) -> BeaconRules {
        let beaconRule:BeaconRules!
        if isForCoreData == true {
            beaconRule = CoreDataHelper.sharedInstance.insertIntoCoreData("BeaconRules",managedContext: managedObject) as! BeaconRules
        }
        else{
            
            //let nullManagedObjectContext:NSManagedObjectContext
            let entity:NSEntityDescription = NSEntityDescription.entityForName("BeaconRules", inManagedObjectContext:managedObject)!
            beaconRule = NSManagedObject(entity: entity, insertIntoManagedObjectContext: nil) as! BeaconRules
        }
        
        beaconRule.beaconID = "BeaconID" <~~ dictionary
        beaconRule.proximity = "Proximity" <~~ dictionary
        beaconRule.interval = "Interval" <~~ dictionary
        beaconRule.startTimeStamp = "StartTimeStamp" <~~ dictionary
        beaconRule.endtimeStamp = "EndtimeStamp" <~~ dictionary
        beaconRule.lastTimeTriggered = "LastTimeTriggered"  <~~ dictionary
        return beaconRule
    }
    
    
    
    //MARK:- Beacon Parsing
    
    func parseBeaconsFromArray(beacons:Array<Dictionary<String,AnyObject>>,queue:dispatch_queue_t = dispatch_get_main_queue(),completion:(result:ParsingResult,beaconList:Array<Beacon>)->Void  ) {
        
        let mainManagedContext:NSManagedObjectContext = CoreDataHelper.sharedInstance.getMainManageObjectContext()
        
        var beaconList:Array<Beacon> = []
        
        do {
            try  Beacon.removeAllInContext(mainManagedContext)
            try mainManagedContext.saveContextAndWait()
        }
        catch let error as NSError {
            
            print("\(error)")
        }
       
        for dict:Dictionary<String,AnyObject> in beacons {
            
            dispatch_sync(dispatch_queue_create("no.dynamicelemnts.parsing", DISPATCH_QUEUE_SERIAL), { () -> Void in
                
                let beacon:Beacon = CoreDataHelper.sharedInstance.insertIntoCoreData("Beacon",managedContext: mainManagedContext) as! Beacon
                
                let iBeacon:Ibeacon = CoreDataHelper.sharedInstance.insertIntoCoreData("Ibeacon", managedContext: mainManagedContext) as! Ibeacon
                
                beaconList.append(self.parseBeaconFromDictionary(dict, beacon: beacon,iBeacon: iBeacon))
            })
        }
        print("Beacon list parsing completed")
        
        mainManagedContext.saveContext({ result  in
            print("Beacon list completion handler called")
            completion(result:.Success,beaconList: beaconList)
        })
    }
    
    func parseBeaconFromDictionary(dictionary:Dictionary<String,AnyObject>,beacon:Beacon,iBeacon:Ibeacon) -> Beacon {
        
        beacon.beaconID =   "BeaconID" <~~ dictionary
        //beacon.proximmity =   ("proximmity" <~~ dictionary)!
        beacon.name = "Name" <~~ dictionary
        beacon.descr = "Description" <~~ dictionary
        beacon.vendorSerial = "VendorSerial" <~~ dictionary
        beacon.beaconMac = "BeaconMac" <~~ dictionary
        beacon.beaconType = "BeaconType" <~~ dictionary
     // beacon.mutableSetValueForKey("Ibeacon").addObject(iBeacon)
        beacon.ibeacon =    iBeacon
    
        beacon.ibeacon?.uuid = "ibeacon.UUID" <~~ dictionary
        beacon.ibeacon?.major = "ibeacon.Major" <~~ dictionary
        beacon.ibeacon?.minor = "ibeacon.Minor" <~~ dictionary
        iBeacon.parentBeacon = beacon
        beacon.latitude =  "latitude" <~~ dictionary
        beacon.longitude = "longitude" <~~ dictionary
        print("IBeacon Information ::: \(beacon.ibeacon?.uuid):::\(beacon.ibeacon?.major):::\(beacon.ibeacon?.minor)")
        print("beaconID is \(beacon.beaconID)")
        return beacon
       
    }
    
    
}

