//
//  OfferRedeemdedModel.swift
//  Din Aker Brygge
//
//  Created by Rahul on 10/3/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import UIKit
import Gloss

class OfferRedeemdedModel: NSObject, Glossy {

    var OfferRedeemedID: NSNumber?
    var ReceiptID: NSNumber?
    var CreatedOn: NSNumber?
    var DeliveredOn: NSNumber?
    
    required init?(json: JSON) {
        
        super.init()
        self.OfferRedeemedID = "OfferRedeemedID" <~~ json
        self.ReceiptID = "ReceiptID" <~~ json
        self.CreatedOn = "CreatedOn" <~~ json
        self.DeliveredOn = "DeliveredOn" <~~ json
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
        
        return jsonify([
            
            "OfferRedeemedID" ~~> self.OfferRedeemedID,
            "ReceiptID" ~~> self.ReceiptID,
            "CreatedOn" ~~> self.CreatedOn,
            "DeliveredOn" ~~> self.DeliveredOn
            ])
    }
}
