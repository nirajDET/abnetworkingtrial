//
//  PostUserActivity.swift
//  Din Aker Brygge
//
//  Created by Paresh Navadiya on 22/06/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Gloss

struct PostUserActivity: Glossy {

    var UserAction: NSNumber? //UserActionType
    //optional
    var ContentID: NSNumber?
    var ContentType: NSNumber?
    //optional
    var VariantID: NSNumber?
    var TotalLineItem: NSNumber?
    var TotalPrice: NSNumber?
    //optional
    var BeaconID: NSNumber?
    var Proximity: NSNumber?
    var Latitude: NSNumber?
    var Longitude: NSNumber?
    //optional
    var PhoneNo: String?
    var Url: String?
   
    init(){
        
    }
    
    init?(json: JSON) {
        
    }
    
    // MARK:- Serialization
    func toJSON() -> JSON? {
       
        var arrParameters:[JSON?] = []
        
        if self.UserAction != nil {
            arrParameters.append(["UserAction":self.UserAction!])
        }
        if self.ContentID != nil {
            arrParameters.append(["ContentID":self.ContentID!])
        }
        if self.ContentType != nil {
            arrParameters.append(["ContentType":self.ContentType!])
        }
        if self.VariantID != nil {
            arrParameters.append(["VariantID":self.VariantID!])
        }
        if self.TotalLineItem != nil {
            arrParameters.append(["TotalLineItem":self.TotalLineItem!])
        }
        if self.TotalPrice != nil {
            arrParameters.append(["TotalPrice":self.TotalPrice!])
        }
        if self.BeaconID != nil {
            arrParameters.append(["BeaconID":self.BeaconID!])
        }
        if self.Proximity != nil {
            arrParameters.append(["Proximity":self.Proximity!])
        }
        if self.Latitude != nil {
            arrParameters.append(["Latitude":self.Latitude!])
        }
        if self.Longitude != nil {
            arrParameters.append(["Longitude":self.Longitude!])
        }
        if self.PhoneNo != nil {
            arrParameters.append(["PhoneNo":self.PhoneNo! as AnyObject])
        }
        if self.Url != nil {
            arrParameters.append(["Url":self.Url! as AnyObject])
        }
           
        return jsonify(arrParameters)
    }
}
