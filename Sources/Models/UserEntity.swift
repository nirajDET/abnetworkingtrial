//
//  UserEntity.swift
//  Din Aker Brygge
//
//  Created by Mihir Mehta on 03/01/17.
//  Copyright © 2017 Dynamic Elements. All rights reserved.
//

import Foundation
import Gloss

@objc(UserEntity) class UserEntity:NSObject, Glossy {
    var userId:Int32 = 0
    var firstName:String?
    var lastName:String?
    var avatarURL:String? = ""
    var contactNumber:String? = ""
    var email:String?
    var address:String?  = ""
    var postalCode:String?
    var dob:NSNumber?
    var interests:Array<InterestEntity>?
    var bookMarks:Array<String>? = []
    var device:Dictionary<String,Any>?
    var currentLatitude:NSNumber = NSNumber(value:1)
    var currentLongitude:NSNumber = NSNumber(value:1)
    var relationWithAkerBrygge:AkerBryggeRelation = .Undefined
    var deviceID:Int32 = 0
    var apiKey:String?
    var country:String? = ""
    var city:String? = ""
    var gender:Int32 = -1
    var doSubscribeNewsletter:Int32 = 0
    var isDummyDOB:Bool?
    var roles:Array<Dictionary<String,AnyObject>> = []
    var modules:Array<Dictionary<String,AnyObject>> = []
    var accountId:NSNumber?
    var latitude:NSNumber?
    var longitude:NSNumber?
    var accountAddress:String?
    var imageList:Array<ImageList>?
    var isDummy:Bool = true
    
    override init() {
        super.init()
    }
    
    required init?(json: JSON) {
        
        
        if json["UserID"] != nil {
        
            self.userId = ("UserID" <~~ json)!
        }
        self.firstName = "FirstName" <~~ json
        self.lastName = "LastName" <~~ json
        self.avatarURL = "AvatarUrl" <~~ json   //  X - AvatarURL
        self.contactNumber = "MobilePhone" <~~ json
        self.email = "EmailID" <~~ json
        self.address = "Address" <~~ json
        self.postalCode = "PostalCode" <~~ json
        self.dob = "Dob" <~~ json   //  X - DOB
        
        if json["InterestList"] != nil {       //  X - Interests
            
            self.interests = [InterestEntity].from(jsonArray: ("InterestList" <~~ json)!)
        }
        
        if json["DeviceID"] != nil {
        
            self.deviceID = ("DeviceID" <~~ json)!
        }
        self.apiKey = "ApiKey" <~~ json
        
        
        if json["Bookmarks"] != nil {
            
            self.bookMarks = ("Bookmarks" <~~ json)!
        }
        self.device = "Device" <~~ json
        self.country = "Country" <~~ json
        self.city = "City" <~~ json
        
         if json["Gender"] != nil {
        
            self.gender = ("Gender" <~~ json)!
        }
        
         if json["IsNewsletterSubscribed"] != nil {     //  X   -   DoSubscribeNewsletter
        
            self.doSubscribeNewsletter = ("IsNewsletterSubscribed" <~~ json)!
        }
        
        if json["Roles"] != nil {
            
            
            self.roles = ("Roles" <~~ json)!
        }
        if json["Modules"] != nil {
            
            self.modules = ("Modules" <~~ json)!
        }
        self.accountId = "AccountID" <~~ json       //  <<<
        self.latitude = "Latitude" <~~ json     //  <<<
        self.longitude = "Longitude" <~~ json       //  <<<
        self.accountAddress = "AccountAddress" <~~ json     //  <<<
        
        if json["ImageList"] != nil {       //  <<<
        
        self.imageList = [ImageList].from(jsonArray: ("ImageList" <~~ json)!)
            
        }
        
        if json["RelationWithAB"] != nil {
            self.relationWithAkerBrygge = AkerBryggeRelation(rawValue: ("RelationWithAB" <~~ json)!)!
        }
        
    }
    func toJSON() -> JSON? {
        return jsonify([
            "UserID" ~~> self.userId,
            "FirstName" ~~> self.firstName,
            "LastName" ~~> self.lastName,
            "AvatarUrl" ~~> self.avatarURL,
            "MobilePhone" ~~> self.contactNumber,
            "EmailID" ~~> self.email,
            "Address" ~~> self.address,
            "PostalCode" ~~> self.postalCode,
            "Dob" ~~> self.dob,
            "InterestList" ~~> self.interests,
            "DeviceID" ~~> self.deviceID,
            "ApiKey" ~~> self.apiKey,
            "Bookmarks" ~~> self.bookMarks,
            "Device" ~~> self.device,
            "Country" ~~> self.country,
            "City" ~~> self.city,
            "Gender" ~~> self.gender,
            "IsNewsletterSubscribed" ~~> self.doSubscribeNewsletter,
            "Roles" ~~> self.roles,
            "Modules" ~~> self.modules,
            "AccountID" ~~> self.accountId,
            "Latitude" ~~> self.latitude,
            "Longitude" ~~> self.longitude,
            "AccountAddress" ~~> self.accountAddress,
            "ImageList" ~~> self.imageList,
            "RelationWithAB" ~~> self.relationWithAkerBrygge.rawValue
            ])
    }
    
    func encode(withCoder encoder: NSCoder) {
        //Encode the properties of the object
        encoder.encode(self.userId, forKey: "UserId")
        encoder.encode(self.firstName, forKey: "FirstName")
        encoder.encode(self.lastName, forKey: "LastName")
        encoder.encode(self.avatarURL, forKey: "AvatarURL")
        encoder.encode(self.contactNumber, forKey: "ContactNumber")
        encoder.encode(self.email, forKey: "Email")
        encoder.encode(self.postalCode, forKey: "PostalCode")
        encoder.encode(self.dob, forKey: "DOB")
        encoder.encode(self.interests, forKey: "Interests")
        encoder.encode(self.bookMarks, forKey: "Bookmarks")
        encoder.encode(self.device, forKey: "Device")
        encoder.encode(self.deviceID, forKey: "DeviceID")
        encoder.encode(self.apiKey, forKey: "ApiKey")
        encoder.encode(self.country, forKey: "Country")
        encoder.encode(self.city, forKey: "City")
//        encoder.encode(self.relationWithAkerBrygge.rawValue, forKey: "RelationWithAB")
        print(self.relationWithAkerBrygge.rawValue)
        
        let relation:Int = self.relationWithAkerBrygge.rawValue
       // encoder.encode(Int, forKey: <#T##String#>)
        encoder.encode(relation, forKey: "RelationWithAB")
        encoder.encode((self.isDummyDOB != nil), forKey: "IsDummy")
        encoder.encode((self.isDummyDOB != nil), forKey: "IsDummyDOB")
        encoder.encode(self.doSubscribeNewsletter, forKey: "DoSubscribeNewsletter")
        encoder.encode(self.gender, forKey: "IsMale")
        encoder.encode(self.roles, forKey: "Roles")
        encoder.encode(self.modules, forKey: "Modules")
        encoder.encode(self.accountId, forKey: "AccountID")
    }
    
    required init?(coder decoder: NSCoder) {
        
       
        
        if decoder.containsValue(forKey: "UserId") && (decoder.decodeObject(forKey: "UserId") != nil || decoder.decodeInt32(forKey: "UserId") != 0) {
        
        if let num = decoder.decodeObject(forKey: "UserId") as? NSNumber {
           // let userIdNumber = decoder.decodeObject(forKey: "UserId") as! NSNumber
            
            self.userId = num.int32Value
        }
        else {
            self.userId = decoder.decodeInt32(forKey: "UserId")
        }
        }
        
        
        self.firstName = decoder.decodeObject(forKey: "FirstName") as? String
        self.lastName = decoder.decodeObject(forKey: "LastName") as? String
        self.avatarURL = decoder.decodeObject(forKey: "AvatarURL") as? String
        self.contactNumber = decoder.decodeObject(forKey: "ContactNumber") as? String
        self.email = decoder.decodeObject(forKey: "Email") as? String
        self.postalCode = decoder.decodeObject(forKey: "PostalCode") as? String
        self.dob = decoder.decodeObject(forKey: "DOB") as? NSNumber
//        if decoder.containsValue(forKey: "Interests") == true {
        
//        if decoder.containsValue(forKey: "Interests") == true  {
//            
//            if decoder.decodeObject(forKey: "Interests") != nil {
//            
//                self.interests = decoder.decodeObject(forKey: "Interests")  as? Array<InterestEntity>
//            }
//            }
//        }
        self.bookMarks = decoder.decodeObject(forKey: "Bookmarks") as? Array<String>
        self.device = decoder.decodeObject(forKey: "Device") as? Dictionary<String,Any>
        self.deviceID = decoder.decodeInt32(forKey: "DeviceID")
        self.apiKey = decoder.decodeObject(forKey: "ApiKey") as? String
        self.country = decoder.decodeObject(forKey: "Country") as? String
        self.city = decoder.decodeObject(forKey: "City") as? String
        self.gender = decoder.decodeInt32(forKey: "IsMale")
        self.isDummyDOB = decoder.decodeBool(forKey: "IsDummyDOB")
        
        if decoder.containsValue(forKey: "RelationWithAB") && (decoder.decodeObject(forKey: "RelationWithAB") != nil || decoder.decodeInteger(forKey: "RelationWithAB") != -1) {
            
            if let rNum = decoder.decodeObject(forKey: "RelationWithAB") as? NSNumber {
                
                
                
                self.relationWithAkerBrygge = AkerBryggeRelation(rawValue: rNum.intValue)!
            }
            else {
            
                self.relationWithAkerBrygge = AkerBryggeRelation(rawValue: decoder.decodeInteger(forKey: "RelationWithAB"))!
            }
        }
        else {
            self.relationWithAkerBrygge = .Undefined
        }
        if decoder.containsValue(forKey: "DoSubscribeNewsletter") {
            
            self.doSubscribeNewsletter = decoder.decodeInt32(forKey: "DoSubscribeNewsletter")
        }
        if decoder.containsValue(forKey: "Roles") {
            self.roles = decoder.decodeObject(forKey: "Roles")! as! Array<Dictionary<String, AnyObject>>
        }
        if decoder.containsValue(forKey: "Modules") {
            self.modules = decoder.decodeObject(forKey: "Modules")! as! Array<Dictionary<String, AnyObject>>
        }
        if decoder.containsValue(forKey: "AccountID") {
            self.accountId = decoder.decodeObject(forKey: "AccountID") as? NSNumber
        }
    }
    
    //    override func isEqual(_ object: Any?) -> Bool {
    //        return false
    //    }
    
    //    override func isEqual(_ object2: Any?) -> Bool {
    //
    //        if let object:UserEntity = object2 as? UserEntity {
    //
    //        if (object.firstName?.count > 0 || self.firstName?.count > 0) && object.firstName != self.firstName) {
    //            return false
    //        }
    //        if (object.lastName.length > 0 || self.lastName.length > 0) && !(object.lastName == self.lastName) {
    //            return false
    //        }
    //        if (object.contactNumber.length > 0 || self.contactNumber.length > 0) && !(object.contactNumber == self.contactNumber) {
    //            return false
    //        }
    //        if (object.postalCode.length > 0 || self.postalCode.length > 0) && !(object.postalCode == self.postalCode) {
    //            return false
    //        }
    //        if CInt(object.dob) != CInt(self.dob) {
    //            return false
    //        }
    //        if object.gender != self.gender {
    //            return false
    //        }
    //        if object.doSubscribeNewsletter != self.doSubscribeNewsletter {
    //            return false
    //        }
    //        if (object.email.length > 0 || self.email.length > 0) && !(object.email == self.email) {
    //            return false
    //        }
    //        return true
    //        }
    //        return false
    //    }
    
    func getAllRoles() -> String {
        var roles = ""
        for role: [String: AnyObject] in self.roles {
            
            if roles.characters.count == 0 {
                if let numRole:NSNumber =  role["RoleID"] as? NSNumber {
                    roles = "\(numRole.intValue)"
                }
            }
            else {
                if let numRole:NSNumber =  role["RoleID"] as? NSNumber {
                     roles = "\(roles),\(numRole.intValue)"
                }
            }
        }
        return roles
    }
    
    func getAllModules() -> String {
        var modules = ""
        for module: [AnyHashable: Any] in self.modules {
            
            if modules.characters.count == 0 {
                if let numModule:NSNumber =  module["ModuleID"] as? NSNumber {
                    modules = "\(numModule.intValue)"
                }
            }
            else {
                if let numModule:NSNumber =  module["ModuleID"] as? NSNumber {
                    modules = "\(modules),\(numModule.intValue)"
                }
            }
        }
        return modules
    }
    
    func isModuleExist(_ moduleID: Int) -> Bool {
        for dictionary: [String: AnyObject] in self.modules {
            if (dictionary["ModuleID"] as! Int) == moduleID {
                return true
            }
        }
        return false
    }
    
    func newUserEntityObject() -> UserEntity{
        return UserEntity(json: self.toJSON()!)!
    }
    
    
}



