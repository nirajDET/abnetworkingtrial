//
//  ActivityManager.swift
//  Aker_Brygge
//
//  Created by Paresh on 05/12/16.
//  Copyright © 2016 DET. All rights reserved.
//

import Foundation

@objc class ActivityManager:NSObject {
    static let sharedInstance = ActivityManager()
    
    func postUserActivity(withContentType contentType: CampaignActivityType, userAction actionType: UserActionType, withContentID contentID: NSNumber, callback: @escaping (_ succeeded: Bool, _ response: Any) -> Void) {
        let requestURL = "UserActivity"
        let dictParameters:[String : String] = [
            "UserAction" : "\(actionType.rawValue)" as String,
            "ContentType" : "\(contentType.rawValue)" as String,
            "ContentID" : contentID.stringValue
        ] as [String : String]
        
        NetworkingManager.sharedInstance.postRequest(dictParameters as AnyObject, url: requestURL, callback: {(_ succeeded: Bool, _ response: Any) -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                callback(succeeded, response)
            })
        })
    }
    
    func postUserActivity(_ parameters: [AnyHashable: Any], callback: @escaping (_ succeeded: Bool, _ response: Any) -> Void) {
        let requestURL = "UserActivity"
        NetworkingManager.sharedInstance.postRequest(parameters as AnyObject, url: requestURL, callback: {(_ succeeded: Bool, _ response: Any) -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                
                let appDelegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.writeToTextFile(with: "\n postUserActivity with succeesed \(succeeded) response: \(response) ", name: "Beacon_Trigeering")
                callback(succeeded, response)
            })
        })
    }
}
