//
//  AppConstants.h
//
//  Created on 11/8/14.
//  Copyright (c) 2014 Dynamic Elements Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#define ACCOUNTBLACK_LOGO           0
#define ACCOUNTWHITE_LOGO           1
#define ACCOUNTIMAGE_THUMB          2
#define ACCOUNTIMAGE_FULL           3
#define OFFERIMAGE_THUMB            4
#define OFFERIMAGE_FULL             5
#define INSTAGRAM_THUMB             6
#define INSTAGRAM_FULL              7
#define EVENTIMAGE_THUMB            8
#define EVENTIMAGE_FULL             9
#define CITYEXPERIENCE_THUMB        10
#define CITYEXPERIENCE_FULL         11
#define ARTICLE_THUMB               12
#define ARTICLE_FULL                13
#define ACCOUNT_INSTA_LOGO          14
#define ACCOUNT_PICKUP_IMAGE        15

#define kMapContentType             @"mapcontenttype"
#define kAccountType                @"accounttype"
#define kCampaignActivityType       @"campaignactivitytype"
#define kCityExperienceStartTag     999
#define kOfferStartTag              1999

//Size of feed tiles
#define SQUARE_TILE            0
#define LARGE_SQUARE_TILE      1
#define RECTANGLE_TILE         2
#define DYNAMIC_TILE           3
#define LARGE_RECTANGLE_TILE   4

#define APPLICATION_VERSION             [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:(v) options:NSNumericSearch] != NSOrderedAscending)

//Mobile Payment Notifications
#define FASTTRACK_PAYMENT_DONE          @"FastTrackMobilePaymentDone"
#define OFFER_PAYMENT_DONE              @"OfferMobilePaymentDone"

//Mobile Payment Type
#define PAYMENT_TYPE_FASTTRACK 1
#define PAYMENT_TYPE_OFFER     2

//APIs
//#define SIGN_IN_URL                     @"api/User/SignIn"
//#define SIGN_UP_URL                     @"api/User/Signup"
//#define REGISTER_DEVICE_URL             @"api/user/RegisterDevice"
//#define GET_ALL_INTEREST                @"api/Interests"
//#define GET_ALL_CATEGORIES              @"api/Categories"
//#define GET_FEEDS                       @"api/Feeds/Get"
//#define UPDATE_USER                     @"api/User/UpdateUser"
//#define GET_FAVOURITE_BUSOWNER          @"api/User/"
#define GET_NOTIFICATION_CONTENT        @"BeaconLocalContentList"
#define GET_ALL_BEACONS                 @"BeaconList"
#define GET_ALL_CITYEXPERIENCE          @"api/CityExperiences/GetAll"
#define POST_ACTIVITY                   @"api/User/PostActivity"
#define GET_CAMPAIGN_ACTIVITIES_DETAIL  @"CampaignActivity"
#define NEW_GET_ALL_CATEGORIES          @"AccountCategoryList"
#define NEW_GET_ALL_ACCOUNTS            @"AccountList"
#define NEW_GET__ACCOUNT_DETAILS        @"Account"
#define GET_FASTTRACK_ACCOUNTS          @"FastTrack/AccountList"
#define GET_FASTTRACK_EMAIL_RECEIPT     @"FastTrack/OrderReceiptEmail/"

#define GET_FASTTRACK_PRODUCT_CATEGORY  @"Admin/FastTrack/ProductCategoryList"
#define FASTTRACK_PRODUCT_CATEGORY      @"Admin/FastTrack/ProductCategory"
#define GET_FASTTRACK_PRODUCTLIST       @"Admin/FastTrack/ProductList"
#define FASTTRCK_PRODUCT                @"Admin/FastTrack/Product"
//#define FASTTRACK_DELETE_TEMP           @"FastTrack/Product"

#define POST_FASTTRACK_ORDER            @"FastTrack/Order"
#define POST_FASTTRACK_PAID_ORDER       @"FastTrack/Order/Paid"
#define GET_FASTTRACK_LAST_ORDER        @"FastTrack/LastOrder"
#define GET_FASTTRACK_PRODUCTS          @"FastTrack/AccountProducts/"
#define GET_PRODUCTS_BYPRODUCTID        @"FastTrack/Product/"
#define GET_ACTIVE_ORDERS               @"FastTrack/ActiveOrders"
#define GET_OLD_ORDERS                  @"FastTrack/OldOrders"
#define GET_CONCIERGE_CATEGORYLIST      @"Concierge/CategoryList"
#define POST_CONCIERGE_NEWREQUEST       @"Concierge/NewRequest"
#define GET_CONCIERGE_REQUESTLIST       @"Concierge/RequestList"
#define GET_CONCIERGE_REQUEST           @"Concierge/Request/"
#define DELETE_CONCIERGE_REQUEST        @"Concierge/Request/"
#define GET_CONCIERGE_CONVERSATIONS     @"Concierge/Conversations/"
#define GET_ADMIN_CONCIERGE_REQUESTLIST @"Admin/Concierge/RequestList"
#define POST_IMAGE_TO_SERVER            @"Image/"


#define GET_OFFER_RECEIPT               @"Offer/Redemption/ReceiptList/"
#define OFFER_EMAIL_RECEIPT             @"Offer/RedemptionReceiptEmail/"
#define GET_NOTIFICATION_COUNT          @"NotificationBadgeCount"

#define GET_ADMIN_ACCOUNT               @"Admin/Account"

//WebService Response Keys
#define RESPONCE_DATA_KEY               @"Data"
#define NEW_RESPONCE_DATA_KEY           @"Data"
#define EMAIL_ID                        @"emailId"
#define REQUEST_KEY                     @"RequestType"
#define ERROR_CODE_KEY                  @"Code"
#define ERROR_MESSAGE                   @"Message"
#define NEW_ERROR_MESSAGE               @"Message"
#define NEW_ERROR_CODE_KEY              @"Code"

typedef enum {
    
    CampaignActivities = 0,
    UserOrderStatus,
    AdminNewOrder,
    ConciergeConversation,
    AdminConciergeNewRequest,
    BeaconContentUpdate,
    UserOfferOrderStatus,
    AdminNewOfferOrder,
    SuperAdminNewApprovalRequest,
    AdminCampaignActivityApproved,
    SuperAdminCampaignActivity
}MessageType;

#define PUBNUM_ORDER_STATUS             @"ORDER_STATUS_UPDATE"
#define PUBNUM_RECEIVED_NEW_ORDER       @"RECEIVED_NEW_ORDER"
#define PUBNUM_NEW_CHAT_MESSAGE         @"NEW_CHAT_MESSAGE"
#define PUBNUM_NEW_CONCIERGE_REQUEST    @"NEW_CONCIERGE_REQUEST"
#define PUBNUM_RECEIVED_NEW_OFFER_ORDER @"RECEIVED_NEW_OFFER_ORDER"

//Map Pin Color
#define MAP_PIN_WHITE         @"white_map_dot"
#define MAP_PIN_PINK          @"pink"

//Error Codes
#define ERROR_CODE_FAILURE                  0
#define ERROR_CODE_SUCCESS                  1
#define ERROR_CODE_SUCCESS_SIGN_UP          2
#define ERROR_CODE_EMAIL_ALREADY_EXIST      3
#define ERROR_CODE_DEVICE_ALREADY_EXISTS    4
#define ERROR_CODE_USER_NOT_EXIST           5
#define ERROR_CODE_NO_DATA_FOUND            6
#define ERROR_CODE_INTERNAL_SERVICE_ERROR   7
#define ERROR_CODE_OFFER_NOT_AVAILABLE      8
#define ERROR_CODE_OFFER_EXPIRED            9
#define ERROR_CODE_REDEMPTION_LIMIT_OVER    10
#define ERROR_CODE_NO_INTERNET_CONNECTION   @"-1"

//API response notifications

#define WINDOW           [(AppDelegate *)[[UIApplication sharedApplication] delegate] window]
#define appDelegate      ((AppDelegate *)[[UIApplication sharedApplication] delegate])


#define IS_IPHONE        (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_IPHONE_5      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 568.0)
#define IS_IPHONE_6      (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 667.0)
#define IS_IPHONE_6PLUS  (IS_IPHONE && [[UIScreen mainScreen] nativeScale] == 3.0f)
#define IS_IPHONE_6_PLUS (IS_IPHONE && [[UIScreen mainScreen] bounds].size.height == 736.0)
#define IS_RETINA        ([[UIScreen mainScreen] scale] == 2.0)


//Process Indicator Message
#define PROCESS_INDICATOR_MSG       @"Processing..."

#define  ABOUT_US                   @"AboutUs"
#define  CONTACT_US                 @"ContactUs"
#define  SERVICE_CATALOG            @"ServiceCatalog"
#define  E_SERVICE                  @"EService"
#define  NEWS                       @"News"
#define  OTHERS                     @"Other"

//FEED REQUEST DICTIONARY KEYS
#define  REQUEST_KEY_FEED_TYPE                   @"FeedType"
#define  REQUEST_KEY_CURRENT_USER_ID             @"UserID"
#define  REQUEST_KEY_BUSI_OWNER_ID               @"BusinessOwnerId"
#define  REQUEST_KEY_LOCATION                    @"Location"
#define  REQUEST_KEY_SORUCE_TYPE                 @"SourceType"


#define BUSINESS_OWNER_PLIST_FILE   @"BusinessOwner.plist"
#define USER_PLIST_FILE             @"User.plist"

#define UPPER_LEFT_LATITUDE         @""
#define BOTTOM_RIGHT_LATITUDE       @""
#define NS_IS_PUSH_NOTIFICATION_ON  @"ISPushNotificationON"
#define PLACEHOLDER_IMAGE           @"cente_icon_Feed"
#define PLACEHOLDER_GALLERY         @"center_icon_Insta"

//LAZY Loading PageSize
#define LAZY_LOADING_PAGE_SIZE      @"15"
#define NSStringFromBOOL(aBOOL)     aBOOL? @"true" : @"false"

//colors
#define PEACH_TEXT_COLOR            [UIColor colorWithRed:255.0/255.0 green:104.0/255.0 blue:116.0/255.0 alpha:1.0]
#define LIGHT_PEACH_COLOR           [UIColor colorWithRed:252.0/255.0 green:226.0/255.0 blue:216.0/255.0 alpha:1.0]
#define VIEW_BACKGROUND_COLOR       [UIColor colorWithRed:34.0/255.0 green:34.0/255.0 blue:34.0/255.0 alpha:1.0]
#define HIGHLIGHTED_TEXT_COLOR      [UIColor colorWithRed:255.0/255.0 green:102.0/255.0 blue:113.0/255.0 alpha:1.0]
#define GREEN_COLOR                 [UIColor colorWithRed:105.0/255.0 green:136.0/255.0 blue:72.0/255.0 alpha:1.0]
#define BORDER_GRAY_COLOR           [UIColor colorWithRed:112.0/255.0 green:104.0/255.0 blue:104.0/255.0 alpha:1.0]
#define BORDER_PINK_COLOR           [UIColor colorWithRed:241.0/255.0 green:103.0/255.0 blue:117.0/255.0 alpha:1.0]
#define BORDER_BROWN_COLOR          [UIColor colorWithRed:136.0/255.0 green:80.0/255.0 blue:60.0/255.0 alpha:1.0]
#define BORDER_GREEN_COLOR          [UIColor colorWithRed:87.0/255.0 green:119.0/255.0 blue:59.0/255.0 alpha:1.0]

//web view link
#define TERMS_CONDITION_URL         @"http://www.akerbrygge.no/aker-brygges-personvernerklaering/"

//Font
#define MENU_BUTTON_TITLE           @"Helvetica"
#define FONT_AKER_BRYGGE_DISPLAY    @"AKER BRYGGE DISPLAY"
#define AKERBRYGGEDISPLAY_Normal    @"AKERBRYGGEDISPLAY-Normal"
#define TSTAR_MOD_BOLD              @"TSTARMOD-Bold"
#define TSTAR_MOD_REGULAR           @"TSTAR MOD"
#define TSTAR_MOD_MEDIUM            @"TSTARMOD-Medium"
#define GEORGIA_REGULAR             @"Georgia"
#define GEORGIA_ITALIC              @"Georgia-Italic"

#define PROFILE_FONT_SIZE           15
#define MAP_FONT_SIZE               15

//main screen size
#define WIDTH                       [UIScreen mainScreen].bounds.size.width
#define HEIGHT                      [UIScreen mainScreen].bounds.size.height

//label font
#define CLOSE_CODE                  "m"
#define ARROW_UP_CODE               "l"
#define ARROW_DOWN_CODE             "k"
#define ARROW_NEXT_CODE             "i"
#define ARROW_PREVIOUS_CODE         "j"
#define FEED_CODE                   "g"
#define USER_MAN_CODE               "e"
#define USER_FEMALE_CODE            "f"
#define MAP_CODE                    "d"
#define INFINITY_CODE               "\uinfinity"

//Date Formate
#define DOB_FORMATE                  @"dd/MM/yyyy"

//Cell Identifier
#define CATEGORY_SUB_CELL_IDENTIFIER         @"CategorySubCell"
#define COLL_CATEGORY_ITEM_CELL              @"CategoryItemCell"
#define COLL_INTEREST_CELL_IDENTIFIER        @"InterestCell"
#define COLL_CELL_HEADER                     @"CollectionViewHeader"
#define COLL_CELL_BUSIOWNER                  @"BusinessOwnerCell"
#define COLL_CELL_ARTICLE                    @"ArticleListCell"
#define COLL_CELL_NO_DATA                    @"NODataHeader"
#define TABLE_CELL_PUSH_NOTIF                @"PushNotificationCell"
#define TABLE_CELL_EMPLY_PUSH_NOTIF          @"EmptyNotificationCell"
#define TABLE_CELL_OFFER_PUSH_NOTIF          @"OfferNotificationCell"
#define ARTICLE_LIST_CELL_IDENTIFIER         @"ArticleListCell"
#define ARTICLE_CELL_IDENTIFIER              @"ArticleCell"
#define BRIEF_OFFER_CELL                     @"Brief Offer Cell"
#define OFFER_CELL                           @"Offer Cell"
#define USER_PROFILE_CELL                    @"User Profile Cell"
#define USERNAME_CELL_IDENTIFIER             @"UserNameCell"
#define FIRSTNAME_CELL_IDENTIFIER            @"FirstNameCell"
#define LASTNAME_CELL_IDENTIFIER             @"LastNameCell"
#define GENDER_CELL_IDENTIFIER               @"GenderCell"
#define EMAIL_CELL_IDENTIFIER                @"EMailCell"
#define DATEPICKER_CELL_IDENTIFIER           @"DatePickerCell"
#define POSTNUMBER_CELL_IDENTIFIER           @"PostNumberCell"
#define SUBCRIBEME_CELL_IDENTIFIER           @"SubcribeMeCell"
#define TERMS_AND_COND_CELL_IDENTIFIER       @"TermsAndConditionCell"
#define ART_CELL_IDENTIFIER                  @"ArtImageViewCell"
#define CUSTOM_MAP_CELL_IDENTIFIER           @"CustomMapViewCell"
#define OPENING_HOUR_CELL_IDENTIFIER         @"OpeningHourCell"


//Activity Acbtion Type
typedef enum {
    ActionType_Impression=0,
    ActionType_Open,
    ActionType_Like ,
    ActionType_Share,
    ActionType_Forward,
    ActionType_Comment,
    ActionType_BookTable,
    ActionType_OfferRedemption,
    ActionType_RSPV_AttendEvent,
    ActionType_Unlike,
    ActionType_Save,
    ActionType_Unsave,
    ActionType_Notification
}ActionType;

//Activity Type
typedef enum {
    ActivityType_Feed=0,
    ActivityType_BusinessOwner,
    ActivityType_Offer,
    ActivityType_CityExperience,
}ActivityType;

//Category Type
typedef enum {
    isNormal,
    isService,
    isSeverdigheter
}CategoryType;


//Types of NotificationType
typedef enum {
    NotifTypeOffer,
    NotifTypeNotification,
    NotifTypeWebView,
    NotifTypeVideo
}NotificationType;


//Beacon Proximity
typedef enum {
    ProximityImmediate = 0,
    ProximityNear      = 1,
    ProximityFar       = 2,
    ProximityEnter     = 3,
    ProximityExit      = 4,
    ProximityUnknown   = 5
}BeaconProximity;


typedef enum TypeWebView : NSInteger TypeWebView;
enum TypeWebView:NSInteger{
    TypeWebViewTermsCondition,
    TypeWebViewBookATable,
    TypeWebViewInstagram,
    TypeWebViewWebsite,TypeWebViewFeedWebsite
};



typedef enum ApplicationTypeOption : NSInteger ApplicationTypeOption;
enum ApplicationTypeOption : NSInteger {
    Option_None = -1,
    Option_Check,
    Option_Amendment,
    Option_Re_Check
};


typedef enum NotificationFeedTypeOption : NSInteger NotificationFeedTypeOption;
enum NotificationFeedTypeOption : NSInteger {
    NotificationFeedTypeArticle = 0,
    NotificationFeedTypeEvent = 1,
    NotificationFeedTypeInstagram = 2,
    NotificationFeedTypeMessage = 3,
    NotificationFeedTypeOffer = 4,
    NotificationFeedTypeCityExperience = 5,
};

//Screen Type
typedef enum NavigateToScreen : NSInteger NavigateToScreen;
enum NavigateToScreen:NSInteger{
    nsLoginView = 0,
    nsBODetailView,
    nsFeedView,
    nsMapView,
    nsCategorySubCategoryView,
    nsCategoryDetailView,
    nsInterestView,
    nsTempFeed,
    nsTermsAndCondView,
    nsArticleListView,
    nsArticleView,
    nsCityExperienceView,
    nsHistoricalPlaceView,
    nsOffersView,
    nsHvaSkjerView,
    nsUserProfile,
    nsUserDetailView,
    nsFavouriteView,
    nsInstaDetailView,
    nsOfferDetailView,
    nsCalenderView,
    nsTabBarControl
};


#define USER_DEFAULT_LOCAL_USER_DATA             @"SavedUser"
#define USER_DEFAULT_LOCAL_DEVICE_ID             @"DeviceID"
#define USER_DEFAULT_DEVICE_TOKEN                @"DeviceToken"
#define USER_DEFAULT_BUS_OWNER_FETCH_DATE        @"UpdatedBusiOwnerDay"
#define USER_DEFAULT_BEACON_FETCH_DATE           @"UpdatedBeaconDay"
#define USER_DEFAULT_LOCAL_CATEGORY_DATA         @"SavedCategory"
#define USER_DEFAULT_LOCAL_ACCOUNT_DATA          @"SavedAccount"
#define USER_DEFAULT_LOCAL_BEACON_DATA           @"Beacons"

//User default storage
#define LAST_ACTIVE_SCREEN      @"LastActiveScreen"

//Regex
#define REGEX_SIGNUP_DATE       @"^(0[1-9]{1}|[12]{1}[0-9]{1}|3[01]{1}).{1}(0[1-9]{1}|1[0-2]{1}).{1}([12]{1}[0-9]{3})$"
#define REGEX_AGE               @"^[0-9]{1,2}$"
#define REGEX_NAME              @"^[ÀÁÂÃÄÅĀÆàáâãäåāæa ÇĆČçćč ÈÉÊËèéêë ÌÍÎÏìíîï Ðð Łł Ññ ÒÓÔÕÖØŒòóôõöøœ Þþ Šš ÙÚÛÜùúûü ŸÝýÿ Žž . a-zA-Z -]+$"
#define REGEX_EMAIL             @"[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
#define REGEX_EMAIL_FOR_PROFILE @"^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"

#define REGEX_POSTAL_CODE       @"^([0-9]{3,7})$"
#define REGEX_PHONE             @"^\\+?[0-9]{6,16}$"
#define REGEX_DATE_OF_BIRTH     @"^(3[0-1]|[1-2][0-9]|(?:0)[1-9])(?:\\|)(1[0-2]|(?:0)[1-9])(?:\\|)(19|20)[0-9]{2}$"




#define NSStringFromBOOL(aBOOL)  aBOOL? @"true" : @"false"

//Map Categories
#define CATEGORY_ALL              @"ALLE"
#define CATEGORY_FAVORITTER       @"FAVORITTER"
#define CATEGORY_FORDELER         @"FORDELER"
#define CATEGORY_SEVERDIGHETER    @"Severdigheter"
#define CATEGORY_BYDELSTRIPS      @"Bydelstips"
#define CATEGORY_OPPLEVELSER      @"Opplevelser"

//Favourite Sections
#define SECTION_OPPLEVELSER     @"Opplevelser"
#define SECTION_SERVERDIGHETER  @"Severdigheter"
#define SECTION_FIRMA           @"Firma"
#define SECTION_ARTIKLER        @"Artikler"

//Norwegian text
#define STORE_OPEN                      @"Åpent Nå"
#define STORE_CLOSE                     @"Stengt"
#define MESSAGE                         @"Din Aker Brygge"
#define OFFER_REDEEM_MESSAGE            @"tilbudet innløst"
#define URL_NOT_FOUND                   @"Fant ikke websiden"
#define USER_NOT_EXIST_MESSAGE          @"Brukeren eksisterer ikke"
#define LOADING_MESSAGE                 @"Laster inn ..."
#define EMAIL_ALREADY_EXIST             @"Epostadressen er benyttet før"
#define DEVICE_IS_EXIST                 @"Enheten finnes allerede"
#define NO_DATA_FOUND                   @"Ingen data funnet"
#define MSG_OFFER                       @"Ingen fordeler er lagret"
#define MSG_HVA_SKJER                   @"Det er ingen Aktiviteter på dette tidspunktet"
#define INTERNAL_SERVICE_ERROR          @"Noe gikk galt. Vennligst prøv igjen senere"
#define MSG_VALIDATION_DATE             @"Vennligst legg inn dato som dd|mm|yyyy"
#define MSG_DATE                        @"Skriv inn gyldig dato"
#define MSG_VALIDATION_PHONE            @"Telefonnummeret er ikke gyldig"
#define MSG_VALIDATION_POSTAL_CODE      @"Postnummeret er ikke gyldig"
#define MSG_EMAIL_RECEIPT_SENT          @"Receipt has been sent to your registered email address."
#define NO_INTERNET_MESSAGE             @"Internett er ikke tilgjengelig"
#define MSG_BOOKMARKS_NOT_FOUND         @"Bokmerkene er ikke tilgjengelig"
#define MSG_ACCOUNT_NOT_FOUND           @"Du har ingen lagrede favoritt-forretninger"
#define MSG_NO_FAVOURITE                @"Du har ingen lagrede favoritter"
#define MSG_CONTACT_ALREADY_SAVED       @"Kontakten eksisterer allerede"
#define MSG_FAILED_TITLE                @"Feilet"
#define MSG_FAILED_PUSH_NOTIFICATION    @"Push Notification-registrering Mislyktes"
#define MSG_CONTACT_NOT_SAVED           @"Kontakten ble ikke lagret"
#define MSG_CONTACT_SAVED               @"Kontakten er lagret"
#define MSG_ACCESS_DENIED               @"Tilgang avslått"
#define MSG_ERROR                       @"En feil oppstod"
#define MSG_SUCCESS                     @"Suksess"
#define MSG_PRIVACY_SETTINGS            @"Vennligst gå til Innstillinger og endre innstillngene for Aker Brygge"
#define MSG_SAVED                       @"Lagret"
#define METHOD_NAME_DICT_KEY            @"MethodName"
#define OFFER_REDEMPTION_BUTTON_TITLE   @"LØS INN TILBUD NÅ"
#define OFFER_REDEEMED_BUTTON_TITLE     @"Tilbudet er løst inn"
#define OFFER_REDEMPTION_ALERT_TITLE    @"Løs inn tilbud nå"
#define OFFER_REDEMPTION_ALERT_MESSAGE  @"Dette er siste gang du kan benytte dette tilbudet"
#define OFFER_REDEMPTION_ALERT_OK       @"OK"
#define OFFER_REDEMPTION_ALERT_CANCEL   @"Avbryt"
