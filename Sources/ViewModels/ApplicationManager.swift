//
//  ApplicationManager.swift
//  Aker_Brygge
//
//  Created by Paresh on 07/12/16.
//  Copyright © 2016 DET. All rights reserved.
//

import Foundation
import CoreBluetooth

@objc class ApplicationManager:NSObject, CBCentralManagerDelegate {
    
    var bluetoothManager: CBCentralManager!
    var arCategory: [CategoryModel] = []
    var arrAcounts: [AccountModel] = []
    var arrCityExperience: [CampaignActivity] = []
    var arrFavourites: [CampaignActivity] = []
    var arrOffers: [CampaignActivity] = []
    var strBluetoothState: String = ""
    var redeemedInfo: Dictionary<String,AnyObject> = [:]
    
    static let sharedInstance = ApplicationManager()
    
    func initialize() {
        self.initializeBluetoothManager()
    }
    
    override init() {
        super.init()
        self.initialize()
    }
    
    // MARK: - Identify Bluetooth State
    
    func initializeBluetoothManager() {
        self.strBluetoothState = "false"
        self.bluetoothManager = CBCentralManager(delegate: self, queue: nil, options: [ CBCentralManagerOptionShowPowerAlertKey : Int(0) ])
    }
    //This delegate method will monitor for any changes in bluetooth state and respond accordingly
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        var stateString: String = ""
        switch bluetoothManager.state {
        case .resetting:
            stateString = "false"
        case .unsupported:
            stateString = "false"
        case .unauthorized:
            stateString = "false."
        case .poweredOff:
            stateString = "false"
        case .poweredOn:
            stateString = "true"
            
            if UserManager.sharedInstance().isUserLoggedIn == true {
                BeaconManager.sharedInstance.getBeaconData(completionHandler: {(_ succeeded: Bool, _ response: Any) -> Void in
                    if succeeded {
                        BeaconManager.sharedInstance.prepareBeaconsAndInitAllRegions(locationManager: appDelegate.locationManager)
                    }
                })
            }
            
        default:
            stateString = "false"
        }
        
        self.strBluetoothState = stateString
    }
    
    // MARK: - Get All Categories
    
    func getAllCategories_WSCallback(_ callback: @escaping (_ succeeded: Bool, _ response: Any) -> Void) {
        //UserEntity *userEntity = (UserEntity *)[[ApplicationManager sharedInstance] getUserFromLocal];
        //NSString *requestURL = [NSString stringWithFormat:@"%@/%@",NEW_GET_ALL_CATEGORIES,userEntity.UserId];
        self.pleaseWait()
        let requestURL = "\(NEW_GET_ALL_CATEGORIES)"
        NetworkingManager.sharedInstance.getRequest(requestURL, callback: {(_ succeeded: Bool, _ response: Any?) -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                self.clearAllNotice()
                if succeeded == true {
                    if (response is Array<Any>) && response != nil {
                        //Parse All Categories
                        
                        
                        
                        ParsingManager.sharedInstance.parseCategories(response as! Array<Dictionary<String, AnyObject>>, completionHandler: {(_ arrCategories: [CategoryModel]) -> Void in
                            callback(true, arrCategories)
                        })
                    }
                    else {
                        callback(false, response as Any)
                    }
                }
                else {
                    callback(false, response as Any)
                }
            })
        })
    }
    
    func getAllAccounts_WSCallback(_ callback: @escaping (_ succeeded: Bool, _ response: Any) -> Void) {
        self.pleaseWait()
        let requestURL = "\(NEW_GET_ALL_ACCOUNTS)"
        NetworkingManager.sharedInstance.getRequest(requestURL, callback: {(_ succeeded: Bool, _ response: Any?) -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                self.clearAllNotice()
                if succeeded == true {
                    //NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
                    //id responseObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                    if (response is Array<Any>) && response != nil {
                        //Parse All Categories
                        ParsingManager.sharedInstance.parseAccounts(response as! Array<Dictionary<String, AnyObject>>, completionHandler: {(_ arrAcounts: [AccountModel]) -> Void in
                            //Notify controller on success response from server
                            callback(true, arrAcounts)
                        })
                    }
                    else {
                        callback(false, response as Any)
                    }
                }
                else {
                    callback(false, response as Any)
                }
            })
        })
    }
    
    // MARK: - Get All CityExperience
    func getAllCityExperience_WSCallback(_ callback: @escaping (_ succeeded: Bool, _ response: Any) -> Void) {
        self.pleaseWait()
        let requestURL = "CampaignActivityList/11/0/0"
        NetworkingManager.sharedInstance.getRequest(requestURL, callback: {(_ succeeded: Bool, _ response: Any?) -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                self.clearAllNotice()
                if succeeded {
                    if (response is [Any]) && response != nil {
                        //Parse All Categories
                        ParsingManager.sharedInstance.parseCampaignActivities(response as! Array<Dictionary<String, AnyObject>>, completionHandler: {(_ arrCampaignActivites: [CampaignActivity]) -> Void in
                            callback(true, arrCampaignActivites)
                        })
                    }
                    else {
                        callback(false, response as Any)
                    }
                }
                else {
                    callback(false, response as Any)
                }
            })
        })
    }
    
    func getFavouritesWithCallback(_ callback: @escaping (_ succeeded: Bool, _ response: Any) -> Void) {
        self.pleaseWait()
    
        UserWsManager.sharedInstance.getUserFavouritesFromServer(callback: {(_ succeeded: Bool, _ response: Any?) -> Void in
            DispatchQueue.main.async(execute: {() -> Void in
                self.clearAllNotice()
                if succeeded == true {
                    var arrTempFavourites:[CampaignActivity] = []
                    for campaignActivity: CampaignActivity in (response as! [CampaignActivity]) {
                        arrTempFavourites.append(campaignActivity)
                    }
                    self.arrFavourites = arrTempFavourites
                    callback(true, response as Any)
                }
                else {
                    callback(false, response as Any)
                }
            })
        })
    }
    
    // MARK: - Local Storage
    
    func storeUser(toLocal userObj: UserEntity) {
        let prefs = UserDefaults.standard                
        let myEncodedObject = NSKeyedArchiver.archivedData(withRootObject: userObj)
        prefs.set(myEncodedObject, forKey: USER_DEFAULT_LOCAL_USER_DATA)
    }
    
    func removeUserFromLocal() {
        let prefs = UserDefaults.standard
        prefs.removeObject(forKey: USER_DEFAULT_LOCAL_USER_DATA)
    }
    
    func getUserFromLocal() -> UserEntity? {
        let prefs = UserDefaults.standard
        let myEncodedObject:NSData? = prefs.object(forKey: USER_DEFAULT_LOCAL_USER_DATA) as! NSData?
        
        if myEncodedObject != nil {
            
            do{
                let savedUserEntity:UserEntity? = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(myEncodedObject! as NSData) as? UserEntity
                if savedUserEntity != nil {
                    if (savedUserEntity!.userId) > 0 {
                        if (savedUserEntity!.dob != nil) && savedUserEntity!.dob!.intValue <= 0 {
                            savedUserEntity!.dob = 0
                        }
                        //Consider user as dummy if email address is not available
                        if (savedUserEntity!.email != nil) && savedUserEntity!.email!.characters.count > 0 {
                            savedUserEntity!.isDummy = false
                        }
                        else {
                            savedUserEntity!.isDummy = true
                        }
                        
                        if savedUserEntity!.dob != nil && AppUtils.isDateValid(savedUserEntity!.dob!.stringValue) {
                            let strAge:String = AppUtils.getAgeFromDate(savedUserEntity!.dob!.stringValue)
                            savedUserEntity!.dob = NSNumber(value:Int(strAge)!)
                            self.storeUser(toLocal: savedUserEntity!)
                        }
                        
                        return savedUserEntity
                    }
                    else {
                        return nil
                    }
                }
                else{
                    return nil
                }
            } catch {
                prefs.removeObject(forKey: USER_DEFAULT_LOCAL_USER_DATA)
                return nil
            }
        }
        else{
            return nil
        }
        
    }

    func storeCategories(toLocal arrCategory: [Any]) {
        let prefs = UserDefaults.standard
        let myEncodedObject = NSKeyedArchiver.archivedData(withRootObject: arrCategory)
        prefs.set(myEncodedObject, forKey: USER_DEFAULT_LOCAL_CATEGORY_DATA)
    }
    
    func storeAccounts(toLocal arrCategory: [Any]) {
        let prefs = UserDefaults.standard
        let myEncodedObject = NSKeyedArchiver.archivedData(withRootObject: arrCategory)
        prefs.set(myEncodedObject, forKey: USER_DEFAULT_LOCAL_ACCOUNT_DATA)
    }
    // MARK: - Calculate Control Height
    
    func calculateStringHeight(_ str: String, andFont font: UIFont, width: Float) -> Float {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
        //set the line break mode
        let attrDict = [
            NSFontAttributeName : font,
            NSParagraphStyleAttributeName : paragraphStyle
            ] as [String : AnyObject]
        
        let size = str.boundingRect(with: CGSize(width: CGFloat(width), height: CGFloat(MAXFLOAT)), options: [.truncatesLastVisibleLine, .usesLineFragmentOrigin], attributes: attrDict, context: nil).size
        return Float(size.height) + 75.0
    }
    
    func calculateStringHeight(_ str: String, andFont font: UIFont, withLineSpacing lineSpace: Int) -> Float {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = CGFloat(lineSpace)
        paragraphStyle.alignment = .center
        paragraphStyle.lineBreakMode = NSLineBreakMode.byWordWrapping
        //set the line break mode
        let attrDict = [
            NSFontAttributeName : font,
            NSParagraphStyleAttributeName : paragraphStyle
            ] as [String : AnyObject]
        
        let size = str.boundingRect(with: CGSize(width: CGFloat(WIDTH), height: CGFloat(MAXFLOAT)), options: [.truncatesLastVisibleLine, .usesLineFragmentOrigin], attributes: attrDict, context: nil).size
        return Float(size.height) + 75.0
    }
    
    func getLabelHeight(_ label: UILabel) -> CGFloat {
        let constraint = CGSize(width: CGFloat(label.frame.size.width), height: CGFloat(20000.0))
        var size: CGSize
        let context = NSStringDrawingContext()
        let boundingBox = label.text!.boundingRect(with: constraint, options: .usesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: context).size
        size = CGSize(width: CGFloat(ceil(boundingBox.width)), height: CGFloat(ceil(boundingBox.height)))
        return size.height
    }
    // MARK: - Update Favourite in Local
    
    func updateAccountFavourite(_ accountId: Int, favourtieFlag favourite: Bool) {
       
        var results:[AccountModel] = self.arrAcounts
        results = results.filter { (accountModel) -> Bool in
            return accountModel.accountID!.intValue == accountId
        }
        if results.count > 0 {
            let accountModel:AccountModel = results[0] 
            accountModel.isFavourite = NSNumber(value:favourite)
        }
        //From Favourite Array
        
        var results2:[CampaignActivity] =  self.arrFavourites
        results2 = results2.filter { (campaignActivity) -> Bool in
            return campaignActivity.accountID?.intValue == accountId
        }
        if results2.count > 0 {
            let campaignActivity:CampaignActivity = results2[0] 
            campaignActivity.isFavorite = NSNumber(value:favourite)
        }
    }

    func updateCityExperienceFavourite(_ campaignActivityID: Int, favourtieFlag favourite: Bool) {
       
        var results:[CampaignActivity] = self.arrCityExperience
        results = results.filter { (campaignActivity) -> Bool in
            return campaignActivity.accountID!.intValue == campaignActivityID
        }
        if results.count > 0 {
            let campaignActivity:CampaignActivity = results[0] 
            campaignActivity.isFavorite = NSNumber(value:favourite)
        }
        //From Favourite Array
        
        let results2:[CampaignActivity] = self.arrFavourites.filter { (campaignActivity) -> Bool in
            return campaignActivity.campaignActivityID!.intValue == campaignActivityID
        }
        if results2.count > 0 {
            let campaignActivity:CampaignActivity = results2[0] 
            campaignActivity.isFavorite = NSNumber(value:favourite)
        }
    }
    
    func isBusiOwnerNeedsToUpdateFromLastDate() -> Bool {

        if let lastStoredDate:Date = UserDefaults.standard.object(forKey: USER_DEFAULT_BUS_OWNER_FETCH_DATE) as? Date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-ddHH:mm:ss ZZZ"
            let startDate = Date()
            let gregorianCalendar:Calendar = Calendar(identifier: .gregorian)
            let components = gregorianCalendar.dateComponents([.day], from: startDate, to: lastStoredDate)
            if components.day! < 0 {
                return true
            }
            else {
                return false
            }
        }
        else{
            return true
        }
    }
}

