//
//  BeaconManager.swift
//  Din Aker Brygge
//
//  Created by Mihir Mehta on 13/12/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Foundation

class BeaconManager: NSObject {
    //MARK: Shared Instance
    
    static let sharedInstance : BeaconManager = {
        let instance = BeaconManager()
        return instance
    }()
    
    var beaconCampaignActivities:[CampaignActivity]?
    var beacons:[Beacon]?
    var uniqueUUIDs:[Beacon]?
    
    func getBeaconData(completionHandler:@escaping (_ success:Bool,_ error:Error?)-> Void) -> Void {
        let userManager = UserWsManager.sharedInstance
        
        userManager.getBeaconList { (succeeded, response) in
            if succeeded {
                self.beacons = response as! [Beacon]?
                userManager.getBeaconCampaignActivities(callback: { (succeeded, response2) in
                    if succeeded {
                        self.beaconCampaignActivities = response2 as? [CampaignActivity]
                        
                        self.uniqueUUIDs = self.beacons?.uniqueElements
                        
                        completionHandler(true,nil)
                    }
                    else {
                        completionHandler(false,response as? Error)
                    }
                    
                    
                    
                })
            }
            else {
                
                completionHandler(false,response as? Error)
            }
        }
        
    }
    
    func getCampaignActivityWithId(_ activityId:NSNumber) -> CampaignActivity? {
        if self.beaconCampaignActivities == nil || self.beaconCampaignActivities?.count == 0 {
            return nil
        }
        
        let filter = self.beaconCampaignActivities?.filter({ (activity:CampaignActivity) -> Bool in
            return activityId.intValue == activity.campaignActivityID?.intValue
        })
        return filter?.last
    }
    
    func verifyBeacon(_ beacon:Beacon,proximity:NSNumber) -> Void {
        if self.beaconCampaignActivities == nil || self.beaconCampaignActivities?.count == 0 {
            if let jsonArray:Array<Dictionary<String,AnyObject>> = UserDefaults.standard.object(forKey: "BeaconContent") as? Array<Dictionary<String,AnyObject>> {
                ParsingManager.sharedInstance.parseCampaignActivities(jsonArray, completionHandler: { (campainActivities:[CampaignActivity]) in
                    self.beaconCampaignActivities = campainActivities
                })
            }
        }
        if self.beaconCampaignActivities != nil {
            
            
            for campaignActivity:CampaignActivity in self.beaconCampaignActivities! {
                
                // print("proximity: \(proximity)")
                
                let beaconRule:BeaconRules? = campaignActivity.findBeaconRuleFromBeacon(beacon, proximity: proximity)
                if beaconRule != nil {
                    let timeStampNum:NSNumber? = UserDefaults.standard.object(forKey: "activity.campaignActivityID:\(campaignActivity.campaignActivityID)") as? NSNumber
                    if timeStampNum != nil {
                        campaignActivity.lastDisplayedTimestamp = timeStampNum!
                    }
                    else if beaconRule!.lastTimeTriggered != nil {
                        campaignActivity.lastDisplayedTimestamp = NSNumber(value: beaconRule!.lastTimeTriggered!.doubleValue/1000.0)
                    }
                    if beaconRule!.interval == nil || beaconRule!.interval!.intValue <= 0 {
                        beaconRule!.interval = NSNumber(value: 86400)
                    }
                    let timeStamp:TimeInterval = campaignActivity.lastDisplayedTimestamp!.doubleValue + beaconRule!.interval!.doubleValue
                    let currentTimeStamp:TimeInterval = Date().timeIntervalSince1970
                    
                    if timeStamp <= currentTimeStamp {
                        
                        if campaignActivity.startDateTime == nil {
                            campaignActivity.startDateTime = NSNumber(value: 0)
                        }
                        if campaignActivity.endDateTime == nil {
                            campaignActivity.endDateTime = NSNumber(value: 0)
                        }
                        
                        
                        if self.verifyContentTime(startTime: campaignActivity.startDateTime!.doubleValue, endTime: campaignActivity.endDateTime!.doubleValue,currentTime: currentTimeStamp*1000.0) && self.verifyBeaconSchedule(beaconContent: campaignActivity) {
                            
                            self.displayLocalNotification(beaconContent: campaignActivity, beaconRule: beaconRule!)
                            
                            
                            campaignActivity.lastDisplayedTimestamp = NSNumber(value: currentTimeStamp)
                            UserDefaults.standard.setValue(campaignActivity.lastDisplayedTimestamp, forKey: "activity.campaignActivityID:\(campaignActivity.campaignActivityID)")
                        }
                    }
                    
                }
                
                
            }
        }
        
        
    }
    
    func verifyContentTime(startTime:TimeInterval,endTime:TimeInterval,currentTime:TimeInterval) -> Bool {
        
        
        if startTime == 0 && endTime == 0 {
            return true
        }
        else if startTime == 0 && endTime > 0 && currentTime <= endTime {
            return true
        }
        else if startTime > 0 && endTime == 0 && currentTime >= startTime {
            return true
        }
        else if startTime > 0 && endTime > 0 && currentTime >= startTime && currentTime <= endTime {
            return true
        }
        else {
            return false
        }
    }
    func verifyBeaconSchedule(beaconContent:CampaignActivity) -> Bool {
        if beaconContent.BeaconSchedule == nil || beaconContent.BeaconSchedule?.count == 0 {
            return true
        }
        else {
            let today = Date().dateWithFormate("EEEE")
            // var predicate = NSPredicate(format: "self.Day = %@", today)
            let daysArray = beaconContent.BeaconSchedule?.filter({ (model:BeaconScheduleModel) -> Bool in
                return model.Day == today
            })
            for schedule: BeaconScheduleModel in daysArray! {
                
                if AppUtils.checkIfCurrentTimeIsInBetweenTime(from: schedule.From?.doubleValue, openingHoursTo: schedule.To?.doubleValue) {
                    return true
                }
            }
            return false
        }
    }
    func displayLocalNotification(beaconContent:CampaignActivity,beaconRule:BeaconRules) {
        var body = ""
        if beaconContent.campaignActivityType?.intValue == CampaignActivityType.account.rawValue || beaconContent.campaignActivityType?.intValue == CampaignActivityType.accountService.rawValue {
            body = "\(beaconContent.accountName!)"
        }
        else {
            //Use "title" for all other content type
            body = "\(beaconContent.title!)"
        }
        let localNotification = UILocalNotification()
        localNotification.alertBody = body.replacingOccurrences(of: "%", with: "\u{FF05}")
        localNotification.timeZone = NSTimeZone.default
        localNotification.soundName = "Default"
        UIApplication.shared.applicationIconBadgeNumber = UIApplication.shared.applicationIconBadgeNumber + 1
        if beaconContent.categoryID != nil {
            localNotification.userInfo = ["Data": beaconContent.campaignActivityID!, "Type": beaconContent.campaignActivityType!, "SubType": beaconContent.categoryID!]
        }
        else {
            localNotification.userInfo = ["Data": beaconContent.campaignActivityID!, "Type": beaconContent.campaignActivityType!]
        }
        //let delegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        // delegate.presentLocalNotificationNow(localNotification)
        
        UIApplication.shared.presentLocalNotificationNow(localNotification)
        self.postUserActivity(beaconContent: beaconContent, beaconRule: beaconRule)
        
    }
    func postUserActivity(beaconContent:CampaignActivity,beaconRule:BeaconRules) {
        let dictParameters:Dictionary<String,AnyObject> = ["UserAction": UserActionType.beaconTriggered.rawValue as AnyObject, "ContentID": beaconContent.campaignActivityID!, "ContentType": "\(beaconContent.campaignActivityType!)" as AnyObject, "BeaconID": beaconRule.beaconID!, "Proximity": beaconRule.proximity!]
        ActivityManager.sharedInstance.postUserActivity(dictParameters) { (succeeded, response) in
            
        }
        
    }
    func prepareBeaconsAndInitAllRegions(locationManager:CLLocationManager) -> Void {
        if self.uniqueUUIDs != nil && self.uniqueUUIDs!.count > 0 {
            for region:CLRegion in locationManager.monitoredRegions {
                
                if let beaconRegion = region as? CLBeaconRegion {
                    
                    locationManager.stopRangingBeacons(in: beaconRegion)
                }
                locationManager.stopMonitoring(for: region)
            }
            if CLLocationManager.isMonitoringAvailable(for: CLBeaconRegion.classForCoder()) {
                let clBeaconRegions:[CLBeaconRegion] = self.uniqueUUIDs!.filter({ (beacon:Beacon) -> Bool in
                    return beacon.ibeacon!.uuid != nil && UUID(uuidString:  beacon.ibeacon!.uuid!) != nil
                }).map({ (beacon:Beacon) -> CLBeaconRegion in
                    let beaconRegion = CLBeaconRegion(proximityUUID: UUID(uuidString:(beacon.ibeacon?.uuid)!)!, identifier: (beacon.ibeacon?.uuid)!)
                    beaconRegion.notifyOnEntry = true
                    beaconRegion.notifyOnExit = false
                    beaconRegion.notifyEntryStateOnDisplay = true
                    return beaconRegion
                })
                
                for beaconRegion:CLBeaconRegion in clBeaconRegions {
                    locationManager.startMonitoring(for: beaconRegion)
                }
                
                
            }
        }
    }
    func didRangeBeacons( clBeacons:[CLBeacon],inRegion region:CLBeaconRegion) -> Void {
        
        let finalCLBeacons = clBeacons.filter { (clBeacon:CLBeacon) -> Bool in
            return clBeacon.proximity != .unknown
        }
        if self.beacons != nil {
            let matchBeacons:[Beacon]? = self.beacons?.filter({ (beacon:Beacon) -> Bool in
                for clBeacon:CLBeacon in finalCLBeacons {
                    if clBeacon.proximityUUID.uuidString == beacon.ibeacon!.uuid!.uppercased() && clBeacon.major.intValue == beacon.ibeacon!.major!.intValue && clBeacon.minor.intValue == beacon.ibeacon!.minor!.intValue {
                        
                        
                        //                        switch (clBeacon.proximity) {
                        //                        case .unknown:
                        //                            print("Inside Proximity is unknown")
                        //                        case .immediate:
                        //                            print("Inside Proximity is immediate")
                        //                        case .far:
                        //                            print("Inside Proximity is far")
                        //                        case .near:
                        //                            print("Inside Proximity is near")
                        //                        }
                        //
                        
                        beacon.ibeacon?.lastProximity = NSNumber(value: clBeacon.proximity.rawValue - 1)
                        return true
                    }
                }
                return false
            })
            
            for mBeacon:Beacon in matchBeacons! {
                
                self.verifyBeacon(mBeacon, proximity: mBeacon.ibeacon!.lastProximity!)
                
                
            }
            
            
        }
    }
    
}
