//
//  CampaignActivityManager.swift
//  Din Aker Brygge
//
//  Created by Rahul on 9/28/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import UIKit

class CampaignActivityManager: NSObject {

    //Singleton object
    static let sharedInstance = CampaignActivityManager()
    
    
    //Get CampaignActivity Details by ID
    func getCampaignActivitByID(_ CampaignActivitId: NSNumber, completionQueue:DispatchQueue = DispatchQueue.main,callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void)  {
        
        let requestURL = "CampaignActivity/\(CampaignActivitId)"
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    ParsingManager.sharedInstance.parseCampaignActivities([response as! Dictionary<String,AnyObject>], completionHandler: { (arrCampaignActivites) in
                        
                        if arrCampaignActivites.count > 0 {
                        
                            callback(true, arrCampaignActivites[0])
                        }
                        else {
                            
                            callback(false, response)
                        }
                    })
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    //Reddem Offer
    func redeemOffer(_ campaignActivityID: NSNumber, Quantity: NSNumber, completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = "Offer/Redemption"
        
        let params = [
                      "CampaignActivityID" : campaignActivityID,
                      "Quantity"           : Quantity
                    ]
        
        NetworkingManager.sharedInstance.postRequest(params as AnyObject, url: requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    callback(true, response)
                    
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    //Offer Payment Status
    func updateOfferOrderPaidStatus(_ completionQueue:DispatchQueue = DispatchQueue.main,order:Dictionary<String,AnyObject>,  callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = "Offer/Redemption/Paid"
        
        NetworkingManager.sharedInstance.putRequest(order as AnyObject, url: requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {

                    callback(true, response)
                    
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    
    
    //Get Offer Receipts
    func getOfferReceipts(_ campaignActivityID:NSNumber, completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = GET_OFFER_RECEIPT + "\(campaignActivityID)"
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    
    func getOfferEmailReceipt(_ receiptId:NSNumber,completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = OFFER_EMAIL_RECEIPT + "\(receiptId)"
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
}
