//
//  ConciergeManager.swift
//  Din Aker Brygge
//
//  Created by Paresh on 13/07/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Foundation

class ConciergeManager: NSObject {
    
    //Singleton object
    static let sharedInstance = ConciergeManager()
    
    //CategoryList
    func getCategoryList(_ completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = GET_CONCIERGE_CATEGORYLIST
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    //NewRequest
    func postNewRequest(_ dictParameters:Dictionary<String,AnyObject>, completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = POST_CONCIERGE_NEWREQUEST
        
        NetworkingManager.sharedInstance.postRequest(dictParameters as AnyObject, url: requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    //postImageToServer
    func postImageToServer(_ image:UIImage, completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = POST_IMAGE_TO_SERVER
        
        NetworkingManager.sharedInstance.postImageAsMutlipartRequest(requestURL, image:image, callback: { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        })
    }
    
    //RequestList
    func getRequestList(_ completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = GET_CONCIERGE_REQUESTLIST
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    //Request/RequestID
    func getRequestDetails(_ requestID: NSNumber, completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = GET_CONCIERGE_REQUEST + "\(requestID)"
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    //DeleteRequest/RequestID
    func deleteRequest(_ requestID:String, completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = DELETE_CONCIERGE_REQUEST + requestID
        
        let dictParameter:Dictionary<String,AnyObject> = ["RequestID":requestID as AnyObject]
        
        NetworkingManager.sharedInstance.deleteRequest(dictParameter as AnyObject, url:requestURL ) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    //Conversation List
    func getConversations(_ RequestID: NSNumber, AccountID: NSNumber, PageNo: NSNumber, completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = "Concierge/Conversations/\(RequestID)/\(AccountID)/\(PageNo)"
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    //Admin RequestList
    func getAdminRequestList(_ completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = GET_ADMIN_CONCIERGE_REQUESTLIST
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
}
