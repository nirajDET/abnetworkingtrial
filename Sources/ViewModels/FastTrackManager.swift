//
//  FastTrackManager.swift
//  Din Aker Brygge
//
//  Created by Rahul Chandera on 22/06/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import UIKit
import Gloss

class FastTrackManager: NSObject {
    
    //Singleton object
    static let sharedInstance = FastTrackManager()
    
    
    
    //MARK:- FASTTRACK Admin List
    
    func createNewFastTrackProduct(_ completionQueue:DispatchQueue = DispatchQueue.main,productCategory:Dictionary<String,AnyObject>,isNew:Bool,callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        
        
        func processResponse(_ suceeded:Bool,response:AnyObject?) {
            do {
                
                let jsonData =  try JSONSerialization.data(withJSONObject: productCategory, options: JSONSerialization.WritingOptions.prettyPrinted)
                let string:String =  String(data: jsonData, encoding: String.Encoding.utf8)!
                print("\(string)")
                print("\(productCategory)")
            } catch let error as NSError {
                print(error)
            }
            
            if suceeded == true {
                
                
                
                callback(true, response)
                
            }
            else {
                
                callback(false, response)
            }
        }
        
        
        
        let requestURL:String = FASTTRCK_PRODUCT
        
        if isNew {
            NetworkingManager.sharedInstance.postRequest(productCategory as AnyObject, url: requestURL) { (succeeded, response) in
                completionQueue.async(execute: {
                    processResponse(succeeded, response: response)
                    
                })
                
            }
            
        }
        else {
            NetworkingManager.sharedInstance.putRequest(productCategory as AnyObject, url: requestURL) { (succeeded, response) in
                completionQueue.async(execute: {
                    processResponse(succeeded, response: response)
                    
                })
                
            }
            
        }
    }
    
    func createNewFastTrackProductCategory(_ completionQueue:DispatchQueue = DispatchQueue.main,productCategory:Dictionary<String,AnyObject>,isNew:Bool,callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        
        
        func processResponse(_ suceeded:Bool,response:AnyObject?) {
            do {
                
                let jsonData =  try JSONSerialization.data(withJSONObject: productCategory, options: JSONSerialization.WritingOptions.prettyPrinted)
                let string:String =  String(data: jsonData, encoding: String.Encoding.utf8)!
                print("\(string)")
                print("\(productCategory)")
            } catch let error as NSError {
                print(error)
            }
            
            if suceeded == true {
                
                
                
                callback(true, response)
                
            }
            else {
                
                callback(false, response)
            }
        }
        
        
        
        let requestURL:String = FASTTRACK_PRODUCT_CATEGORY
        ////FASTTRCK_PRODUCT
        if isNew {
            NetworkingManager.sharedInstance.postRequest(productCategory as AnyObject, url: requestURL) { (succeeded, response) in
                completionQueue.async(execute: {
                    processResponse(succeeded, response: response)
                    
                })
                
            }
            
        }
        else {
            NetworkingManager.sharedInstance.putRequest(productCategory as AnyObject, url: requestURL) { (succeeded, response) in
                completionQueue.async(execute: {
                    processResponse(succeeded, response: response)
                    
                })
                
            }
            
        }
    }
    
    func deleteFastTrackProductCategory(_ completionQueue:DispatchQueue = DispatchQueue.main,productCategory:Array<Dictionary<String,AnyObject>>,callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        
        
        let requestURL:String = FASTTRACK_PRODUCT_CATEGORY
        //FASTTRACK_DELETE_TEMP
        
        NetworkingManager.sharedInstance.deleteRequest(productCategory as AnyObject, url: requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                do {
                    
                    let jsonData =  try JSONSerialization.data(withJSONObject: productCategory, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let string:String =  String(data: jsonData, encoding: String.Encoding.utf8)!
                    print("\(string)")
                    print("\(productCategory)")
                } catch let error as NSError {
                    print(error)
                }
                
                if succeeded == true {
                    
                    
                    
                    callback(true, response)
                    
                }
                else {
                    
                    callback(false, response)
                }
                
                
            })
            
        }
        
        
    }
    
    
    
    func deleteFastTrackProduct(_ completionQueue:DispatchQueue = DispatchQueue.main,productCategory:Array<Dictionary<String,AnyObject>>,callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = FASTTRCK_PRODUCT
        
        NetworkingManager.sharedInstance.deleteRequest(productCategory as AnyObject, url: requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                do {
                    
                    let jsonData =  try JSONSerialization.data(withJSONObject: productCategory, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let string:String =  String(data: jsonData, encoding: String.Encoding.utf8)!
                    print("\(string)")
                    print("\(productCategory)")
                } catch let error as NSError {
                    print(error)
                }
                
                if succeeded == true {
                    
                    
                    
                    callback(true, response)
                    
                }
                else {
                    
                    callback(false, response)
                }
                
                
            })
            
        }
        
        
    }
    
    
    
    func getProductCategoryList(_ completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = GET_FASTTRACK_PRODUCT_CATEGORY
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    func getProductFromCategory(_ categoryID:String,completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = GET_FASTTRACK_PRODUCTLIST + "/\(categoryID)"
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    
    //MARK:-  FASTTRACK B2C
    func getFastTrackEmailReceipt(_ orderId:NSNumber,completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = GET_FASTTRACK_EMAIL_RECEIPT + "\(orderId)"
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    
    
    func getAccountList(_ completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: [AccountModel]?) -> Void) {
        
        let requestURL:String = GET_FASTTRACK_ACCOUNTS
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    ParsingManager.sharedInstance.parseAccountsFromArray(response as! Array, completion: { (result, arrItems) in
                        
                        callback(true, arrItems)
                    })
                }
                else {
                    
                    callback(false, response as? [AccountModel])
                }
            })
        }
    }
    
    
    func getLastOrder(_ completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = GET_FASTTRACK_LAST_ORDER
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    // ParsingManager.sharedInstance.parseAccountsFromArray(response as! Array, isForCoreData:false, completion: { (result, arrItems) in
                    
                    callback(true, response)
                    //})
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    
    func postFasttrackPaidOrder(_ completionQueue:DispatchQueue = DispatchQueue.main,order:Dictionary<String,AnyObject>,  callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = POST_FASTTRACK_PAID_ORDER
        
        
        NetworkingManager.sharedInstance.putRequest(order as AnyObject, url: requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                do {
                    
                    let jsonData =  try JSONSerialization.data(withJSONObject: order, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let string:String =  String(data: jsonData, encoding: String.Encoding.utf8)!
                    print("\(string)")
                    print("\(order)")
                } catch let error as NSError {
                    print(error)
                }
                
                if succeeded == true {
                    
                    
                    
                    callback(true, response)
                    
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
        
        
    }
    
    
    
    
    func postFasttrackOrder(_ completionQueue:DispatchQueue = DispatchQueue.main,order:Dictionary<String,AnyObject>,  callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = POST_FASTTRACK_ORDER
        //FASTTRCK_PRODUCT
        NetworkingManager.sharedInstance.postRequest(order as AnyObject, url: requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                do {
                    
                    let jsonData =  try JSONSerialization.data(withJSONObject: order, options: JSONSerialization.WritingOptions.prettyPrinted)
                    let string:String =  String(data: jsonData, encoding: String.Encoding.utf8)!
                    print("\(string)")
                    print("\(order)")
                } catch let error as NSError {
                    print(error)
                }
                
                if succeeded == true {
                    
                    
                    
                    callback(true, response)
                    
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
        
        
    }
    
    
    func getProductsForAccount(_ accountId:String, completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = GET_FASTTRACK_PRODUCTS + accountId
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    func getProductsByProductID(_ productId:String, completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = GET_PRODUCTS_BYPRODUCTID + productId
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    
    func getActiveOrders(_ completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = GET_ACTIVE_ORDERS
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    
    func getOldOrders(_ completionQueue:DispatchQueue = DispatchQueue.main, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = GET_OLD_ORDERS
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    // let productsCat = [OrderModel].from(jsonArray:response as! [JSON]) as! AnyObject
                    callback(true, response)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
}
