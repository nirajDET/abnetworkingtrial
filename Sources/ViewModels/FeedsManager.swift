//
//  FeedsManager.swift
//  Aker_Brygge
//
//  Created by Paresh on 13/10/16.
//  Copyright © 2016 DET. All rights reserved.
//

import Foundation

@objc class FeedsManager :NSObject {
    
    static let sharedInstance = FeedsManager()
    var lastFeedTimeStamp:String = ""
    
    //MARK:- GET FEEDS
    func getFeeds(_ index: Int, callback: @escaping (_ succeeded: Bool, _ response: Any) -> Void) {
        let requestURL:String = "Feed/\(index)"
        NetworkingManager.sharedInstance.getRequest(requestURL, callback: {(succeeded: Bool, response: Any?) in
            
            DispatchQueue.main.async {
                if succeeded == true {
                    
                    if response is Array<Dictionary<String,AnyObject>> && response != nil {
                        
                        ParsingManager.sharedInstance.parseCampaignActivities(response as! Array<Dictionary<String,AnyObject>>, completionHandler: {(arrCampaignActivites: [CampaignActivity]) in
                            callback(true, arrCampaignActivites)
                        })
                    }
                    else {
                        callback(false, response as Any)
                    }
                }
                else {
                    callback(false, response as Any)
                }
            }
        })
    }
    
    // MARK: - Get Account Feeds
    
    func getAccountFeeds(_ accountId: Int, pageIndex index: Int, callback: @escaping (_ succeeded: Bool, _ response: Any) -> Void) {
        let requestURL = "AccountFeed/\(accountId)/1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16/\(index)"
        NetworkingManager.sharedInstance.getRequest(requestURL, callback: {(succeeded: Bool, response: Any?) in
            DispatchQueue.main.async(execute: {() -> Void in
                if succeeded {
                    if response is Array<Dictionary<String,AnyObject>> && (response != nil) {
                        let sharedInstance = ParsingManager.sharedInstance
                        sharedInstance.parseCampaignActivities(response as! Array<Dictionary<String,AnyObject>>, completionHandler: {(arrCampaignActivites: [CampaignActivity]) -> Void in
                            callback(true, arrCampaignActivites)
                        })
                    }
                    else {
                        callback(false, response as Any)
                    }
                }
                else {
                    callback(false, response as Any)
                }
            })
        })
    }

    // MARK: - Get Account Instagram
    
    func getAccountInstagram(_ accountId: Int, pageIndex index: Int, callback: @escaping (_ succeeded: Bool, _ response: Any) -> Void) {
        let requestURL = "AccountFeed/\(accountId)/12/\(index)"
        NetworkingManager.sharedInstance.getRequest(requestURL, callback: {(succeeded: Bool, response: Any?) in
            DispatchQueue.main.async(execute: {() -> Void in
                if succeeded {
                    if (response is [Any]) && (response != nil) {
                        let sharedInstance = ParsingManager.sharedInstance
                        sharedInstance.parseCampaignActivities(response as! Array<Dictionary<String, AnyObject>>, completionHandler: {(arrCampaignActivites: [CampaignActivity]) -> Void in
                            callback(true, arrCampaignActivites)
                        })
                    }
                    else {
                        callback(false, response as Any)
                    }
                }
                else {
                    callback(false, response as Any)
                }
            })
        })
    }
    // MARK: - Get Article List
    
    func getArticleList(forIndex index: String,_ addionalHeaderParamDict:Dictionary<String,AnyObject>? = nil,callback: @escaping (_ succeeded: Bool, _ response: Any) -> Void) {
        let instance = UserWsManager.sharedInstance
        instance.getAllArticleFromServer(pageNumber:Int((index as NSString ).intValue), callback: {(succeeded: Bool, response: Any?) in
            if succeeded {
                let sharedInstance = ParsingManager.sharedInstance
                sharedInstance.parseCampaignActivities(response as! Array<Dictionary<String, AnyObject>>, completionHandler: {(arrCampaignActivites: [CampaignActivity]) -> Void in
                    callback(succeeded, arrCampaignActivites)
                })
            }
            else {
                callback(false, response as Any)
            }
        })
    }
    
    func getOffersForUser(withCallBack callback: @escaping (_ succeeded: Bool, _ response: Any?) -> Void) {
        let instance = UserWsManager.sharedInstance
        instance.getOffersForUser(1, callback: {(succeeded: Bool, response: Any?) in
            callback(succeeded, response)
        })
    }
    
    func getHvaSkjerForUser(withCallBack callback: @escaping (_ succeeded: Bool, _ response: Any?) -> Void) {
        let instance = UserWsManager.sharedInstance
        instance.getHvaSkjerForUser(1, callback: {(succeeded: Bool, response: Any?) in
            callback(succeeded, response)
        })
    }
}
