//
//  LanguageHandler.swift
//  Aker_Brygge
//
//  Created by Paresh on 08/12/16.
//  Copyright © 2016 DET. All rights reserved.
//

import Foundation


@objc class LanguageHandler: NSObject {
    
    static let sharedInstance = LanguageHandler()
    
    func provideLocalizedText(forKey textkey: String) -> String {
        let path:String = Bundle.main.path(forResource: "en", ofType: "lproj")!
        if let languageBundle:Bundle = Bundle(path: path) {
            let str = languageBundle.localizedString(forKey: textkey, value: "", table: nil)
            return str
        }
        else {
            return ""
        }
    }
}
