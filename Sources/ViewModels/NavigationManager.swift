//
//  NavigationManager.swift
//  Aker_Brygge
//
//  Created by Paresh on 05/12/16.
//  Copyright © 2016 DET. All rights reserved.
//

import Foundation

@objc class NavigationManager:NSObject {
    static let sharedInstance = NavigationManager()
    
    let userStoryboard = UIStoryboard(name: "User", bundle: nil)
    let homeStoryboard = UIStoryboard(name: "Home", bundle: nil)
    let mapStoryboard = UIStoryboard(name: "Map", bundle: nil)
    let feedsStoryboard = UIStoryboard(name: "Feeds", bundle: nil)
    let offersStoryboard = UIStoryboard(name: "Offers", bundle: nil)
    let hvaSkjerStoryboard = UIStoryboard(name: "HvaSkjer", bundle: nil)
    
    var tabBarControl: TabBarViewController?
    
    func getScreen(_ screenType: NavigateToScreen) -> UIViewController? {
        var loginView: LoginViewController?
        var mapView: MapViewController?
        var catSubView: AllCategoryViewController?
        var detailView: CategoryViewController?
        var interestView: InterestViewController?
        var tempFeedView: FeedsViewController?
        var termsAndCondView: TermsAndConditionViewController?
        var articleListView: ArticleListViewController?
        var articleView: ArticleViewController?
        var cityExperienceView: CityExperienceViewController?
        var historicalPlaceView: SeverdigheterViewController?
        var offersView: OffersListViewController?
        var hvaSkjerView: HvaSkjerViewController?
        var userDetailView: UserProfileViewController?
        var favouriteView: FavouriteViewController?
        var instaDetailView: InstagramViewController?
        var offerDetailView: OfferDetailsViewController?
        var calenderView: EventViewController?
        
        switch screenType.rawValue {
        case nsLoginView.rawValue:
            loginView = (userStoryboard.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController)
            return loginView!
        case nsMapView.rawValue:
            mapView = (homeStoryboard.instantiateViewController(withIdentifier: "MapViewController") as! MapViewController)
            return mapView!
            
        case nsCategorySubCategoryView.rawValue:
            catSubView = (mapStoryboard.instantiateViewController(withIdentifier: "AllCategoryViewController") as! AllCategoryViewController)
            return catSubView!
            
        case nsCategoryDetailView.rawValue:
            detailView = (mapStoryboard.instantiateViewController(withIdentifier: "CategoryViewController") as! CategoryViewController)
            return detailView!
            
        case nsInterestView.rawValue:
            interestView = (userStoryboard.instantiateViewController(withIdentifier: "InterestViewController") as! InterestViewController)
            return interestView!
            
        case nsTempFeed.rawValue:
            tempFeedView = (homeStoryboard.instantiateViewController(withIdentifier: "FeedsViewController") as! FeedsViewController)
            return tempFeedView!

        case nsTermsAndCondView.rawValue:
            termsAndCondView = (userStoryboard.instantiateViewController(withIdentifier: "TermsAndConditionView") as! TermsAndConditionViewController)
            return termsAndCondView!
            
        case nsArticleListView.rawValue:
            articleListView = (feedsStoryboard.instantiateViewController(withIdentifier: "ArticleListViewController") as! ArticleListViewController)
            return articleListView!
            
        case nsArticleView.rawValue:
            articleView = (feedsStoryboard.instantiateViewController(withIdentifier: "ArticleViewController") as! ArticleViewController)
            return articleView!
            
        case nsCityExperienceView.rawValue:
            cityExperienceView = (feedsStoryboard.instantiateViewController(withIdentifier: "CityExperienceView") as! CityExperienceViewController)
            return cityExperienceView!
            
        case nsHistoricalPlaceView.rawValue:
            historicalPlaceView = (feedsStoryboard.instantiateViewController(withIdentifier: "HistoricalPlaceView") as! SeverdigheterViewController)
            return historicalPlaceView!
            
        case nsOffersView.rawValue:
            offersView = (offersStoryboard.instantiateViewController(withIdentifier: "OffersListViewController") as! OffersListViewController)
            return offersView!
            
        case nsHvaSkjerView.rawValue:
            hvaSkjerView = (hvaSkjerStoryboard.instantiateViewController(withIdentifier: "HvaSkjerViewController") as! HvaSkjerViewController)
            return hvaSkjerView!
            
        case nsUserDetailView.rawValue:
            userDetailView = (userStoryboard.instantiateViewController(withIdentifier: "UserProfileViewController") as! UserProfileViewController)
            return userDetailView!
            
        case nsFavouriteView.rawValue:
            favouriteView = (userStoryboard.instantiateViewController(withIdentifier: "FavouriteViewController") as! FavouriteViewController)
            return favouriteView!
            
        case nsInstaDetailView.rawValue:
            instaDetailView = (feedsStoryboard.instantiateViewController(withIdentifier: "InstaDetailViewController") as! InstagramViewController)
            return instaDetailView!
            
        case nsOfferDetailView.rawValue:
            offerDetailView = (offersStoryboard.instantiateViewController(withIdentifier: "OfferDetailsViewController") as! OfferDetailsViewController)
            return offerDetailView!
            
        case nsCalenderView.rawValue:
            calenderView = (feedsStoryboard.instantiateViewController(withIdentifier: "CalenderViewController") as! EventViewController)
            return calenderView!
            
        case nsTabBarControl.rawValue:
            
            if self.tabBarControl == nil {
                self.tabBarControl = (homeStoryboard.instantiateViewController(withIdentifier: "TabBarControl") as! TabBarViewController)
            }
            return self.tabBarControl!
        default :
            return nil
            
        }
    }
    
    func setBadgeCountForTabBar() {
        let delegate:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        
        let tabBar:UITabBarController = delegate.getTabBarInstance()
        
        let conciergeTabBarItem:UITabBarItem = tabBar.tabBar.items![3];
        
         let fastTrackTabBarItem:UITabBarItem = tabBar.tabBar.items![2];
        
        if delegate.conciergeBadgeCount == 0 {
            if let count =  UserDefaults.standard.object(forKey: "ConciergeBadgeCount") as? Int32 {
                delegate.conciergeBadgeCount = count
            }
        }
        
        
        if delegate.conciergeBadgeCount > 99{
            conciergeTabBarItem.badgeValue = "99+"
        }
        else if delegate.conciergeBadgeCount > 0 {
            conciergeTabBarItem.badgeValue = String("\(delegate.conciergeBadgeCount)")
        }
        else {
            conciergeTabBarItem.badgeValue = nil
        }
        
        if delegate.fastTrackBadgeCount == 0 {
            if let count =  UserDefaults.standard.object(forKey: "FastTrackBadgeCount") as? Int32 {
                delegate.fastTrackBadgeCount = count
            }
        }
        
        
        if delegate.fastTrackBadgeCount > 99{
            fastTrackTabBarItem.badgeValue = "99+"
        }
        else if delegate.fastTrackBadgeCount > 0 {
            fastTrackTabBarItem.badgeValue = String("\(delegate.fastTrackBadgeCount)")
        }
        else {
            fastTrackTabBarItem.badgeValue = nil
        }
        
        
    }
    
    
}
