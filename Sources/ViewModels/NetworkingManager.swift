//
//  NetworkingManager.swift
//  NPro Architecture
//
//  Created by Rahul Chandera on 16/03/16.
//  Copyright © 2016 Norwegian Property ASA. All rights reserved.
//

import UIKit
import Foundation
import MobileCoreServices
import SystemConfiguration

@objc class NetworkingManager: NSObject {
    
    //Singleton object
    static let sharedInstance = NetworkingManager()
    
    let currentBuildMode:BuildMode = BUILD_MODE
    
    var API_BASE_URL:String?

    override init() {
        super.init()
        API_BASE_URL = getApiBaseUrl(buildMode: currentBuildMode)
    }
    
    func getCurrentBuildMode() -> BuildMode{
        return currentBuildMode
    }
    
    func getApiBaseUrl(buildMode: BuildMode) -> String {
     
        var apiBaseURL: String!
        switch buildMode {
            case .qa:
                print("QA")
                apiBaseURL = "http://nprob2cmobileapi-qa.azurewebsites.net/api/" //QA
            break
            case .dev:
                print("DEV")
                apiBaseURL = "http://nprob2cmobileapi.azurewebsites.net/api/" //Dev
            break
            case .production:
                print("PRODUCTION")
                apiBaseURL = "http://nprob2cmobileapi-prod.azurewebsites.net/api/" //Production
            break
            case .staging:
                print("STAGING")
                apiBaseURL = "http://nprob2cmobileapi-stg.azurewebsites.net/api/" //Staging
            break
         }
        return apiBaseURL
     }
    
    func prepareRequestToServerWith(_ requestString:String, methodType:String,params:AnyObject?) -> URLRequest  {
        
        var request = URLRequest(url: URL(string: API_BASE_URL! + requestString)!)
        request.httpMethod = methodType
        request = setHeadersToRequest(request)
        if params != nil {
            request.httpBody = try! JSONSerialization.data(withJSONObject: params!, options: [])
        }
        
        let hData = try! JSONSerialization.data(withJSONObject: request.allHTTPHeaderFields!, options: [])
        
        let hString = String(data: hData, encoding: String.Encoding.utf8)
        
        print("URL : \(request.url!.absoluteString) \nHeader : \(hString!) \n");
        
        if let data = request.httpBody {
            
            let str = String(data: data, encoding: String.Encoding.utf8)
            
            print("\nBody : \(str!) ");
        }
        
        return request
        
    }

    func createDataTastWithRequest( _ request:URLRequest,  callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        if Reachability.isConnectedToNetwork() == false
        {
            callback(false, [NEW_ERROR_MESSAGE:NO_INTERNET_MESSAGE] as AnyObject)
            return
        }
        
         let session = URLSession.shared
        
        //session.dataTas
        
        let task = session.dataTask(with: request, completionHandler: {  (data:Data?, response:URLResponse?, error:Error?) in
            //print("\(params)")
            self.validateResponse(data, response: response, error: error, callback: callback)
            return ()
        }) 
        task.resume()
    }
    
    //  MARK: Add Additional Header Params To Request
    func addAdditionalHeaderParamsToRequest(_ requestURL:URLRequest, addionalHeaderParam:Dictionary<String,AnyObject>) -> URLRequest  {
        
        var request = requestURL
        
        for (key, value) in addionalHeaderParam {
            print("Param: Value: \(value as Any) for key: \"\(key as String)\"")
            request.addValue(String(describing: value), forHTTPHeaderField: key)
        }
        return request
    }
    

    //MARK:- POST Request
    func postRequest(_ params: AnyObject, url: String, callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        if Reachability.isConnectedToNetwork() == false
        {
            callback(false, [NEW_ERROR_MESSAGE:NO_INTERNET_MESSAGE] as AnyObject)
            return
        }
        
        let request:URLRequest = prepareRequestToServerWith(url, methodType: "POST", params: params)
        self.createDataTastWithRequest(request, callback: callback)
    }
    
    //MARK:- PUT Request
    func putRequest(_ params: AnyObject, url: String, callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let request:URLRequest = prepareRequestToServerWith(url, methodType: "PUT", params: params)
        self.createDataTastWithRequest(request, callback: callback)
    }
    
    //MARK:- DELETE Request
    func deleteRequest(_ params: AnyObject, url: String, callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let request:URLRequest = prepareRequestToServerWith(url, methodType: "DELETE", params: params)
        self.createDataTastWithRequest(request, callback: callback)
    }
    
    //MARK:- GET Request
    
    func getRequest(_ url: String,_ additionalHeaderParamDict:Dictionary<String,AnyObject>? = nil, callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        if Reachability.isConnectedToNetwork() == false
        {
            callback(false, [NEW_ERROR_MESSAGE:NO_INTERNET_MESSAGE] as AnyObject)
            return
        }
        
        var request:URLRequest = prepareRequestToServerWith(url, methodType: "GET", params: nil)
        
        //  Add Additional HEader Params
        if additionalHeaderParamDict != nil {
            request = self.addAdditionalHeaderParamsToRequest(request, addionalHeaderParam: additionalHeaderParamDict!)
            
            let hData = try! JSONSerialization.data(withJSONObject: request.allHTTPHeaderFields!, options: [])
            let hString = String(data: hData, encoding: String.Encoding.utf8)

            print("FINAL URL : \(request.url!.absoluteString) \n Revised Header : \(hString!) \n");
        }

        self.createDataTastWithRequest(request, callback: callback)
    }
    
    //MARK:- MULTIPART Request
    func postImageAsMutlipartRequest(_ url: String,image:UIImage, callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
      var request:URLRequest = prepareRequestToServerWith(url, methodType: "POST", params: nil)
        request.timeoutInterval = 180
        let imageData:(Data?,String) = Utils.convertImageToDate(image)
        
        let boundary = generateBoundaryString()
        
        
        
        let fullData = photoDataToFormData(imageData.0!,boundary:boundary,fileName:imageData.1)
        
        request.setValue("multipart/form-data; boundary=" + boundary,
                         forHTTPHeaderField: "Content-Type")
        
        // REQUIRED!
        request.setValue(String(fullData.count), forHTTPHeaderField: "Content-Length")
        
        request.httpBody = fullData
        
        self.createDataTastWithRequest(request, callback: callback)
    }
    
    //MARK:- Helpers
    
    func validateResponse(_ data:Data?,response:URLResponse?,error:Error?,callback: (_ succeeded: Bool, _ response: AnyObject?) -> Void){
        //print("URL ")
        guard let data:Data = data, let response:URLResponse = response, error == nil else {
            
            print("Error = \(error)")
            let responseDict = [NEW_ERROR_MESSAGE:error?.localizedDescription]
            
            callback(false, responseDict as AnyObject?)
            return
        }
        
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            
            if let jsonDict:Dictionary<String,AnyObject> = json as? Dictionary<String,AnyObject> {
                if jsonDict.count == 0 {
                     callback(false, response)
                    return
                }
            }
            
            
            if let httpResponse = response as? HTTPURLResponse {
                
                if httpResponse.statusCode == 200 {
                    
                    callback(true, json as AnyObject?)
                    
                }
                else {
                   callback(false, json as AnyObject?)
                }
            }
            else {
                
                callback(false, response)
            }
        }
        catch let error as NSError {
            
            let httpResponse = response as? HTTPURLResponse
            print("json error: \(error.localizedDescription)\nCode:\(httpResponse!.statusCode)")
            print("\(response.url!)")
            print("\(String(data: data, encoding:String.Encoding.utf8))")
            
            let responseDict = [NEW_ERROR_MESSAGE:HTTPURLResponse.localizedString(forStatusCode: httpResponse!.statusCode)]
            
            let responseDict2 = [NEW_ERROR_MESSAGE:error.localizedDescription]
            
            if let httpResponse = response as? HTTPURLResponse {
                
                if httpResponse.statusCode == 200 {
                    
                    callback(false, responseDict2 as AnyObject?)
                    
                }
                else {
                    callback(false, responseDict as AnyObject?)
                }
            }

            
            
        }
    }
    
    
    //Set required header parameters to request
    func setHeadersToRequest( _ request: URLRequest) -> URLRequest {
        
        var request = request
        
        let appUtilities = AppUtils.sharedInstance
        
        if UserManager.iscurrentUserNil() {
            
            let _ = UserManager.sharedInstance()
        }
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue((appUtilities.appVersion()), forHTTPHeaderField: "AppVersion")
        if UserManager.sharedInstance().currentUser != nil {
            
            let userId:String = "\(UserManager.sharedInstance().currentUser.userId)"
            request.addValue(userId, forHTTPHeaderField: "UserId")
            request.addValue( "\(UserManager.sharedInstance().currentUser.getAllRoles())", forHTTPHeaderField: "Roles")
            request.addValue( "\(UserManager.sharedInstance().currentUser.getAllModules())", forHTTPHeaderField: "Modules")
        }
        else {
            request.addValue("0", forHTTPHeaderField: "Roles")
        }
        
        if UserManager.sharedInstance().currentUser?.apiKey != nil {
            request.addValue(UserManager.sharedInstance().currentUser.apiKey!, forHTTPHeaderField: "APIKey")
        }
        
        if UserManager.sharedInstance().currentUser != nil {
            
            let deviceId:String = "\(UserManager.sharedInstance().currentUser.deviceID)"
            request.addValue(deviceId, forHTTPHeaderField: "DeviceId")
        }
        else {
            request.addValue("0", forHTTPHeaderField: "DeviceId")
        }
        
        //as gurang suggested for opening hour web service
        if request.url?.absoluteString .contains("OpeningHoursStatus") == true {
            
            let timeInterval:Int64 = currentTimeMillis()
            let strTimeInterval:String = String(timeInterval)
            request.addValue(strTimeInterval, forHTTPHeaderField: "LastTimeStamp");
        }
        return request
        //print(request.allHTTPHeaderFields)
    }
    
    func currentTimeMillis() -> Int64{
        let nowDouble = Date().timeIntervalSince1970
        return Int64(nowDouble*1000)
    }
    
    //MARK: - For Multipart (Image)
    /// Create boundary string for multipart/form-data request
    ///
    /// - returns:            The boundary string that consists of "Boundary-" followed by a UUID string.
    
    func generateBoundaryString() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
    
    func photoDataToFormData(_ data:Data,boundary:String,fileName:String) -> Data {
        let fullData = NSMutableData()
        
        // 1 - Boundary should start with --
        let lineOne = "--" + boundary + "\r\n"
        fullData.append(lineOne.data(
            using: String.Encoding.utf8,
            allowLossyConversion: false)!)
        
        // 2
        let lineTwo = "Content-Disposition: form-data; name=\"image\"; filename=\"upload." + fileName + "\"\r\n"
        DLog(lineTwo)
        fullData.append(lineTwo.data(
            using: String.Encoding.utf8,
            allowLossyConversion: false)!)
        
        // 3
        let lineThree = "Content-Type: image/" + fileName + "\r\n\r\n"
        fullData.append(lineThree.data(
            using: String.Encoding.utf8,
            allowLossyConversion: false)!)
        
        // 4
        fullData.append(data)
        
        // 5
        let lineFive = "\r\n"
        fullData.append(lineFive.data(
            using: String.Encoding.utf8,
            allowLossyConversion: false)!)
        
        // 6 - The end. Notice -- at the start and at the end
        let lineSix = "--" + boundary + "--\r\n"
        fullData.append(lineSix.data(
            using: String.Encoding.utf8,
            allowLossyConversion: false)!)
        
        return fullData as Data
    }
}


open class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }}


extension NSMutableData {
    
    func appendString(_ string: String) {
        
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}
