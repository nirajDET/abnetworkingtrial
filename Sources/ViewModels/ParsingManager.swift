//
//  ParsingManager.swift
//  BeaconManager
//
//  Created by Mihir Mehta on 26/04/16.
//  Copyright © 2016 Mihir Mehta. All rights reserved.
//

import Foundation
import Gloss
import CoreData
import CoreLocation

func DLog(_ message: String, function: String = #function) {
    #if DEBUG
        print("\(function): \(message)")
    #endif
}

enum ParsingResult {
    case success
    case error(error:NSError)
}


func ==(lhs: ParsingResult, rhs: ParsingResult) -> Bool {
    
    switch(lhs,rhs) {
        
    case (.success, .success): return true
    case let (.error(la), .error(ra)): return la == ra
    default: return false
    }
    
}



@objc open class ParsingManager : NSObject{
    
    static let sharedInstance = ParsingManager()
    
    
    
    //MARK:- User
    func parseUserObject(_ dictionary:Dictionary<String,AnyObject>) -> UserEntity {
        
        let userModel = UserEntity(json: dictionary)
        return userModel!
    }
    
    func parseInterestDictionary(_ dictionary:Dictionary<String,AnyObject>) -> InterestEntity {
        
        let interestModel = InterestEntity(json: dictionary)
        return interestModel!
    }
    
    
    
    //MARK: Categories
    func parseCategories(_ categoryArray:Array<Dictionary<String,AnyObject>>,completionHandler:@escaping (_ arrCategories:[CategoryModel]) -> Void)
    {
        self.parseCategoriesFromArray(categoryArray) { (result, categories) in
            
            if result == .success {
                
                let arrSortedCategories : [CategoryModel] = categories.sorted(by: {($0 ).categoryID!.intValue < ($1 ).categoryID!.intValue})
                completionHandler(arrSortedCategories)

            }
            else {
                
                completionHandler([])
                //if result == .Error {
                DLog("Error while parsing categories")
                //}
            }
        }
    }
    
    func parseCategoriesFromArray(_ data:Array<Dictionary<String,AnyObject>>,completionQueue:DispatchQueue = DispatchQueue.main,completion:@escaping (_ result:ParsingResult,_ arrcategories:[CategoryModel])->Void  ) {
        
        var categories:[CategoryModel] = []
        
        for dict:Dictionary<String,AnyObject> in data {
            
            self.parseCategoryFromDictionary(dict, categories: &categories)
        }
        
       
        completionQueue.async {
            completion(.success,categories)
        }
    }
    
    func parseCategoryFromDictionary(_ dictionary:Dictionary<String,AnyObject>,categories:inout [CategoryModel]) {
        
        let categoryModel:CategoryModel? = CategoryModel(json: dictionary)
        
        if categoryModel != nil {
            
            var arrSubCategories:[SubCategoryModel] = []
            if categoryModel!.name == CATEGORY_BYDELSTRIPS {
                
                let subCategoryModel:SubCategoryModel = SubCategoryModel()
                subCategoryModel.categoryID = 0
                subCategoryModel.name = CATEGORY_OPPLEVELSER
                arrSubCategories.append(subCategoryModel)
            }
            
            categoryModel!.subCategories = [SubCategoryModel].from(jsonArray:("Categories" <~~ dictionary)!)
            
            categories.append(categoryModel!)
        }
    }
    
    //MARK: Accounts
    
    func parseAccounts(_ storingArray:Array<Dictionary<String,AnyObject>>, completionHandler:@escaping (_ arrAcounts:[AccountModel]) -> Void)
    {
        self.parseAccountsFromArray(storingArray ) { (result, arrItems) in
            
            if result == .success {
                completionHandler(arrItems)
            }
            else {
                
                completionHandler([])
                //if result == .Error {
                DLog("Error while parsing categories")
                //}
            }
        }
    }
    
    func parseAccountsFromArray(_ data:Array<Dictionary<String,AnyObject>>,completionQueue:DispatchQueue = DispatchQueue.main,completion:@escaping (_ result:ParsingResult,_ arrItems:[AccountModel])->Void  ) {
        
       
        var allItems:[AccountModel] = []
        
        for dict:Dictionary<String,AnyObject> in data {

            self.parseAccountWithAllDetailsFromDictionary(dict, storingArray: &allItems)
        }
        
        completionQueue.async {

            completion(.success,allItems)
            
        }
    }
    
    func parseAccountWithAllDetailsFromDictionary(_ dictionary:Dictionary<String,AnyObject>, storingArray:inout [AccountModel]) {
        
        let itemModel:AccountModel! = AccountModel(json:dictionary)
        storingArray.append(itemModel)
    }
    
    func parseAccountFromDictionary(_ dictionary:Dictionary<String,AnyObject>, isForCoreData:Bool, storingArray:inout Array<AccountModel>) {
        
        let itemModel:AccountModel! = AccountModel(json:dictionary)
        storingArray.append(itemModel)
    }
    

    
    //As account detail has so much parameters so this function has been added
    func updateAndParseAccountAllDetailsFromDictionary(_ dictionary:Dictionary<String,AnyObject>,strAccountID:NSNumber) -> AccountModel? {
        
        let itemModel:AccountModel! = AccountModel(json:dictionary)
        return itemModel
    }
    
    
    //MARK:- CampignActivity
    
    func parseCampaignActivities(_ storingArray:Array<Dictionary<String,AnyObject>>, completionHandler:@escaping (_ arrCampaignActivites:Array<CampaignActivity>) -> Void) {
        
        
        self.parseCampaignActivityFromArray(storingArray) { (result, campaignActivites) in
            
            if result == .success {
                
                completionHandler(campaignActivites)
            }
            else {
                
                completionHandler([])
                //if result == .Error {
                DLog("Error while parsing CampaignActivites")
                //}
            }
        }
    }
    
    
    func parseCampaignActivityFromArray(_ campaignActs:Array<Dictionary<String,AnyObject>>, completionQueue:DispatchQueue = DispatchQueue.main,completion:@escaping (_ result:ParsingResult,_ campaignActivites:Array<CampaignActivity>) -> Void  ) {
        
        
        var campaignActivities:Array<CampaignActivity> = []
        
        for dict:Dictionary<String,AnyObject> in campaignActs {
           self.parseCampaignActivityFromDictionary(dict, campaignActivities: &campaignActivities)

        }
        
        completionQueue.async {
            completion(.success, campaignActivities)
        }
    }
    
    func parseCampaignActivityFromDictionary(_ dictionary:Dictionary<String,AnyObject>, campaignActivities:inout Array<CampaignActivity>) {
        
        let campaignActivity:CampaignActivity! = CampaignActivity(json: dictionary)
        campaignActivities.append(campaignActivity)
    }
    
    
    
    
    //MARK:- Beacon Parsing
    
    func parseBeaconsFromArray(_ beacons:Array<Dictionary<String,AnyObject>>,queue:DispatchQueue = DispatchQueue.main,completion:(_ result:ParsingResult,_ beaconList:Array<Beacon>)->Void  ) {
        
        var beaconArray:Array<Beacon> = []
        for dict:Dictionary<String,AnyObject> in beacons {
            let beacon:Beacon = Beacon(json: dict)!
            beaconArray.append(beacon)
           
        }

        print("Beacon list completion handler called")
        completion(.success,beaconArray)
       
    }

}

