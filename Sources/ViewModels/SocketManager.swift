//
//  SocketManager.swift
//  Din Aker Brygge
//
//  Created by Rahul on 7/6/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import UIKit
import PubNub
import Gloss


var PUBNUB_KEY: String?
var PUBNUB_SUB_KEY: String?

//DEV
//let PUBNUB_KEY = "pub-c-59813a57-f6a2-455c-888c-aed1c825bf6a"
//let PUBNUB_SUB_KEY = "sub-c-39fdef58-0534-11e6-a8fd-02ee2ddab7fe"

//QA (same for Staging)
//let PUBNUB_KEY = "pub-c-7578fbff-c2ee-4d1a-9dc7-b72725870a82"
//let PUBNUB_SUB_KEY = "sub-c-f167c8bc-2c85-11e6-be83-0619f8945a4f"

//PRODUCTION
//let PUBNUB_KEY = "pub-c-2e2b2c6b-99f4-42eb-ae24-c237e9d75b76"
//let PUBNUB_SUB_KEY = "sub-c-8282df7c-5ef8-11e6-bf96-0619f8945a4f"


enum OrderStatus:Int {
    
    case unpaidDraft
    case paid
    case inProcess
    case ready
    case delivered
    case cancelByUser
    case cancelByAccount
}

class SocketManager: NSObject, PNObjectEventListener {

    var client : PubNub
    var config : PNConfiguration
    
    //Singleton object
    struct Static
    {
        fileprivate static var instance: SocketManager?
    }
    class var sharedInstance: SocketManager
    {
        if Static.instance == nil
        {
            Static.instance = SocketManager()
        }
        
        return Static.instance!
    }
    
    override init() {
        
        //  Set PUBNUB_KEY & PUBNUB_SUB_KEY
        switch BUILD_MODE {
            case BuildMode.qa:       
                PUBNUB_KEY = PUBNUB_KEY_QA
                PUBNUB_SUB_KEY = PUBNUB_SUB_KEY_QA
                break
            case BuildMode.staging:
                PUBNUB_KEY = PUBNUB_KEY_STAGING
                PUBNUB_SUB_KEY = PUBNUB_SUB_KEY_STAGING
                break
            case BuildMode.dev:
                PUBNUB_KEY = PUBNUB_KEY_DEV
                PUBNUB_SUB_KEY = PUBNUB_SUB_KEY_DEV
                break
            case BuildMode.production:
                PUBNUB_KEY = PUBNUB_KEY_PRODUCTION
                PUBNUB_SUB_KEY = PUBNUB_SUB_KEY_PRODUCTION
                break
        }
        
        // Instantiate configuration instance.
        config = PNConfiguration(publishKey: PUBNUB_KEY!, subscribeKey: PUBNUB_SUB_KEY!)
        
        // Set userId as UUID into config
        let user = UserManager.sharedInstance().currentUser
        if user != nil {
            
            config.uuid = "\(user!.userId)"
            print("PubNub UUID = \(config.uuid)")
        }
        
        // Presence Settings
        self.config.presenceHeartbeatValue = 10 // Tell the server that the hearbeat timeout is 10 seconds
        self.config.presenceHeartbeatInterval = 10; // Send the heartbeat to the server every 10 seconds
        
        // Time Token Handling Settings
        self.config.keepTimeTokenOnListChange = false // When changing channels, 'catchup' ?
        //self.config.restoreSubscription = true // If you lose the connection, should you resubscribe when it comes back?
        self.config.catchUpOnSubscriptionRestore = false // If restoreSubscription == YES, catchup ? Or start at 'now' ?
        
        // Instantiate PubNub client.
        client = PubNub.clientWithConfiguration(config)
        client.logger.disableLogLevel(1)
        super.init()
        
        // Delegate
        client.addListener(self)
    }
    
    
    //MARK:- Channel Subscription
    func subscribeToChannel(_ channelName: String!) {
    
        print("PubNub Channel Subscribed = \(channelName)")
        self.client.subscribeToChannels([channelName!], withPresence: true)
    }
    
    func unSubscribeFromChannel(_ channelName: String!) {
        
        print("PubNub Channel Unsubscribed = \(channelName)")
        self.client.unsubscribeFromChannels([channelName!], withPresence: true)
    }
    
    func unSubscribeFromAllChanels() {
        
        print("PubNub Unsubscribed From All")
        self.client.unsubscribeFromAll()
    }
    
    
    //MARK:- Message Handling
    func sendMessageTo(_ channelName: String!, Message: AnyObject) {
        
        self.client.publish(Message, toChannel: channelName!, compressed: true, withCompletion: nil)
    }
    
    func client(_ client: PubNub, didReceiveMessage message: PNMessageResult) {
        
        let info =  message.data.message as? [String:AnyObject]
        if info != nil {
            
            print("PubNub Message = \(info)")
            
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.handlePubNubMessage(info)
        }
    }
    
    
    //Release All Instance of PubNub
    func resetPubNub() {
      
        self.unSubscribeFromAllChanels()
        SocketManager.Static.instance = nil
    }
}
