//
//  UserManager.h
//  TestTawasolManager
//
//  Created on 11/7/14.
//  Copyright (c) 2014 Dynamic Elements Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "AppConstants.h"

@class UserEntity;

typedef NS_ENUM(NSInteger, AkerBryggeRelation) {
    
    Undefined = -1,
    JobberHer,
    BorHer,
    ErHerOfte,
    KunPaBesok
} ;

@interface UserManager: NSObject

+ (UserManager*)sharedInstance;
+ (BOOL) iscurrentUserNil;
@property (strong, nonatomic) NSMutableArray* arInterest;
@property (nonatomic,strong) UserEntity *currentUser;
@property (nonatomic,readwrite) BOOL isUserLoggedIn;


- (void)signInUserWithEmail:(NSString*)Email relation:(AkerBryggeRelation) relation Callback:(void (^)(bool succeeded, id response))callback;
- (void)signUpUserWithInfo:(UserEntity *)userEntity Callback:(void (^)(bool succeeded, id response))callback;
- (void)updateUserWithInfo:(UserEntity *)userEntity Callback:(void (^)(bool succeeded, id response))callback;
- (void)getAllInterest:(void (^)(bool succeeded, id response))callback;
- (NSString*)getUserNameWithTracking : (NSString*)name;
- (NSString*)setUserName: (NSString*)name;
- (void)startListningAdminChannels;

@end
