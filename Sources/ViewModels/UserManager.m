//
//  UserManager.m
//  TestTawasolManager
//
//  Created on 11/7/14.
//  Copyright (c) 2014 Dynamic Elements Pvt. Ltd. All rights reserved.
//

#import "UserManager.h"


@implementation UserManager

//Singleton object
static UserManager *userManager = nil;
+ (UserManager*)sharedInstance {
    
    if (!userManager) {
        static dispatch_once_t onceToken;
        
        onceToken = 0;
        
        dispatch_once(&onceToken, ^{
            userManager = [[self alloc] init];
            [self initialize];
        });
    }
    return userManager;
}
- (void)initilize
{
    self.currentUser = [[UserEntity alloc] init];
    self.currentUser = (UserEntity *)[[ApplicationManager sharedInstance] getUserFromLocal];
}
+ (BOOL) iscurrentUserNil {
    if (userManager == nil) {
        return true;
    }
    else {
        return false;
    }
}

#pragma mark - Login User
- (void)signInUserWithEmail:(NSString*)Email relation:(AkerBryggeRelation)relation Callback:(void (^)(bool succeeded, id response))callback {
    
    
    
    NSDictionary *paramDict = @{
                                @"EmailID":                 Email,
                                @"Password":                @"",
                                @"Device":                  [self getDeviceInfo]
                                };
    
    [[NetworkingManager sharedInstance] postRequest:paramDict url:@"signIn" callback:^(BOOL succeeded, id _Nullable response) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (succeeded)
            {
                self.isUserLoggedIn = TRUE;
                if (response && [response isKindOfClass:[NSDictionary class]]  ) {
                    
                    //Parse user object
                    self.currentUser = [[ParsingManager sharedInstance] parseUserObject:response];
                    self.currentUser.roles = [response objectForKey:@"Roles"];
                    self.currentUser.modules = [response objectForKey:@"Modules"];
                    
                    NSNumber *serverDOB = [response objectForKey:@"Dob"];
                    if (serverDOB != nil) {
                        
                        if (serverDOB.integerValue != 0) {
                            
                            self.currentUser.dob = [self convertAgeInYears:serverDOB];
                        }
                        else {
                            
                            self.currentUser.dob = [NSNumber numberWithInt:0];
                        }
                    }
                    else {
                        
                        self.currentUser.dob = [NSNumber numberWithInt:0];
                    }
                    
                    if (self.currentUser.relationWithAkerBrygge == Undefined) {
                        
                        self.currentUser.relationWithAkerBrygge = relation;
                        [self updateUserWithInfo:self.currentUser Callback:^(bool succeeded, id response) {}];
                    }
                    
                    //Store UserData To Local
                    [[ApplicationManager sharedInstance] storeUserToLocal:self.currentUser];
                    [[NSUserDefaults standardUserDefaults] setObject:self.currentUser.toJSON forKey:@"CurrentUser"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    //Pubnub channels
                    [self startListningAdminChannels];
                    
                    
                    
                    
                    //[self pleaseWait];
                    
                    if (self.currentUser.accountId != nil) {
                        
                        
                        [[AdminOfferManager sharedInstance] getBeaconList:self.currentUser.accountId completionQueue:(OS_dispatch_queue * _Nonnull)dispatch_get_main_queue() callback:^(BOOL succeeded, id  _Nullable response) {
                            //[self clearAllNotice];
                            
                            if (succeeded && response != nil) {
                                //= [@[AdminAccountBeacon] fromJ
                                NSMutableArray *arr = [NSMutableArray array];
                                
                                
                                for (NSDictionary *dictionary in response) {
                                    [arr addObject:[[AdminAccountBeacon alloc] initWithJson:dictionary]];
                                }
                                [AdminOfferManager sharedInstance].beaconsArray = arr;
                            }
                            
                            
                            
                            //                        if UserManager.sharedInstance().arInterest == nil {
                            //
                            //                            UserManager.sharedInstance().arInterest = []
                            //
                            //                            UserManager.sharedInstance().getAllInterest({(succeeded, response) -> Void in
                            //
                            //                                self.setupInterest()
                            //                            })
                            //                        }
                            
                            if([UserManager sharedInstance].arInterest == nil) {
                                [UserManager sharedInstance].arInterest = [NSMutableArray array];
                                
                                [[UserManager sharedInstance] getAllInterest:^(bool succeeded, id response) {
                                    callback(YES, response);
                                }];
                                
                                
                            }
                            else {
                                callback(YES, response);
                            }
                            
                            
                        }];
                    }
                    else {
                        callback(YES, response);
                    }
                    
                }
                else
                    callback(NO, response);
            }
            else
                callback(NO, response);
        });
    }];
    
}


#pragma mark - Pubnub Channels
- (void)startListningAdminChannels {
    
    //PubNub
    [SocketManager sharedInstance];
    
    if ([self.currentUser isModuleExist:1]) { // ORDER_MODULE
        [[SocketManager sharedInstance] subscribeToChannel:[NSString stringWithFormat:@"fasttrack_%@",self.currentUser.accountId]];
    }
    if ([self.currentUser isModuleExist:2]) { //CONCIERGE_MODULE
        [[SocketManager sharedInstance] subscribeToChannel:[NSString stringWithFormat:@"concierge_%@",self.currentUser.accountId]];
    }
    if ([self.currentUser isModuleExist:10]) { //REDEMPTION_MODULE
        [[SocketManager sharedInstance] subscribeToChannel:[NSString stringWithFormat:@"campaignactivity_%@",self.currentUser.accountId]];
    }
    
    if (self.currentUser.roles != nil && [[AdminUserManager sharedInstance] checkIfUserIsSuperAdmin]) {
        
        //Subscribed to channels for receiving PubNub messages, for Admin
        [[SocketManager sharedInstance] subscribeToChannel:@"campaignactivity_review"];
    }
}


#pragma mark - SignUp User
- (void)signUpUserWithInfo:(UserEntity *)userEntity Callback:(void (^)(bool succeeded, id response))callback {
    
    NSMutableArray *mutArrInterests = [NSMutableArray array];
    
    for(InterestEntity *interestEntity in userEntity.interests)
    {
        NSDictionary *dictDetails = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",interestEntity.interestID],@"InterestID",interestEntity.name,@"Name",@"true",@"isSelected",nil];
        [mutArrInterests addObject:dictDetails];
    }
    
    NSDictionary *paramDict = [self prepareDictionaryFromUserEntity:userEntity];
    
    [[NetworkingManager sharedInstance] postRequest:paramDict url:@"SignUp" callback:^(BOOL succeeded, id _Nullable response) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (succeeded)
            {
                
                if ([response isKindOfClass:[NSDictionary class]] && response) {
                    
                    userEntity.userId = [[response objectForKey:@"UserID"] intValue];
                    userEntity.deviceID = [[response objectForKey:@"DeviceID"] intValue];
                    userEntity.apiKey = [response objectForKey:@"ApiKey"];
                    self.currentUser = userEntity;
                    
                    [self signInUserWithEmail:userEntity.email relation:userEntity.relationWithAkerBrygge Callback:^(bool succeeded, id response) {
                        
                        callback(succeeded, response);
                        
                    }];
                }
                else
                    callback(NO, response);
            }
            else {
                
                if ([response isKindOfClass:[NSDictionary class]]) {
                    callback(NO, response);
                }
                else {
                    
                    NSLog(@"%@, %@",paramDict,response);
                    callback(NO, @{@"Message":@"Ukjent feil"});
                }
                
            }
        });
    }];
    
    
}
- (void) updateCurrentUserRelation:(AkerBryggeRelation) relation {
    
    if (self.currentUser != nil) {
        
        self.currentUser.relationWithAkerBrygge = relation;
    }
}


#pragma mark - Update User
- (void)updateUserWithInfo:(UserEntity *)userEntity Callback:(void (^)(bool succeeded, id response))callback {
    
    
    if(userEntity.address == nil)
        userEntity.address = @"";
    if(userEntity.dob == nil)
        userEntity.dob = [NSNumber numberWithInt:0];
    if(userEntity.country == nil)
        userEntity.country = @"";
    if(userEntity.city == nil)
        userEntity.city = @"";
    
    NSDictionary *paramDict = [self prepareDictionaryFromUserEntity:userEntity];
    
    
    [[NetworkingManager sharedInstance] putRequest:[paramDict mutableCopy] url:@"User" callback:^(BOOL succeeded, id _Nullable response) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (succeeded)
            {
                if ([response isKindOfClass:[NSDictionary class]] && response) {
                    
                    //Parse user object
                    self.currentUser = [[ParsingManager sharedInstance] parseUserObject:response];
                    self.currentUser.roles = [response objectForKey:@"Roles"];
                    self.currentUser.modules = [response objectForKey:@"Modules"];
                    
                    NSNumber *serverDOB = [response objectForKey:@"Dob"];
                    if (serverDOB != nil) {
                        
                        if (serverDOB.integerValue != 0) {
                            
                            self.currentUser.dob = [self convertAgeInYears:serverDOB];
                        }
                        else {
                            
                            self.currentUser.dob = [NSNumber numberWithInt:0];
                        }
                    }
                    else {
                        
                        self.currentUser.dob = [NSNumber numberWithInt:0];
                    }
                    
                    //Store UserData To NSUSERDEFAULTS
                    [[ApplicationManager sharedInstance] storeUserToLocal:self.currentUser];
                    
                    callback(YES, response);
                }
                else
                    callback(NO, response);
            }
            else
                callback(NO, response);
        });
    }];
    
}
- (NSDictionary *) prepareDictionaryFromUserEntity:(UserEntity *)userEntity  {
    
    NSNumber *numDOB = [NSNumber numberWithDouble:0];
    if (userEntity.dob.integerValue != 0) { //blank
        numDOB = [self convertAgeInMiles:userEntity.dob];
    }
    
    return @{
             @"FirstName":               userEntity.firstName ? userEntity.firstName : @"",
             @"LastName":                userEntity.lastName ? userEntity.lastName : @"",
             @"AvatarUrl":               @"",
             @"MobilePhone":             userEntity.contactNumber ? userEntity.contactNumber : @"",
             @"EmailID":                 userEntity.email ? userEntity.email : @"",
             @"Password":                @"",
             @"Address":                 userEntity.address ? userEntity.address : @"",
             @"PostalCode":              userEntity.postalCode ? userEntity.postalCode : @"",
             @"Gender":                  [NSString stringWithFormat:@"%d",userEntity.gender],
             @"Dob":                     numDOB,
             @"IsNewsletterSubscribed":  [NSNumber numberWithBool:userEntity.doSubscribeNewsletter],
             @"Country":                 userEntity.country ? userEntity.country : @"",
             @"City":                    userEntity.city ? userEntity.city : @"",
             @"InterestList":            [self dictionaryWithPropertiesOfObjects:userEntity.interests],
             @"Device":                  [self getDeviceInfo],
             @"Status":                  [NSNumber numberWithBool:TRUE],
             @"RelationWithAB":          [NSNumber numberWithInteger:userEntity.relationWithAkerBrygge]
             };
}


#pragma mark - Get All Interest
- (void)getAllInterest:(void (^)(bool succeeded, id response))callback {
    
    [[NetworkingManager sharedInstance] getRequest:@"InterestList" :nil callback:^(BOOL succeeded, id _Nullable response) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (succeeded)
            {
                if ([response isKindOfClass:[NSArray class]] && response) {
                    
                    [self parseAllInterest:response];
                    callback(YES, response);
                }
                else
                    callback(NO, response);
            }
            else
                callback(NO, response);
        });
    }];
}

-(void)parseAllInterest:(NSArray*)interestArray
{
    NSError* err = nil;
    
    self.arInterest = [[NSMutableArray alloc]init];
    InterestEntity *interest_ent;
    
    for (NSDictionary *dict in interestArray) {
        
        interest_ent = [[InterestEntity alloc] initWithJson:dict];
        if (!err)
            [self.arInterest addObject:interest_ent];
    }
}


#pragma mark - Device Info
- (NSDictionary*)getDeviceInfo
{
    //Current device info
    UIDevice *currentDevice = [UIDevice currentDevice];
    
    //Device token for push notfiction
    NSString *deviceToken = [[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULT_DEVICE_TOKEN];
    
    if (deviceToken == nil || deviceToken.length == 0) {
        DLog(@"Got it");
    }
    
    
    if(deviceToken == nil)
        deviceToken = @"";
    
    //Check Bluetooth status(ON/OFF)
    NSString *BlueToothState = [[ApplicationManager sharedInstance] strBluetoothState];
    
    NSDictionary *deviceDict = @{
                                 @"Name":                 currentDevice.name,
                                 @"NetworkType":           @"",
                                 @"Model":                [AppUtils deviceModel],
                                 @"OS":                   currentDevice.systemName,
                                 @"OSVersion":            currentDevice.systemVersion,
                                 @"AppVersion":           APPLICATION_VERSION,
                                 @"IsBlueToothEnabled":   BlueToothState,
                                 @"Token":                deviceToken
                                 };
    return deviceDict;
}


-(NSString*)getUserNameWithTracking:(NSString*)name {
    
    NSMutableArray *arName = [NSMutableArray arrayWithCapacity:[name length]];
    for (int i = 0; i < [name length]; i++) {
        [arName addObject:[NSString stringWithFormat:@"%C", [name characterAtIndex:i]]];
    }
    NSString *final_string = [arName componentsJoinedByString:@" "];
    
    return final_string;
}


-(NSString*)setUserName:(NSString*)name
{
    if(name != nil && name.length > 0 ) {
        
        NSString *strTemp = [name substringFromIndex: [name length] - 1];
        NSString *finalStr;
        
        if ([strTemp caseInsensitiveCompare:@"s"] == NSOrderedSame)
            finalStr = [[NSString stringWithFormat:@"%@'",name] uppercaseString];
        else
            finalStr =  [[NSString stringWithFormat:@"%@s",name] uppercaseString];
        
        finalStr = [[UserManager sharedInstance] getUserNameWithTracking:finalStr];
        return finalStr;
    }
    else
        return @"";
}


- (NSMutableArray *)dictionaryWithPropertiesOfObjects:(NSArray*)array
{
    NSMutableArray *dictionaryObjects = [NSMutableArray new];
    
    for (id obj in array) {
        
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        
        unsigned count;
        objc_property_t *properties = class_copyPropertyList([obj class], &count);
        
        for (int i = 0; i < count; i++) {
            NSString *key = [NSString stringWithUTF8String:property_getName(properties[i])];
            [dict setObject:[obj valueForKey:key] forKey:key];
        }
        
        free(properties);
        
        [dictionaryObjects addObject:[NSDictionary dictionaryWithDictionary:dict]];
    }
    return dictionaryObjects;
}

#pragma mark - Convert Methods

-(NSNumber*)convertAgeInMiles:(NSNumber*)currentAge {
    
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitHour | NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    [components setYear:([components year] - currentAge.integerValue)];
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier: NSCalendarIdentifierGregorian];
    NSDate *birthDate = [calendar dateFromComponents:components];
    
    double ageInMiles = birthDate.timeIntervalSince1970 * 1000.0;
    return [NSNumber numberWithDouble:ageInMiles];
}

-(NSNumber*)convertAgeInYears:(NSNumber*)currentAgeInMiliSeconds {
    
    double currentTimeMillis = [NSDate date].timeIntervalSince1970*1000.0;
    NSTimeInterval interval =   1000.0 * 365 * 60.0 *60.0 * 24.0;
    
    int age = ((currentTimeMillis - currentAgeInMiliSeconds.doubleValue) / interval);
    return [NSNumber numberWithInt:age];
}



@end
