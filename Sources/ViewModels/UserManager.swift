//
//  UserManager.swift
//  Din Aker Brygge
//
//  Created by Mihir Mehta on 18/05/16.
//  Copyright © 2016 Dynamic Elements. All rights reserved.
//

import Foundation
import Gloss

@objc class UserWsManager:NSObject {
    
    static let sharedInstance = UserWsManager()
    
    override init() {
        
        super.init()
    }
    
    //getRequest(url: String, callback: (succeeded: Bool, response: AnyObject?) -> Void
    func getBeaconCampaignActivities(_ completionQueue:DispatchQueue = DispatchQueue.main,callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void)  {
        let requestURL:String = GET_NOTIFICATION_CONTENT
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    //Store Beacon content into local, to use it when app is in background
                    
                    
//                    let paths:[String] = NSSearchPathForDirectoriesInDomains(.DocumentDirectory, .UserDomainMask, true)
//                    let stringsPlistPath:String = paths[0].stringByAppendingString("BeaconContents.plist")
//                    let fileManager = NSFileManager.defaultManager()
//                    if (!(fileManager.fileExistsAtPath(stringsPlistPath)))
//                    {
//                        let bundle : String? = NSBundle.mainBundle().pathForResource("BeaconContents", ofType: "plist")
//                    
//                        if bundle != nil {
//                            
//                            do {
//                                try NSFileManager.defaultManager().copyItemAtPath(bundle!, toPath: stringsPlistPath)
//                            }catch{
//                                print("Error occurred while copying file to document \(error)")
//                            }
//                        }
//                    }
//                    let arr:Array<Dictionary<String,AnyObject>>? = response as? Array<Dictionary<String,AnyObject>>
//                    
//                    if arr != nil {
//                        
//                        let dict:NSMutableDictionary = ["BeaconContent":arr!]
//                        dict.writeToFile(stringsPlistPath, atomically: false)
//                    }
                    
                   // let arr = BeaconManager.
                    
                    UserDefaults.standard.set(response, forKey: "BeaconContent")
                    
                    ParsingManager.sharedInstance.parseCampaignActivities(response as! Array<Dictionary<String,AnyObject>>, completionHandler: { (arrCampaignActivites) in
                        print("activity.beaconRulesList")
                        for activity:CampaignActivity in arrCampaignActivites {
                            
                            for beaconRule:BeaconRules in activity.beaconRulesList! {
                                
                                print("\(activity.campaignActivityID!) \(beaconRule.beaconID) \(beaconRule.proximity)")
                            }
                        }
                        
                        
                        callback(true,arrCampaignActivites as AnyObject?)
                    })
                }
                else {
                    
                    callback(false,response)
                }
            })
        }
    }
    
    func getBeaconList(_ completionQueue:DispatchQueue = DispatchQueue.main,callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void)  {
        let requestURL:String = GET_ALL_BEACONS
        
        NetworkingManager.sharedInstance.getRequest(requestURL) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    UserDefaults.standard.set(response, forKey: "BeaconList")

                    ParsingManager.sharedInstance.parseBeaconsFromArray(response as! Array<Dictionary<String,AnyObject>>, completion: { (result, arrItems) in
                        
                        for beacon:Beacon in arrItems {
                            print("BeaconID: \(beacon.beaconID), \(beacon.ibeacon!.description)")
                        }
                        
                        callback(true,arrItems as AnyObject?)
                    })
                }
                else {
                    
                    callback(false,response)
                }
            })
        }
    }
    
    
    func getUserFavouritesFromServer(_ completionQueue:DispatchQueue = DispatchQueue.main,callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void)  {
        //let requestURL:URL = URL(string: "FavoriteContentList")!
        
        NetworkingManager.sharedInstance.getRequest("FavoriteContentList") { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded {
                    
                    ParsingManager.sharedInstance.parseCampaignActivities(response as! Array<Dictionary<String,AnyObject>>, completionHandler: { (arrCampaignActivites) in
                        
                        for activity:CampaignActivity in arrCampaignActivites {
                            
                            let activityType = activity.campaignActivityType as! Int
                            
                            print("\(activity.title) || \(activity.accountID) || \(activityType)")
                            if activityType == CampaignActivityType.account.rawValue {
                                ApplicationManager.sharedInstance.updateAccountFavourite(activity.accountID!.intValue, favourtieFlag: true)
                            }
                            else if activityType == CampaignActivityType.cityExperience.rawValue {
                                ApplicationManager.sharedInstance.updateCityExperienceFavourite(activity.campaignActivityID!.intValue, favourtieFlag: true)
                            }
                        }
                        
                        callback(true,arrCampaignActivites as AnyObject?)
                    })
                }
                else {
                    
                    callback(false,response)
                }
            })
        }
    }
    
    func getOffersForUser(_ pageIndex:Int,_ addionalHeaderParamDict:Dictionary<String,AnyObject>? = nil, callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void)  {
        
        let baseURLString = String("CampaignActivityList/1,2,3,4,5,6/0/\(pageIndex)")
        
       // let requestURL:URL = URL(string: baseURLString!)!
        
        NetworkingManager.sharedInstance.getRequest(baseURLString!, addionalHeaderParamDict) { (succeeded, response) in
            
            if succeeded == true {
                
                ParsingManager.sharedInstance.parseCampaignActivities(response as! Array<Dictionary<String, AnyObject>>, completionHandler: { (arrCampaignActivites) in
                    
                    callback(true,arrCampaignActivites as AnyObject?)
                })
            }
            else {
                
                callback(false,response)
            }
        }
    }
    
    func getHvaSkjerForUser(_ pageIndex:Int,_ addionalHeaderParamDict:Dictionary<String,AnyObject>? = nil, callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void)  {
        
        let baseURLString = String("CampaignActivityList/Events/0/\(pageIndex)")
        
        // let requestURL:URL = URL(string: baseURLString!)!
        //  PageSize
        NetworkingManager.sharedInstance.getRequest(baseURLString!) { (succeeded, response) in
            
            if succeeded == true {
                
                ParsingManager.sharedInstance.parseCampaignActivities(response as! Array<Dictionary<String, AnyObject>>, completionHandler: { (arrCampaignActivites) in
                    
                    callback(true,arrCampaignActivites as AnyObject?)
                })
            }
            else {
                
                callback(false,response)
            }
        }
    }
    
    func getAllArticleFromServer(_ completionQueue:DispatchQueue = DispatchQueue.main,pageNumber:Int ,callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void)  {
        
        let baseURLString = String("CampaignActivityList/\(CampaignActivityType.article.rawValue),\(CampaignActivityType.blogPost.rawValue)/0/\(pageNumber)")
        
       // let requestURL:URL = URL(string: baseURLString!)!
        
        NetworkingManager.sharedInstance.getRequest(baseURLString!) { (succeeded, response) in
            completionQueue.async(execute: {
                callback(succeeded,response)
            })
        }
    }
    
    func deleteUserNotificationByID(_ notificationId: NSNumber, completionQueue:DispatchQueue = DispatchQueue.main,callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void)  {
        
       // let requestURL:URL = URL(string: "UserNotificationList")!
        
        let param = [
            "NotificationID": notificationId
        ]
        
        NetworkingManager.sharedInstance.deleteRequest([param] as AnyObject, url: "UserNotificationList") { (succeeded, response) in
            completionQueue.async(execute: {
                
                callback(succeeded,response)
            })
        }
    }
    
    func getUserNotificationFromServer(_ addionalHeaderParamDict:Dictionary<String,AnyObject>? = nil,_ completionQueue:DispatchQueue = DispatchQueue.main,callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void)  {
       // let requestURL:URL = URL(string: "UserNotificationList")!
        
        
        NetworkingManager.sharedInstance.getRequest("UserNotificationList", addionalHeaderParamDict) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    let RedemptionList = [CampaignActivity].from(jsonArray:(response)! as! [JSON])
                    callback(true, RedemptionList as AnyObject?)
                }
                else {
                    
                    callback(false, response)
                }
            })
        }
    }
    
    func getOpeningHoursForAccount(_ strAccountID:String,completionQueue:DispatchQueue = DispatchQueue.main,callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void)  {
        
        let baseURLString = String("OpeningHoursStatus/\(strAccountID)")
        
        //let requestURL:URL = URL(string: baseURLString!)!
        
        NetworkingManager.sharedInstance.getRequest(baseURLString!) { (succeeded, response) in
            completionQueue.async(execute: {
                callback(succeeded,response)
            })
        }
    }
    
    
    //Post User Activity
    func postUserActivity(_ contentType: Int, actionType: Int, ContentID: Int, Quantity: Int = 0, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = "UserActivity"
        
        let param = [
            "ContentType": contentType,
            "UserAction":  actionType,
            "ContentID":   ContentID,
            "Quantity":    Quantity
        ]
        
        NetworkingManager.sharedInstance.postRequest(param as AnyObject, url: requestURL) { (succeeded, response) in
            
            callback(succeeded, response)
        }
    }
    
    //Post User Activity with Parameters
    func postUserActivity(_ parameters:NSDictionary, callback:@escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        let requestURL:String = "UserActivity"
        
        NetworkingManager.sharedInstance.postRequest(parameters, url: requestURL) { (succeeded, response) in
            
            callback(succeeded, response)
        }
    }
    
    
    //Account Details
    func getAccountDetails(_ accountID:NSNumber, completionQueue:DispatchQueue = DispatchQueue.main,callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void)  {
        
        let baseURLString = String("Account/\(accountID)")
        
       // let requestURL:URL = URL(string: baseURLString!)!
        
        NetworkingManager.sharedInstance.getRequest(baseURLString!) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    let account: AccountModel = ParsingManager.sharedInstance.updateAndParseAccountAllDetailsFromDictionary(response as! Dictionary<String,AnyObject>, strAccountID: accountID)!
                    callback(succeeded,account)
                }
                else {
                    
                    callback(succeeded,response)
                }
                
            })
        }
    }
    
    func getBadgeCountForUser(completionQueue:DispatchQueue = DispatchQueue.main,callback: @escaping (_ succeeded: Bool, _ response: AnyObject?) -> Void) {
        
        
        //let baseURLString = GET_NOTIFICATION_COUNT
        
       // let requestURL:URL = URL(string: baseURLString!)!
        
        NetworkingManager.sharedInstance.getRequest(GET_NOTIFICATION_COUNT) { (succeeded, response) in
            completionQueue.async(execute: {
                
                if succeeded == true {
                    
                    //let account: AccountModel = ParsingManager.sharedInstance.updateAndParseAccountAllDetailsFromDictionary(response as! Dictionary<String,AnyObject>, strAccountID: accountID)!
                    callback(succeeded,response)
                }
                else {
                    
                    callback(succeeded,response)
                }
                
            })
        }
        
    }
    
}
